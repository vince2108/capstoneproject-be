package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.entity.WorkSchedule;
import fpt.edu.capstone.repository.WorkScheduleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WorkScheduleServiceImplTest {

    @Mock
    private WorkScheduleRepository mockWorkScheduleRepository;

    private WorkScheduleServiceImpl workScheduleServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        workScheduleServiceImplUnderTest = new WorkScheduleServiceImpl(mockWorkScheduleRepository);
    }

    @Test
    void testGetById() {
        final WorkSchedule workSchedule = new WorkSchedule(1,
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(),
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, 1,
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        when(mockWorkScheduleRepository.getById(1)).thenReturn(workSchedule);
        final WorkSchedule result = workScheduleServiceImplUnderTest.getById(1);
    }
}
