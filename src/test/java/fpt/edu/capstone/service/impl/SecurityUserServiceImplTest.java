package fpt.edu.capstone.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SecurityUserServiceImplTest {

    private SecurityUserServiceImpl securityUserServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        securityUserServiceImplUnderTest = new SecurityUserServiceImpl();
    }
    @Test
    void testLoadUserByUsername() {
        assertThat(securityUserServiceImplUnderTest.loadUserByUsername("username")).isNull();
    }
}
