package fpt.edu.capstone.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Uploader;
import fpt.edu.capstone.dto.file.UploadFileRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DinaryServiceImplTest {

    @Mock
    private Cloudinary mockCloudinaryConfig;

    private DinaryServiceImpl dinaryServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        dinaryServiceImplUnderTest = new DinaryServiceImpl(mockCloudinaryConfig);
    }

    @Test
    void testUploadFile() {
                     
        final MultipartFile gif = null;

                     
        final Uploader uploader = new Uploader(new Cloudinary(new HashMap<>()), null);
        when(mockCloudinaryConfig.uploader()).thenReturn(uploader);

                     
        final String result = dinaryServiceImplUnderTest.uploadFile(gif);

                    
        assertThat(result).isEqualTo("result");
    }

    @Test
    void testUploadFileToDinary() throws Exception {
                     
        final UploadFileRequest request = new UploadFileRequest();
        request.setType("type");
        request.setUploadFileName("uploadFileName");
        request.setFile(null);
        request.setTypeUpload("typeUpload");

        final MultipartFile multipartFile = null;

                     
        final Uploader uploader = new Uploader(new Cloudinary(new HashMap<>()), null);
        when(mockCloudinaryConfig.uploader()).thenReturn(uploader);

                     
        final String result = dinaryServiceImplUnderTest.uploadFileToDinary(request, multipartFile);

                    
        assertThat(result).isEqualTo("result");
    }

    @Test
    void testUploadFileToDinary_ThrowsException() {
                     
        final UploadFileRequest request = new UploadFileRequest();
        request.setType("type");
        request.setUploadFileName("uploadFileName");
        request.setFile(null);
        request.setTypeUpload("typeUpload");

        final MultipartFile multipartFile = null;

                     
        final Uploader uploader = new Uploader(new Cloudinary(new HashMap<>()), null);
        when(mockCloudinaryConfig.uploader()).thenReturn(uploader);

                     
        assertThatThrownBy(() -> dinaryServiceImplUnderTest.uploadFileToDinary(request, multipartFile))
                .isInstanceOf(Exception.class);
    }
}
