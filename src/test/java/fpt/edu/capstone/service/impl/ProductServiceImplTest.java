package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.product.ProductRequest;
import fpt.edu.capstone.dto.product.ProductResponse;
import fpt.edu.capstone.dto.productGroup.IProductGroupResponse;
import fpt.edu.capstone.dto.productGroup.PrGroupUpRequest;
import fpt.edu.capstone.dto.productGroup.ProductGroupRequest;
import fpt.edu.capstone.entity.Product;
import fpt.edu.capstone.entity.ProductGroup;
import fpt.edu.capstone.repository.ProductGroupRepository;
import fpt.edu.capstone.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @Mock
    private ProductRepository mockProductRepository;
    @Mock
    private ProductGroupRepository mockProductGroupRepository;
    @Mock
    private ModelMapper mockModelMapper;

    private ProductServiceImpl productServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        productServiceImplUnderTest = new ProductServiceImpl(mockProductRepository, mockProductGroupRepository,
                mockModelMapper);
    }
    public ProductGroup productGroup(){
        productGroup().setProductGroupId(1);
        productGroup().setProductGroupName("productGroupName");
        productGroup().setProductTypeId(1);
        productGroup().setUse(Boolean.valueOf("True"));
        return productGroup();
    }
    @Test
    void testCreateProductGroup() {
             
        final ProductGroupRequest productGroupRequest = new ProductGroupRequest();
        productGroupRequest.setProductGroupName("productGroupName");

             
        final ProductGroup productGroup = productGroup();
        when(mockModelMapper.map(any(Object.class), eq(ProductGroup.class))).thenReturn(productGroup);

             
        final ProductGroup productGroup1 = productGroup();
        when(mockProductGroupRepository.save(any(ProductGroup.class))).thenReturn(productGroup1);

             
        productServiceImplUnderTest.createProductGroup(productGroupRequest);

             
        verify(mockProductGroupRepository).save(any(ProductGroup.class));
    }

    @Test
    void testGetAllProductGroup() {
             
        when(mockProductGroupRepository.getAllProductGroup()).thenReturn(Arrays.asList());

             
        final List<IProductGroupResponse> result = productServiceImplUnderTest.getAllProductGroup();

             
    }

    @Test
    void testGetAllProductGroup_ProductGroupRepositoryReturnsNoItems() {
             
        when(mockProductGroupRepository.getAllProductGroup()).thenReturn(Collections.emptyList());

             
        final List<IProductGroupResponse> result = productServiceImplUnderTest.getAllProductGroup();

             
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testUpdateProductGroup() {
             
        final PrGroupUpRequest prGroupUpRequest = new PrGroupUpRequest();
        prGroupUpRequest.setProductGroupId(1);
        prGroupUpRequest.setProductGroupName("productGroupName");
        prGroupUpRequest.setProductTypeId(1);

             
        final Optional<ProductGroup> productGroup = Optional.of(productGroup());
        when(mockProductGroupRepository.findById(1)).thenReturn(productGroup);

             
        final ProductGroup productGroup1 = productGroup();
        when(mockModelMapper.map(any(Object.class), eq(ProductGroup.class))).thenReturn(productGroup1);

             
        final ProductGroup productGroup2 = productGroup();
        when(mockProductGroupRepository.save(any(ProductGroup.class))).thenReturn(productGroup2);

             
        productServiceImplUnderTest.updateProductGroup(prGroupUpRequest);

             
        verify(mockProductGroupRepository).save(any(ProductGroup.class));
    }

    @Test
    void testUpdateProductGroup_ProductGroupRepositoryFindByIdReturnsAbsent() {
             
        final PrGroupUpRequest prGroupUpRequest = new PrGroupUpRequest();
        prGroupUpRequest.setProductGroupId(1);
        prGroupUpRequest.setProductGroupName("productGroupName");
        prGroupUpRequest.setProductTypeId(1);

        when(mockProductGroupRepository.findById(1)).thenReturn(Optional.empty());

             
        assertThatThrownBy(() -> productServiceImplUnderTest.updateProductGroup(prGroupUpRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testDeleteProduct() {
             
             
        final Optional<ProductGroup> productGroup = Optional.of(productGroup());
        when(mockProductGroupRepository.findById(1)).thenReturn(productGroup);

             
        final ProductGroup productGroup1 = productGroup();
        when(mockProductGroupRepository.save(any(ProductGroup.class))).thenReturn(productGroup1);

             
        productServiceImplUnderTest.deleteProduct(1);

             
        verify(mockProductGroupRepository).save(any(ProductGroup.class));
    }

    @Test
    void testDeleteProduct_ProductGroupRepositoryFindByIdReturnsAbsent() {
             
        when(mockProductGroupRepository.findById(1)).thenReturn(Optional.empty());

             
        assertThatThrownBy(() -> productServiceImplUnderTest.deleteProduct(1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testCreateProduct() {
             
        final ProductRequest productRequest = new ProductRequest();
        productRequest.setProductName("productName");
        productRequest.setUnitId(1);
        productRequest.setPrice(0.0);
        productRequest.setNote("note");
        productRequest.setUserObject("userObject");
        productRequest.setUsing("using");
        productRequest.setProductGroupIds(Arrays.asList(1));

             
        final Product product = new Product(1, "productName", 0.0, 1, "note", "userObject", "using");
        when(mockModelMapper.map(any(Object.class), eq(Product.class))).thenReturn(product);

             
        final Product product1 = new Product(1, "productName", 0.0, 1, "note", "userObject", "using");
        when(mockProductRepository.save(any(Product.class))).thenReturn(product1);

             
        productServiceImplUnderTest.createProduct(productRequest);

             
        verify(mockProductRepository).insertProduct_ProductGroup(1, 1);
    }

    @Test
    void testGetAllProduct() {
             
        when(mockProductRepository.getAllProduct()).thenReturn(Arrays.asList());

             
        final ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1);
        productResponse.setProductName("productName");
        productResponse.setPrice(0.0);
        productResponse.setUnitId(1);
        productResponse.setUnitName("unitName");
        productResponse.setNote("note");
        productResponse.setUserObject("userObject");
        productResponse.setUsing("using");
        productResponse.setIUnitGroupList(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ProductResponse.class))).thenReturn(productResponse);

        when(mockProductRepository.getProGroupByIdProduct(1)).thenReturn(Arrays.asList());

             
        final List<ProductResponse> result = productServiceImplUnderTest.getAllProduct();

             
    }

    @Test
    void testGetAllProduct_ProductRepositoryGetAllProductReturnsNoItems() {
             
        when(mockProductRepository.getAllProduct()).thenReturn(Collections.emptyList());

             
        final ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1);
        productResponse.setProductName("productName");
        productResponse.setPrice(0.0);
        productResponse.setUnitId(1);
        productResponse.setUnitName("unitName");
        productResponse.setNote("note");
        productResponse.setUserObject("userObject");
        productResponse.setUsing("using");
        productResponse.setIUnitGroupList(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ProductResponse.class))).thenReturn(productResponse);

        when(mockProductRepository.getProGroupByIdProduct(1)).thenReturn(Arrays.asList());

             
        final List<ProductResponse> result = productServiceImplUnderTest.getAllProduct();

             
    }

    @Test
    void testGetAllProduct_ProductRepositoryGetProGroupByIdProductReturnsNoItems() {
             
        when(mockProductRepository.getAllProduct()).thenReturn(Arrays.asList());

             
        final ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1);
        productResponse.setProductName("productName");
        productResponse.setPrice(0.0);
        productResponse.setUnitId(1);
        productResponse.setUnitName("unitName");
        productResponse.setNote("note");
        productResponse.setUserObject("userObject");
        productResponse.setUsing("using");
        productResponse.setIUnitGroupList(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ProductResponse.class))).thenReturn(productResponse);

        when(mockProductRepository.getProGroupByIdProduct(1)).thenReturn(Collections.emptyList());

             
        final List<ProductResponse> result = productServiceImplUnderTest.getAllProduct();

             
    }
}
