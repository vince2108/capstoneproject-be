package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.entity.Role;
import fpt.edu.capstone.repository.RoleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RoleServiceImplTest {

    @Mock
    private RoleRepository mockRoleRepository;

    private RoleServiceImpl roleServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        roleServiceImplUnderTest = new RoleServiceImpl(mockRoleRepository);
    }
    @Test
    void testGetAllRole() {
        when(mockRoleRepository.findAll()).thenReturn(Arrays.asList(new Role(1, "roleName")));
        final List<Role> result = roleServiceImplUnderTest.getAllRole();
    }
    @Test
    void testGetAllRole_RoleRepositoryReturnsNoItems() {
        when(mockRoleRepository.findAll()).thenReturn(Collections.emptyList());
        final List<Role> result = roleServiceImplUnderTest.getAllRole();
        assertThat(result).isEqualTo(Collections.emptyList());
    }
}
