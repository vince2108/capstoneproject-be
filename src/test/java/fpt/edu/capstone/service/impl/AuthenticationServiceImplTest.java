package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.account.LoginResponse;
import fpt.edu.capstone.dto.account.RegisterRequest;
import fpt.edu.capstone.entity.Account;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.AuthenticationRepository;
import fpt.edu.capstone.repository.StaffRepository;
import fpt.edu.capstone.service.ContactService;
import fpt.edu.capstone.service.EmailService;
import fpt.edu.capstone.service.RoleService;
import fpt.edu.capstone.service.StaffService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthenticationServiceImplTest {

    @Mock
    private AuthenticationRepository mockLoginRepository;
    @Mock
    private ContactService mockContactService;
    @Mock
    private StaffService mockStaffService;
    @Mock
    private StaffRepository mockStaffRepository;
    @Mock
    private EmailService mockEmailService;
    @Mock
    private RoleService mockRoleService;

    private AuthenticationServiceImpl authenticationServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        authenticationServiceImplUnderTest = new AuthenticationServiceImpl(mockLoginRepository, mockContactService,
                mockStaffService, mockStaffRepository, mockEmailService, mockRoleService);
    }

    public Account account() {
        account().setAccountId(1);
        account().setUsername("username");
        account().setPassword("password");
        return account();
    }

    public Contact contact() {
        Contact contact = new Contact();
        contact.setAccountId(1);
        contact.setFullName("Nguyen Quang Vinh");
        contact.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        contact.setAddress("Hanoi");
        contact.setVillage("Cau Giay");
        contact.setDistrict("128 Cau Giay");
        contact.setProvince("Cau Giay");
        contact.setSex("Male");
        contact.setIdentityCard("415213212");
        contact.setPhoneNumber("1967445451");
        contact.setEthnicity("Eth");
        contact.setJob("job");
        contact.setStatus(true);
        contact.setRoleId(1);
        contact.setEmail("nqv@gmail.com");
        return contact;
    }

    public Staff staff() {
        Staff staff = new Staff();
        staff.setStaffId(1);
        staff.setAvatar("avatar.com");
        staff.setCertificate("cert");
        staff.setEducation("FPT");
        staff.setBranchId(1);
        return staff;
    }

    @Test
    void testLogin() throws Exception {
        final LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");
        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(account);
        final Optional<Account> account1 = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password")).thenReturn(account1);
        final Contact contact = contact();
        when(mockContactService.findContactByAccountId(1)).thenReturn(contact);
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);
        when(mockStaffRepository.findStaffInConsultingById(1)).thenReturn(Optional.empty());
        final LoginResponse result = authenticationServiceImplUnderTest.login(request);
    }

    @Test
    void testLogin_AuthenticationRepositoryFindAccountsByUsernameReturnsAbsent() {

        final LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");

        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(Optional.empty());


        assertThatThrownBy(() -> authenticationServiceImplUnderTest.login(request)).isInstanceOf(ClinicException.class);
    }

    @Test
    void testLogin_AuthenticationRepositoryFindAccountsByUsernameAndPasswordReturnsAbsent() {

        final LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");


        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(account);

        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password"))
                .thenReturn(Optional.empty());


        assertThatThrownBy(() -> authenticationServiceImplUnderTest.login(request)).isInstanceOf(ClinicException.class);
    }

    @Test
    void testLogin_StaffRepositoryFindByIdReturnsAbsent() throws Exception {

        final LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");


        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(account);


        final Optional<Account> account1 = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password")).thenReturn(account1);


        final Contact contact = contact();
        when(mockContactService.findContactByAccountId(1)).thenReturn(contact);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());
        when(mockStaffRepository.findStaffInConsultingById(1)).thenReturn(Optional.empty());


        final LoginResponse result = authenticationServiceImplUnderTest.login(request);


    }

    @Test
    void testLogin_StaffRepositoryFindStaffInConsultingByIdReturnsAbsent() {

        final LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");


        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(account);


        final Optional<Account> account1 = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password")).thenReturn(account1);


        final Contact contact = contact();
        when(mockContactService.findContactByAccountId(1)).thenReturn(contact);


        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

        when(mockStaffRepository.findStaffInConsultingById(1)).thenReturn(Optional.empty());


        assertThatThrownBy(() -> authenticationServiceImplUnderTest.login(request))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testLogin_ThrowsException() {

        final LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");


        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(account);


        final Optional<Account> account1 = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password")).thenReturn(account1);


        final Contact contact = contact();
        when(mockContactService.findContactByAccountId(1)).thenReturn(contact);


        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

        when(mockStaffRepository.findStaffInConsultingById(1)).thenReturn(Optional.empty());


        assertThatThrownBy(() -> authenticationServiceImplUnderTest.login(request)).isInstanceOf(Exception.class);
    }

    @Test
    void testRegister() {

        final RegisterRequest request = new RegisterRequest("username", "password", "confirmPassword");


        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(account);

        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());


        authenticationServiceImplUnderTest.register(request);


        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testRegister_AuthenticationRepositoryFindAccountsByUsernameReturnsAbsent() {

        final RegisterRequest request = new RegisterRequest("username", "password", "confirmPassword");
        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(Optional.empty());
        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());


        authenticationServiceImplUnderTest.register(request);


        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testSaveAccount() {

        final Account account = account();
        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());


        authenticationServiceImplUnderTest.saveAccount(account);


        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testForgotPassword() throws Exception {


        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactService.findByEmail("email")).thenReturn(contact);

        when(mockLoginRepository.findAccountByAccountId(0)).thenReturn(account());
        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());


        authenticationServiceImplUnderTest.forgotPassword("email");


        verify(mockLoginRepository).save(any(Account.class));
        verify(mockEmailService).sendForgotPasswordEmail("email", "newPassword");
    }

    @Test
    void testForgotPassword_ContactServiceReturnsAbsent() {

        when(mockContactService.findByEmail("email")).thenReturn(Optional.empty());


        assertThatThrownBy(() -> authenticationServiceImplUnderTest.forgotPassword("email"))
                .isInstanceOf(Exception.class);
    }

    @Test
    void testForgotPassword_EmailServiceThrowsMessagingException() throws Exception {


        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactService.findByEmail("email")).thenReturn(contact);

        when(mockLoginRepository.findAccountByAccountId(0)).thenReturn(account());
        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());
        doThrow(MessagingException.class).when(mockEmailService).sendForgotPasswordEmail("email", "newPassword");


        assertThatThrownBy(() -> authenticationServiceImplUnderTest.forgotPassword("email"))
                .isInstanceOf(Exception.class);
        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testForgotPassword_EmailServiceThrowsUnsupportedEncodingException() throws Exception {


        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactService.findByEmail("email")).thenReturn(contact);

        when(mockLoginRepository.findAccountByAccountId(0)).thenReturn(account());
        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());
        doThrow(UnsupportedEncodingException.class).when(mockEmailService).sendForgotPasswordEmail("email",
                "newPassword");


        assertThatThrownBy(() -> authenticationServiceImplUnderTest.forgotPassword("email"))
                .isInstanceOf(Exception.class);
        verify(mockLoginRepository).save(any(Account.class));
    }
}
