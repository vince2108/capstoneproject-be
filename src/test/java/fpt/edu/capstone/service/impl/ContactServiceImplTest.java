package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.account.ChangePwRequest;
import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.patient.ContactAndRecordRequest;
import fpt.edu.capstone.dto.patient.MedicalRecordResponse;
import fpt.edu.capstone.dto.patient.PatientResponse;
import fpt.edu.capstone.dto.staff.InventoryStaff;
import fpt.edu.capstone.dto.staff.StaffRequest;
import fpt.edu.capstone.dto.staff.StaffResponse;
import fpt.edu.capstone.entity.*;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.utils.ResponseDataPagination;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContactServiceImplTest {

    @Mock
    private ContactRepository mockContactRepository;
    @Mock
    private MedicalReportRepository mockMedicalReportRepository;
    @Mock
    private ConsultingRoomRepository mockConsultingRoomRepository;
    @Mock
    private MedicalRecordRepository mockMedicalRecordRepository;
    @Mock
    private MedicalReportServiceRepository mockRServiceRepository;
    @Mock
    private MedicalServiceRepository mockMedicalServiceRepository;
    @Mock
    private InvoiceRepository mockInvoiceRepository;
    @Mock
    private InvoiceDetailRepository mockInvoiceDetailRepository;
    @Mock
    private StaffRepository mockStaffRepository;
    @Mock
    private BranchRepository mockBranchRepository;
    @Mock
    private ModelMapper mockModelMapper;
    @Mock
    private AuthenticationRepository mockLoginRepository;

    private ContactServiceImpl contactServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        contactServiceImplUnderTest = new ContactServiceImpl(mockContactRepository, mockMedicalReportRepository,
                mockConsultingRoomRepository, mockMedicalRecordRepository, mockRServiceRepository,
                mockMedicalServiceRepository, mockInvoiceRepository, mockInvoiceDetailRepository, mockStaffRepository,
                mockBranchRepository, mockModelMapper, mockLoginRepository);
    }
    public Contact contact(){
        Contact contact = new Contact();
        contact.setAccountId(1);
        contact.setFullName("Nguyen Quang Vinh");
        contact.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        contact.setAddress("Hanoi");
        contact.setVillage("Cau Giay");
        contact.setDistrict("128 Cau Giay");
        contact.setProvince("Cau Giay");
        contact.setSex("Male");
        contact.setIdentityCard("415213212");
        contact.setPhoneNumber("1967445451");
        contact.setEthnicity("Eth");
        contact.setJob("job");
        contact.setStatus(true);
        contact.setRoleId(1);
        contact.setEmail("nqv@gmail.com");
        return contact;
    }

    public ConsultingRoom consultingRoom(){
        ConsultingRoom consultingRoom = new ConsultingRoom();
        consultingRoom.setConsultingRoomId(1);
        consultingRoom.setRoomName("Kham ABC");
        consultingRoom.setAbbreviationName("ABC");
        consultingRoom.setBranchId(1);
        consultingRoom.setAction(true);
        return consultingRoom;
    }

    public MedicalReport medicalReport(){
        MedicalReport medicalReport = new MedicalReport();
        medicalReport.setMedicalReportId(1);
        medicalReport.setDate(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        medicalReport.setAccountId(1);
      //  medicalReport.setConsultingRoomId(1);
        medicalReport.setFinishedExamination(true);
        medicalReport.setMedicalRecordId(1);
        return medicalReport;
    }

    public StaffRequest staffRequest (){
        StaffRequest staffRequest = new StaffRequest();
        staffRequest.setFullName("fullName");
        staffRequest.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        staffRequest.setAddress("address");
        staffRequest.setVillage("village");
        staffRequest.setDistrict("district");
        staffRequest.setProvince("province");
        staffRequest.setSex("sex");
        staffRequest.setIdentityCard("identityCard");
        staffRequest.setPhoneNumber("phoneNumber");
        staffRequest.setEthnicity("ethnicity");
        staffRequest.setJob("job");
        staffRequest.setEmail("email");
        staffRequest.setUserName("username");
        staffRequest.setPassWord("password");
        staffRequest.setConfirmPassWord("password");
        staffRequest.setBranchId(1);
        staffRequest.setRoleId(1);
        staffRequest.setIdConsultingRoom(1);
        staffRequest.setPosition("position");
        return staffRequest;
    }

    public ContactRequest contactRequest (){
        ContactRequest contact = new ContactRequest();
        contact.setFullName("Nguyen Quang Vinh");
        contact.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        contact.setAddress("Hanoi");
        contact.setVillage("Cau Giay");
        contact.setDistrict("128 Cau Giay");
        contact.setProvince("Cau Giay");
        contact.setSex("Male");
        contact.setIdentityCard("415213212");
        contact.setPhoneNumber("1967445451");
        contact.setEthnicity("Eth");
        contact.setJob("job");
        contact.setEmail("nqv@gmail.com");
        return contact;
    }

    public Account account(){
        Account account = new Account();
        account.setUsername("username");
        account.setPassword("123");
        account.setAccountId(1);
        return account;
    }

    public Staff staff(){
        Staff staff = new Staff();
        staff.setStaffId(1);
        staff.setAvatar("avatar.com");
        staff.setCertificate("cert");
        staff.setEducation("FPT");
        staff.setBranchId(1);
        return staff;
    }


    @Test
    void testGetAllPatient() {
           
                  
        final List<MedicalReport> medicalReports = Arrays.asList(
                medicalReport());
        when(mockMedicalReportRepository.findReportByFinishedExamination(false)).thenReturn(medicalReports);

           
        final List<PatientResponse> result = contactServiceImplUnderTest.getAllPatient();

           
    }

    @Test
    void testGetAllPatient_MedicalReportRepositoryReturnsNoItems() {
           
        when(mockMedicalReportRepository.findReportByFinishedExamination(false)).thenReturn(Collections.emptyList());

           
        final List<PatientResponse> result = contactServiceImplUnderTest.getAllPatient();

           
        assertThat(result).isNull();
    }

    @Test
    void testFindContactByAccountId() {
           
           
        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

           
        final Contact result = contactServiceImplUnderTest.findContactByAccountId(1);

           
    }

    @Test
    void testFindContactByAccountId_ContactRepositoryReturnsAbsent() {
           
        when(mockContactRepository.findById(1)).thenReturn(Optional.empty());

           
        assertThatThrownBy(() -> contactServiceImplUnderTest.findContactByAccountId(1))
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void testFindContactByRoleIdAndStatusT() {
           
        when(mockContactRepository.getIdPatientExaminationByIdBranch(1)).thenReturn(Arrays.asList(1));

                  
        final Contact contact = contact();
        when(mockContactRepository.findContactByRoleIdAndStatusT(1, 1)).thenReturn(contact);

           
        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleIdAndStatusT(1, 1);

           
    }

    @Test
    void testFindContactByRoleIdAndStatusT_ContactRepositoryGetIdPatientExaminationByIdBranchReturnsNoItems() {
           
        when(mockContactRepository.getIdPatientExaminationByIdBranch(0)).thenReturn(Collections.emptyList());

                  
        final Contact contact = contact();
        when(mockContactRepository.findContactByRoleIdAndStatusT(0, 0)).thenReturn(contact);

           
        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleIdAndStatusT(0, 0);

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testFindContactByRoleIdAndStatusF() {
           
                 
        final List<Contact> contactList = Arrays.asList(
                contact());
        when(mockContactRepository.findContactByRoleIdAndStatusF(0)).thenReturn(contactList);

           
        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleIdAndStatusF(0);

           
    }

    @Test
    void testFindContactByRoleIdAndStatusF_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.findContactByRoleIdAndStatusF(0)).thenReturn(Collections.emptyList());

           
        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleIdAndStatusF(0);

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testFindContactByRoleId() {
           
                  
        final List<Contact> contactList = Arrays.asList(
                contact());
        when(mockContactRepository.findContactByRoleId(1)).thenReturn(contactList);

           
        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleId(0);

           
    }

    @Test
    void testFindContactByRoleId_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.findContactByRoleId(1)).thenReturn(Collections.emptyList());

           
        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleId(1);

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testContactRequestToContact() {
           
        final ContactRequest contactRequest = contactRequest();

           
        final Contact contact = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);

           
        final Contact result = contactServiceImplUnderTest.ContactRequestToContact(contactRequest);

           
    }

    @Test
    void testContactRequestToStaff() {
           
        final StaffRequest staffRequest = staffRequest();

                  
        final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

                 
        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsername("userName")).thenReturn(account);

           
        final Contact contact = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);

        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());

                  
        final Staff staff = staff();
        when(mockStaffRepository.save(any(Staff.class))).thenReturn(staff);

           
        contactServiceImplUnderTest.ContactRequestToStaff(staffRequest);

           
        verify(mockLoginRepository).save(any(Account.class));
        verify(mockStaffRepository).insertStaff_Consulting(1, 1, "position");
    }

    @Test
    void testContactRequestToStaff_ConsultingRoomRepositoryReturnsAbsent() {
           
        final StaffRequest staffRequest = staffRequest();

        when(mockConsultingRoomRepository.findById(1)).thenReturn(Optional.empty());

           
        assertThatThrownBy(() -> contactServiceImplUnderTest.ContactRequestToStaff(staffRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testContactRequestToStaff_AuthenticationRepositoryFindAccountsByUsernameReturnsAbsent() {
           
        final StaffRequest staffRequest = staffRequest();

                  
        final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

        when(mockLoginRepository.findAccountsByUsername("userName")).thenReturn(Optional.empty());

           
        final Contact contact = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);

        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());

                  
        final Staff staff = staff();
        when(mockStaffRepository.save(any(Staff.class))).thenReturn(staff);

           
        contactServiceImplUnderTest.ContactRequestToStaff(staffRequest);

           
        verify(mockLoginRepository).save(any(Account.class));
        verify(mockStaffRepository).insertStaff_Consulting(1, 1, "position");
    }

    @Test
    void testContactRequestToInventoryStaff() {
           
        final InventoryStaff inventoryStaff = new InventoryStaff();
        inventoryStaff.setFullName("fullName");
        inventoryStaff.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        inventoryStaff.setAddress("address");
        inventoryStaff.setVillage("village");
        inventoryStaff.setDistrict("district");
        inventoryStaff.setProvince("province");
        inventoryStaff.setSex("sex");
        inventoryStaff.setIdentityCard("identityCard");
        inventoryStaff.setPhoneNumber("phoneNumber");
        inventoryStaff.setEthnicity("ethnicity");
        inventoryStaff.setJob("job");
        inventoryStaff.setEmail("email");
        inventoryStaff.setUserName("username");
        inventoryStaff.setPassWord("password");
        inventoryStaff.setConfirmPassWord("confirmPassWord");
        inventoryStaff.setBranchId(1);

                 
        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(account);

           
        final Contact contact = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);

        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());

                  
        final Staff staff = staff();
        when(mockStaffRepository.save(any(Staff.class))).thenReturn(staff);

           
        contactServiceImplUnderTest.ContactRequestToInventoryStaff(inventoryStaff);

           
        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testContactRequestToInventoryStaff_AuthenticationRepositoryFindAccountsByUsernameReturnsAbsent() {
           
        final InventoryStaff inventoryStaff = new InventoryStaff();
        inventoryStaff.setFullName("fullName");
        inventoryStaff.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        inventoryStaff.setAddress("address");
        inventoryStaff.setVillage("village");
        inventoryStaff.setDistrict("district");
        inventoryStaff.setProvince("province");
        inventoryStaff.setSex("sex");
        inventoryStaff.setIdentityCard("identityCard");
        inventoryStaff.setPhoneNumber("phoneNumber");
        inventoryStaff.setEthnicity("ethnicity");
        inventoryStaff.setJob("job");
        inventoryStaff.setEmail("email");
        inventoryStaff.setUserName("username");
        inventoryStaff.setPassWord("password");
        inventoryStaff.setConfirmPassWord("confirmPassWord");
        inventoryStaff.setBranchId(1);

        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(Optional.empty());

           
        final Contact contact = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);

        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());

                  
        final Staff staff = staff();
        when(mockStaffRepository.save(any(Staff.class))).thenReturn(staff);

           
        contactServiceImplUnderTest.ContactRequestToInventoryStaff(inventoryStaff);

           
        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testUpdateContact() {
           
        final ContactRequest contactRequest = contactRequest();

                  
        final Contact contact = contact();
        when(mockContactRepository.getOne(1)).thenReturn(contact);

           
        final Contact contact1 = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact1);

           
        final Contact result = contactServiceImplUnderTest.updateContact(1, contactRequest);

           
    }

    @Test
    void testUpdateStaff() {
           
        final ContactRequest contactRequest = new ContactRequest();
        contactRequest.setFullName("fullName");
        contactRequest.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        contactRequest.setAddress("address");
        contactRequest.setVillage("village");
        contactRequest.setDistrict("district");
        contactRequest.setProvince("province");
        contactRequest.setSex("sex");
        contactRequest.setIdentityCard("identityCard");
        contactRequest.setPhoneNumber("phoneNumber");
        contactRequest.setEthnicity("ethnicity");
        contactRequest.setJob("job");
        contactRequest.setEmail("email");

                  
        final Contact contact = contact();
        when(mockContactRepository.getOne(1)).thenReturn(contact);

                  
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

           
        final Contact contact1 = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact1);

           
        final Contact result = contactServiceImplUnderTest.updateStaff(1, contactRequest);

           
    }

    @Test
    void testUpdateStaff_StaffRepositoryReturnsAbsent() {
           
        final ContactRequest contactRequest = contactRequest();

                  
        final Contact contact = contact();
        when(mockContactRepository.getOne(1)).thenReturn(contact);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

           
        assertThatThrownBy(() -> contactServiceImplUnderTest.updateStaff(1, contactRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testDeleteStaff() {
           
                  
        final Contact contact = contact();
        when(mockContactRepository.getById(1)).thenReturn(contact);

           
        final Contact contact1 = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact1);

           
        contactServiceImplUnderTest.deleteStaff(1);

           
        verify(mockContactRepository).save(any(Contact.class));
    }
    public MedicalRecord medicalRecord(){
        medicalRecord().setMedicalRecordId(1);
        medicalRecord().setAccountId(1);
        medicalRecord().setCircuit(0);
        medicalRecord().setBloodPressure(0);
        medicalRecord().setBMI(0.0f);
        medicalRecord().setTemperature(0.0f);
        medicalRecord().setWeight(0.0f);
        medicalRecord().setPastMedicalHistory("pastMedicalHistory");
        medicalRecord().setAllergies("allergies");
        medicalRecord().setNote("note");
        medicalRecord().setIcd10("icd10");
        medicalRecord().setIcd("icd");
        medicalRecord().setAdvice("advice");
        medicalRecord().setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        return medicalRecord();
    }

    @Test
    void testFindPatientById() {
           
           
        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

                  
        final Optional<MedicalRecord> medicalRecord = Optional.of(
                medicalRecord());
        when(mockMedicalRecordRepository.findTopByAccountIdOrderByMedicalRecordIdDesc(0)).thenReturn(medicalRecord);

           
        final MedicalRecordResponse medicalRecordResponse = new MedicalRecordResponse();
        medicalRecordResponse.setAccountId(0);
        medicalRecordResponse.setFullName("fullName");
        medicalRecordResponse.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        medicalRecordResponse.setAddress("address");
        medicalRecordResponse.setVillage("village");
        medicalRecordResponse.setDistrict("district");
        medicalRecordResponse.setProvince("province");
        medicalRecordResponse.setSex("sex");
        medicalRecordResponse.setIdentityCard("identityCard");
        medicalRecordResponse.setPhoneNumber("phoneNumber");
        medicalRecordResponse.setEthnicity("ethnicity");
        medicalRecordResponse.setJob("job");
        medicalRecordResponse.setEmail("email");
        medicalRecordResponse.setMedicalRecordId(0);
        medicalRecordResponse.setCircuit(0);
        when(mockModelMapper.map(any(Object.class), eq(MedicalRecordResponse.class))).thenReturn(medicalRecordResponse);

           
        final MedicalRecordResponse result = contactServiceImplUnderTest.findPatientById(0);

           
    }

    @Test
    void testFindPatientById_ContactRepositoryReturnsAbsent() {
           
        when(mockContactRepository.findById(0)).thenReturn(Optional.empty());

           
        assertThatThrownBy(() -> contactServiceImplUnderTest.findPatientById(0)).isInstanceOf(ClinicException.class);
    }

    @Test
    void testFindPatientById_MedicalRecordRepositoryReturnsAbsent() {
           
           
        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

        when(mockMedicalRecordRepository.findTopByAccountIdOrderByMedicalRecordIdDesc(1)).thenReturn(Optional.empty());

           
        assertThatThrownBy(() -> contactServiceImplUnderTest.findPatientById(1)).isInstanceOf(ClinicException.class);
    }

    @Test
    void testFindStaffByRoleId() {
           

        final List<Contact> contactList = Arrays.asList(
                contact());
        when(mockContactRepository.findStaffByRoleId()).thenReturn(contactList);

           
        final List<Contact> result = contactServiceImplUnderTest.findStaffByRoleId();

           
    }

    @Test
    void testFindStaffByRoleId_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.findStaffByRoleId()).thenReturn(Collections.emptyList());

           
        final List<Contact> result = contactServiceImplUnderTest.findStaffByRoleId();

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testFindStaffByRoleIdAndAccountId() {
           
                  
        final Contact contact = contact();
        when(mockContactRepository.findStaffByRoleIdAndAccountId(1)).thenReturn(contact);

           
        final StaffResponse staffResponse = new StaffResponse();
        staffResponse.setAccountId(1);
        staffResponse.setFullName("fullName");
        staffResponse.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        staffResponse.setAddress("address");
        staffResponse.setVillage("village");
        staffResponse.setDistrict("district");
        staffResponse.setProvince("province");
        staffResponse.setSex("sex");
        staffResponse.setIdentityCard("identityCard");
        staffResponse.setAvatar("avatar");
        staffResponse.setEducation("education");
        staffResponse.setCertificate("certificate");
        staffResponse.setBranchId(1);
        staffResponse.setConsultingRoomId(1);
        staffResponse.setPossition("possition");
        when(mockModelMapper.map(any(Object.class), eq(StaffResponse.class))).thenReturn(staffResponse);

        when(mockStaffRepository.getStaffInfo(1)).thenReturn(null);

           
        final StaffResponse result = contactServiceImplUnderTest.findStaffByRoleIdAndAccountId(0);

           
    }

    @Test
    void testFindStaffByRoleIdAndAccountId_StaffRepositoryReturnsNull() {
           
                  
        final Contact contact = contact();
        when(mockContactRepository.findStaffByRoleIdAndAccountId(1)).thenReturn(contact);

           
        final StaffResponse staffResponse = new StaffResponse();
        staffResponse.setAccountId(1);
        staffResponse.setFullName("fullName");
        staffResponse.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        staffResponse.setAddress("address");
        staffResponse.setVillage("village");
        staffResponse.setDistrict("district");
        staffResponse.setProvince("province");
        staffResponse.setSex("sex");
        staffResponse.setIdentityCard("identityCard");
        staffResponse.setAvatar("avatar");
        staffResponse.setEducation("education");
        staffResponse.setCertificate("certificate");
        staffResponse.setBranchId(1);
        staffResponse.setConsultingRoomId(1);
        staffResponse.setPossition("possition");
        when(mockModelMapper.map(any(Object.class), eq(StaffResponse.class))).thenReturn(staffResponse);

        when(mockStaffRepository.getStaffInfo(1)).thenReturn(null);

           
        assertThatThrownBy(() -> contactServiceImplUnderTest.findStaffByRoleIdAndAccountId(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testFindStaffByName() {
           
                  
        final List<Contact> contactList = Arrays.asList(
                contact());
        when(mockContactRepository.findStaffByName("fullName")).thenReturn(contactList);

           
        final List<Contact> result = contactServiceImplUnderTest.findStaffByName("fullName");

           
    }

    @Test
    void testFindStaffByName_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.findStaffByName("fullName")).thenReturn(Collections.emptyList());

           
        final List<Contact> result = contactServiceImplUnderTest.findStaffByName("fullName");

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetContactByNameAndStatus() {
           
                  
        final List<Contact> contactList = Arrays.asList(
                contact());
        when(mockContactRepository.findContactByNameAndStatus("name", false)).thenReturn(contactList);

           
        final List<Contact> result = contactServiceImplUnderTest.getContactByNameAndStatus("name", false);

           
    }

    @Test
    void testGetContactByNameAndStatus_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.findContactByNameAndStatus("name", false)).thenReturn(Collections.emptyList());

           
        final List<Contact> result = contactServiceImplUnderTest.getContactByNameAndStatus("name", false);

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetContactByName() {
           
                  
        final List<Contact> contactList = Arrays.asList(
                contact());
        when(mockContactRepository.findContactByName("name")).thenReturn(contactList);

           
        final List<Contact> result = contactServiceImplUnderTest.getContactByName("name");

           
    }

    @Test
    void testGetContactByName_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.findContactByName("name")).thenReturn(Collections.emptyList());

           
        final List<Contact> result = contactServiceImplUnderTest.getContactByName("name");

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetContactByExactName() {
           
        when(mockContactRepository.findContactByExactName("name")).thenReturn(account());

           
        final Account result = contactServiceImplUnderTest.getContactByExactName("name");

           
    }

    @Test
    void testGetContactsPaidByIdBranch() {
           
                  
        final List<Contact> contactList = Arrays.asList(
                contact());
        when(mockContactRepository.findContactPaymentByIdBranch(1)).thenReturn(contactList);

           
        final List<Contact> result = contactServiceImplUnderTest.getContactsPaidByIdBranch(1);

           
    }

    @Test
    void testGetContactsPaidByIdBranch_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.findContactPaymentByIdBranch(1)).thenReturn(Collections.emptyList());

           
        final List<Contact> result = contactServiceImplUnderTest.getContactsPaidByIdBranch(1);

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetContactsPaidByIdBranchAndName() {
           
                 
        final List<Contact> contactList = Arrays.asList(
                contact());
        when(mockContactRepository.findContactPaymentByIdBranchAndName(1, "name")).thenReturn(contactList);

           
        final List<Contact> result = contactServiceImplUnderTest.getContactsPaidByIdBranchAndName(1, "name");

           
    }

    @Test
    void testGetContactsPaidByIdBranchAndName_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.findContactPaymentByIdBranchAndName(1, "name")).thenReturn(Collections.emptyList());

           
        final List<Contact> result = contactServiceImplUnderTest.getContactsPaidByIdBranchAndName(1, "name");

           
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetListContact() {
           
                  
        final Page<Contact> contactPage = new PageImpl<>(Arrays.asList(
                contact()));
        when(mockContactRepository.getAllContact(any(Pageable.class))).thenReturn(contactPage);

           
        final ResponseDataPagination result = contactServiceImplUnderTest.getListContact(1, 10);

           
    }

    @Test
    void testGetListContact_ContactRepositoryReturnsNoItems() {
           
        when(mockContactRepository.getAllContact(any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

           
        final ResponseDataPagination result = contactServiceImplUnderTest.getListContact(1, 10);

           
    }

    @Test
    void testSaveStaffAccount() {
           
        final LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("username");
        loginRequest.setPassword("password");

        when(mockLoginRepository.findAccountByUsername("username")).thenReturn(account());
        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());

           
        contactServiceImplUnderTest.saveStaffAccount(loginRequest);

           
        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testSaveStaffAccount_AuthenticationRepositoryFindAccountByUsernameReturnsNull() {
           
        final LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("username");
        loginRequest.setPassword("password");

        when(mockLoginRepository.findAccountByUsername("username")).thenReturn(null);

           
        assertThatThrownBy(() -> contactServiceImplUnderTest.saveStaffAccount(loginRequest))
                .isInstanceOf(ClinicException.class);
    }

    @Test
    void testChangePW() {
           
        final ChangePwRequest changePwRequest = new ChangePwRequest();
        changePwRequest.setUsername("username");
        changePwRequest.setPassword("password");
        changePwRequest.setNewPassword("password");
        changePwRequest.setRenewPassword("renewPassword");

        final Optional<Account> account = Optional.of(account());
        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password")).thenReturn(account);

        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());

           
        contactServiceImplUnderTest.changePW(changePwRequest);

           
        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testChangePW_AuthenticationRepositoryFindAccountsByUsernameAndPasswordReturnsNull() {
           
        final ChangePwRequest changePwRequest = new ChangePwRequest();
        changePwRequest.setUsername("username");
        changePwRequest.setPassword("password");
        changePwRequest.setNewPassword("password");
        changePwRequest.setRenewPassword("renewPassword");

        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password"))
                .thenReturn(Optional.empty());
        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());

           
        contactServiceImplUnderTest.changePW(changePwRequest);

           
        verify(mockLoginRepository).save(any(Account.class));
    }

    @Test
    void testUpdateExaminationInfo() {
           
        final ContactAndRecordRequest contactAndRecordRequest = new ContactAndRecordRequest();
        contactAndRecordRequest.setAccountId(1);
        contactAndRecordRequest.setFullName("fullName");
        contactAndRecordRequest.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        contactAndRecordRequest.setAddress("address");
        contactAndRecordRequest.setVillage("village");
        contactAndRecordRequest.setDistrict("district");
        contactAndRecordRequest.setProvince("province");
        contactAndRecordRequest.setSex("sex");
        contactAndRecordRequest.setIdentityCard("identityCard");
        contactAndRecordRequest.setPhoneNumber("phoneNumber");
        contactAndRecordRequest.setEthnicity("ethnicity");
        contactAndRecordRequest.setJob("job");
        contactAndRecordRequest.setEmail("email");
        contactAndRecordRequest.setMedicalRecordId(1);
        contactAndRecordRequest.setCircuit(1);

           
        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

                  
        final Optional<MedicalRecord> medicalRecord = Optional.of(
                medicalRecord());
        when(mockMedicalRecordRepository.findById(1)).thenReturn(medicalRecord);

           
        final Contact contact1 = contact();
        when(mockModelMapper.map(any(Object.class), eq(Contact.class))).thenReturn(contact1);

           
        final Contact contact2 = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact2);

           
        final MedicalRecord medicalRecord1 = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord1);

           
        contactServiceImplUnderTest.updateExaminationInfo(contactAndRecordRequest);

           
    }

    @Test
    void testUpdateExaminationInfo_ContactRepositoryFindByIdReturnsAbsent() {
           
        final ContactAndRecordRequest contactAndRecordRequest = new ContactAndRecordRequest();
        contactAndRecordRequest.setAccountId(1);
        contactAndRecordRequest.setFullName("fullName");
        contactAndRecordRequest.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        contactAndRecordRequest.setAddress("address");
        contactAndRecordRequest.setVillage("village");
        contactAndRecordRequest.setDistrict("district");
        contactAndRecordRequest.setProvince("province");
        contactAndRecordRequest.setSex("sex");
        contactAndRecordRequest.setIdentityCard("identityCard");
        contactAndRecordRequest.setPhoneNumber("phoneNumber");
        contactAndRecordRequest.setEthnicity("ethnicity");
        contactAndRecordRequest.setJob("job");
        contactAndRecordRequest.setEmail("email");
        contactAndRecordRequest.setMedicalRecordId(1);
        contactAndRecordRequest.setCircuit(1);

        when(mockContactRepository.findById(1)).thenReturn(Optional.empty());

                  
        final Optional<MedicalRecord> medicalRecord = Optional.of(
                medicalRecord());
        when(mockMedicalRecordRepository.findById(1)).thenReturn(medicalRecord);

           
        assertThatThrownBy(
                () -> contactServiceImplUnderTest.updateExaminationInfo(contactAndRecordRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testUpdateExaminationInfo_MedicalRecordRepositoryFindByIdReturnsAbsent() {
           
        final ContactAndRecordRequest contactAndRecordRequest = new ContactAndRecordRequest();
        contactAndRecordRequest.setAccountId(1);
        contactAndRecordRequest.setFullName("fullName");
        contactAndRecordRequest.setDob(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        contactAndRecordRequest.setAddress("address");
        contactAndRecordRequest.setVillage("village");
        contactAndRecordRequest.setDistrict("district");
        contactAndRecordRequest.setProvince("province");
        contactAndRecordRequest.setSex("sex");
        contactAndRecordRequest.setIdentityCard("identityCard");
        contactAndRecordRequest.setPhoneNumber("phoneNumber");
        contactAndRecordRequest.setEthnicity("ethnicity");
        contactAndRecordRequest.setJob("job");
        contactAndRecordRequest.setEmail("email");
        contactAndRecordRequest.setMedicalRecordId(1);
        contactAndRecordRequest.setCircuit(1);

           
        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

        when(mockMedicalRecordRepository.findById(1)).thenReturn(Optional.empty());

           
        final Contact contact1 = contact();
        when(mockModelMapper.map(any(Object.class), eq(Contact.class))).thenReturn(contact1);

           
        final Contact contact2 = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact2);

           
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

           
        contactServiceImplUnderTest.updateExaminationInfo(contactAndRecordRequest);

           
    }

    @Test
    void testFindByEmail() {
           
           
        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findByEmail("email")).thenReturn(contact);

           
        final Optional<Contact> result = contactServiceImplUnderTest.findByEmail("email");

           
    }

    @Test
    void testFindByEmail_ContactRepositoryReturnsAbsent() {
           
        when(mockContactRepository.findByEmail("email")).thenReturn(Optional.empty());

           
        final Optional<Contact> result = contactServiceImplUnderTest.findByEmail("email");

           
        assertThat(result).isEmpty();
    }
}
