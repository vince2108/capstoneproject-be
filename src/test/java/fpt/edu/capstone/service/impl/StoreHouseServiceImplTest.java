package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.storeHouse.StoreHouseResponse;
import fpt.edu.capstone.entity.Branch;
import fpt.edu.capstone.entity.StoreHouse;
import fpt.edu.capstone.repository.BranchRepository;
import fpt.edu.capstone.repository.StaffRepository;
import fpt.edu.capstone.repository.StoreHouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StoreHouseServiceImplTest {

    @Mock
    private StoreHouseRepository mockStoreHouseRepository;
    @Mock
    private StaffRepository mockStaffRepository;
    @Mock
    private BranchRepository mockBranchRepository;

    private StoreHouseServiceImpl storeHouseServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        storeHouseServiceImplUnderTest = new StoreHouseServiceImpl(mockStoreHouseRepository, mockStaffRepository,
                mockBranchRepository);
    }
    public StoreHouse storeHouse(){
        storeHouse().setStoreHouseId(1);
        storeHouse().setStoreHouseName("storeHouseName");
        storeHouse().setBranchId(1);
        storeHouse().setAddress("address");
        storeHouse().setAction(Boolean.valueOf("false"));
        storeHouse().setLocation("location");
        storeHouse().setAbbreviationName("abbreviationName");
        return storeHouse();
    }
    public Branch branch(){
        branch().setBranchId(1);
        branch().setBranchName("branchName");
        branch().setAction(Boolean.valueOf("false"));
        branch().setPhoneNumber("phoneNumber");
        branch().setAddress("address");
        return branch();
    }
    @Test
    void testCreateStoreHouse() {
                 
        final StoreHouse storeHouse = storeHouse();

                 
        final List<StoreHouse> storeHouses = Arrays.asList(
                storeHouse());
        when(mockStoreHouseRepository.getByStoreName("storeHouseName", "abbreviationName")).thenReturn(storeHouses);

                 
        final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

                 
        final StoreHouse storeHouse1 = storeHouse();
        when(mockStoreHouseRepository.save(any(StoreHouse.class))).thenReturn(storeHouse1);

                 
        storeHouseServiceImplUnderTest.createStoreHouse(storeHouse);

                 
        verify(mockStoreHouseRepository).save(any(StoreHouse.class));
    }

    @Test
    void testCreateStoreHouse_StoreHouseRepositoryGetByStoreNameReturnsNoItems() {
                 
        final StoreHouse storeHouse = storeHouse();
        when(mockStoreHouseRepository.getByStoreName("storeHouseName", "abbreviationName"))
                .thenReturn(Collections.emptyList());

                 
        final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

                 
        final StoreHouse storeHouse1 = storeHouse();
        when(mockStoreHouseRepository.save(any(StoreHouse.class))).thenReturn(storeHouse1);

                 
        storeHouseServiceImplUnderTest.createStoreHouse(storeHouse);

                 
        verify(mockStoreHouseRepository).save(any(StoreHouse.class));
    }

    @Test
    void testCreateStoreHouse_BranchRepositoryReturnsAbsent() {
                 
        final StoreHouse storeHouse = storeHouse();

                 
        final List<StoreHouse> storeHouses = Arrays.asList(
                storeHouse());
        when(mockStoreHouseRepository.getByStoreName("storeHouseName", "abbreviationName")).thenReturn(storeHouses);

        when(mockBranchRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> storeHouseServiceImplUnderTest.createStoreHouse(storeHouse))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testUpdateStoreHouse() {
                 
        final StoreHouse storeHouse = storeHouse();

                 
        final Optional<StoreHouse> storeHouse1 = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.findByStoreHouseId(1)).thenReturn(storeHouse1);

                 
        final List<StoreHouse> storeHouses = Arrays.asList(
                storeHouse());
        when(mockStoreHouseRepository.getByStoreNameN("storeHouseName", "abbreviationName", 1)).thenReturn(storeHouses);

                 
        final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

                 
        final StoreHouse storeHouse2 = storeHouse();
        when(mockStoreHouseRepository.save(any(StoreHouse.class))).thenReturn(storeHouse2);

                 
        storeHouseServiceImplUnderTest.updateStoreHouse(storeHouse);

                 
        verify(mockStoreHouseRepository).save(any(StoreHouse.class));
    }

    @Test
    void testUpdateStoreHouse_StoreHouseRepositoryFindByStoreHouseIdReturnsAbsent() {
                 
        final StoreHouse storeHouse = storeHouse();
        when(mockStoreHouseRepository.findByStoreHouseId(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> storeHouseServiceImplUnderTest.updateStoreHouse(storeHouse))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testUpdateStoreHouse_StoreHouseRepositoryGetByStoreNameNReturnsNoItems() {
                 
        final StoreHouse storeHouse = storeHouse();

                 
        final Optional<StoreHouse> storeHouse1 = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.findByStoreHouseId(1)).thenReturn(storeHouse1);

        when(mockStoreHouseRepository.getByStoreNameN("storeHouseName", "abbreviationName", 1))
                .thenReturn(Collections.emptyList());

                 
        final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

                 
        final StoreHouse storeHouse2 = storeHouse();
        when(mockStoreHouseRepository.save(any(StoreHouse.class))).thenReturn(storeHouse2);

                 
        storeHouseServiceImplUnderTest.updateStoreHouse(storeHouse);

                 
        verify(mockStoreHouseRepository).save(any(StoreHouse.class));
    }

    @Test
    void testUpdateStoreHouse_BranchRepositoryReturnsAbsent() {
                 
        final StoreHouse storeHouse = storeHouse();

                 
        final Optional<StoreHouse> storeHouse1 = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.findByStoreHouseId(1)).thenReturn(storeHouse1);

                 
        final List<StoreHouse> storeHouses = Arrays.asList(
                storeHouse());
        when(mockStoreHouseRepository.getByStoreNameN("storeHouseName", "abbreviationName", 1)).thenReturn(storeHouses);

        when(mockBranchRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> storeHouseServiceImplUnderTest.updateStoreHouse(storeHouse))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetAllStoreHouse() {
                 
                 
        final List<StoreHouse> storeHouses = Arrays.asList(
                storeHouse());
        when(mockStoreHouseRepository.getAllStoreHouse()).thenReturn(storeHouses);

                 
        final List<StoreHouse> result = storeHouseServiceImplUnderTest.getAllStoreHouse();

                 
    }

    @Test
    void testGetAllStoreHouse_StoreHouseRepositoryReturnsNoItems() {
                 
        when(mockStoreHouseRepository.getAllStoreHouse()).thenReturn(Collections.emptyList());

                 
        final List<StoreHouse> result = storeHouseServiceImplUnderTest.getAllStoreHouse();

                 
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetStoreHouseById() {
                 
                 
        final Optional<StoreHouse> storeHouse = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.getByIdStoreHouse(1)).thenReturn(storeHouse);

                 
        final StoreHouse result = storeHouseServiceImplUnderTest.getStoreHouseById(1);

                 
    }

    @Test
    void testGetStoreHouseById_StoreHouseRepositoryReturnsAbsent() {
                 
        when(mockStoreHouseRepository.getByIdStoreHouse(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> storeHouseServiceImplUnderTest.getStoreHouseById(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testDeleteStore() {
                 
                 
        final Optional<StoreHouse> storeHouse = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.getByIdStoreHouse(1)).thenReturn(storeHouse);

                 
        storeHouseServiceImplUnderTest.deleteStore(1);

                 
        verify(mockStoreHouseRepository).deleteByIdStoreHouse(1);
    }

    @Test
    void testDeleteStore_StoreHouseRepositoryGetByIdStoreHouseReturnsAbsent() {
                 
        when(mockStoreHouseRepository.getByIdStoreHouse(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> storeHouseServiceImplUnderTest.deleteStore(1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStoreHouseOverview() {
                 
                 
        final StoreHouseResponse result = storeHouseServiceImplUnderTest.storeHouseOverview();

                 
    }

    @Test
    void testGetStoreHouseByBranchId() {
                 
                 
        final List<StoreHouse> storeHouses = Arrays.asList(
                storeHouse());
        when(mockStoreHouseRepository.getStoreHouseByIdBranch(1)).thenReturn(storeHouses);

                 
        final List<StoreHouse> result = storeHouseServiceImplUnderTest.getStoreHouseByBranchId(1);

                 
    }

    @Test
    void testGetStoreHouseByBranchId_StoreHouseRepositoryReturnsNoItems() {
                 
        when(mockStoreHouseRepository.getStoreHouseByIdBranch(1)).thenReturn(Collections.emptyList());

                 
        final List<StoreHouse> result = storeHouseServiceImplUnderTest.getStoreHouseByBranchId(1);

                 
        assertThat(result).isEqualTo(Collections.emptyList());
    }
}
