package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.unit.UnitsRequest;
import fpt.edu.capstone.dto.unitgroup.UnitRequest;
import fpt.edu.capstone.dto.unitgroup.UnitResponse;
import fpt.edu.capstone.entity.Unit;
import fpt.edu.capstone.entity.UnitGroup;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.UnitGroupRepository;
import fpt.edu.capstone.repository.UnitRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UnitServiceImplTest {

    @Mock
    private UnitRepository mockUnitRepository;
    @Mock
    private UnitGroupRepository mockUnitGroupRepository;
    @Mock
    private ModelMapper mockModelMapper;

    private UnitServiceImpl unitServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        unitServiceImplUnderTest = new UnitServiceImpl(mockUnitRepository, mockUnitGroupRepository, mockModelMapper);
    }
    public Unit unit(){
        unit().setUnitId(1);
        unit().setUnitName("unitName");
        unit().setUnitGroupId(1);
        unit().setUse(Boolean.valueOf("false"));
        return unit();
    }
    @Test
    void testGetListUnit() {
             
             
        final List<Unit> list = Arrays.asList(unit());
        when(mockUnitRepository.getAllUnit()).thenReturn(list);

            
        final UnitResponse unitResponse = new UnitResponse();
        unitResponse.setUnitId(1);
        unitResponse.setUnitName("unitName");
        unitResponse.setUnitGroupId(1);
        unitResponse.setUnitGroupName("name");
        when(mockModelMapper.map(any(Object.class), eq(UnitResponse.class))).thenReturn(unitResponse);

        when(mockUnitGroupRepository.getById(1)).thenReturn(new UnitGroup(1, "name", false));

             
        final List<UnitResponse> result = unitServiceImplUnderTest.getListUnit();

             
    }

    @Test
    void testGetListUnit_UnitRepositoryReturnsNoItems() {
             
        when(mockUnitRepository.getAllUnit()).thenReturn(Collections.emptyList());

            
        final UnitResponse unitResponse = new UnitResponse();
        unitResponse.setUnitId(1);
        unitResponse.setUnitName("unitName");
        unitResponse.setUnitGroupId(1);
        unitResponse.setUnitGroupName("name");
        when(mockModelMapper.map(any(Object.class), eq(UnitResponse.class))).thenReturn(unitResponse);

        when(mockUnitGroupRepository.getById(1)).thenReturn(new UnitGroup(1, "name", false));

             
        final List<UnitResponse> result = unitServiceImplUnderTest.getListUnit();

             
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testDeleteUnit() {
             
        final UnitsRequest unitsRequest = new UnitsRequest();
        unitsRequest.setIdUnits(Arrays.asList(1));

             
        final Optional<Unit> unit = Optional.of(unit());
        when(mockUnitRepository.findById(1)).thenReturn(unit);

             
        unitServiceImplUnderTest.deleteUnit(unitsRequest);

             
        verify(mockUnitRepository).deleteUnitById(1);
    }

    @Test
    void testDeleteUnit_UnitRepositoryFindByIdReturnsAbsent() {
             
        final UnitsRequest unitsRequest = new UnitsRequest();
        unitsRequest.setIdUnits(Arrays.asList(1));

        when(mockUnitRepository.findById(1)).thenReturn(Optional.empty());

             
        assertThatThrownBy(() -> unitServiceImplUnderTest.deleteUnit(unitsRequest)).isInstanceOf(ClinicException.class);
    }

    @Test
    void testCreateUnit() {
             
        final UnitRequest request = new UnitRequest();
        request.setUnitName("unitName");
        request.setUnitGroupId(1);

             
        final Optional<Unit> unit = Optional.of(unit());
        when(mockUnitRepository.findByUnitNameAndUse("unitName", true)).thenReturn(unit);

             
        final Optional<UnitGroup> unitGroup = Optional.of(new UnitGroup(1, "name", false));
        when(mockUnitGroupRepository.findById(1)).thenReturn(unitGroup);

        when(mockModelMapper.map(any(Object.class), eq(Unit.class))).thenReturn(unit());
        when(mockUnitRepository.save(any(Unit.class))).thenReturn(unit());

             
        unitServiceImplUnderTest.createUnit(request);

             
        verify(mockUnitRepository).save(any(Unit.class));
    }

    @Test
    void testCreateUnit_UnitRepositoryFindByUnitNameAndUseReturnsAbsent() {
             
        final UnitRequest request = new UnitRequest();
        request.setUnitName("unitName");
        request.setUnitGroupId(1);

        when(mockUnitRepository.findByUnitNameAndUse("unitName", true)).thenReturn(Optional.empty());

             
        final Optional<UnitGroup> unitGroup = Optional.of(new UnitGroup(1, "name", false));
        when(mockUnitGroupRepository.findById(1)).thenReturn(unitGroup);

        when(mockModelMapper.map(any(Object.class), eq(Unit.class))).thenReturn(unit());
        when(mockUnitRepository.save(any(Unit.class))).thenReturn(unit());

             
        unitServiceImplUnderTest.createUnit(request);

             
        verify(mockUnitRepository).save(any(Unit.class));
    }

    @Test
    void testCreateUnit_UnitGroupRepositoryReturnsAbsent() {
             
        final UnitRequest request = new UnitRequest();
        request.setUnitName("unitName");
        request.setUnitGroupId(1);

             
        final Optional<Unit> unit = Optional.of(unit());
        when(mockUnitRepository.findByUnitNameAndUse("unitName", true)).thenReturn(unit);

        when(mockUnitGroupRepository.findById(1)).thenReturn(Optional.empty());

             
        assertThatThrownBy(() -> unitServiceImplUnderTest.createUnit(request)).isInstanceOf(ClinicException.class);
    }

    @Test
    void testGetUnitByUnitGroupId() {
             
             
        final List<Unit> list = Arrays.asList(unit());
        when(mockUnitRepository.getUnitByUnitGroupId(1)).thenReturn(list);

             
        final List<Unit> result = unitServiceImplUnderTest.getUnitByUnitGroupId(1);

             
    }

    @Test
    void testGetUnitByUnitGroupId_UnitRepositoryReturnsNoItems() {
             
        when(mockUnitRepository.getUnitByUnitGroupId(1)).thenReturn(Collections.emptyList());

             
        final List<Unit> result = unitServiceImplUnderTest.getUnitByUnitGroupId(1);

             
        assertThat(result).isEqualTo(Collections.emptyList());
    }
}
