package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.transfer.*;
import fpt.edu.capstone.service.TransferService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class TransferController {
    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    private final TransferService transferService;

    @Operation(summary = "tạo phiếu điều chuyển ")
    @PostMapping("create-transfer")
    public ResponseData createTransfer(@RequestBody TransferRequest transferRequest){
        try{
            transferService.createTransfer(transferRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

    @Operation(summary = "lấy tất cả phiếu điều chuyển nhập ")
    @GetMapping("all-transfer-receipt")
    public ResponseData getAllTransferReceipt(){
        try{
            List<ITransferReceiptResponse> transferResponses = transferService.getTransfersReceipt();
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, transferResponses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
    @Operation(summary = "lấy tất cả phiếu điều chuyển nhập theo stauts")
    @GetMapping("transfer-receipt-by-id")
    public ResponseData getAllTransferReceipt(@RequestParam(name = "statusId", required = false) Integer idStatus){
        try{
            List<ITransferReceiptResponse> transferResponses = transferService.getTransfersReceipt(idStatus);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, transferResponses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
    @Operation(summary = "lấy tất cả phiếu điều chuyển xuất ")
    @GetMapping("all-transfer-export")
    public ResponseData getAllTransferExport(){
        try{
            List<ITransferExportResponse> transferResponses = transferService.getTransfersExport();
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, transferResponses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
    @Operation(summary = "lấy tất cả phiếu điều chuyển xuất ")
    @GetMapping("transfer-export-by-id")
    public ResponseData getAllTransferExportByStatus(@RequestParam(name = "statusId", required = false) Integer idStatus){
        try{
            List<ITransferExportResponse> transferResponses = transferService.getTransfersExport(idStatus);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, transferResponses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

    @Operation(summary = "lấy chi thiết phiếu điều chuyển xuất (status san sang) ")
    @GetMapping("transfer-short-export")
    public ResponseData getTransferShExportById(@RequestParam(name = "idExport")int idTransfer){
        try{
           TransferShExportResponse transferShExportResponse = transferService.getTransferShExportById(idTransfer);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, transferShExportResponse);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

    @Operation(summary = "lấy chi thiết phiếu điều chuyển xuất (status hoan thanh) ")
    @GetMapping("transfer-full-export")
    public ResponseData getTransferFullExportById(@RequestParam(name = "idExport")int idTransfer){
        try{
            TransferFullExportResponse transferFuExportResponse = transferService.getTransferFullExportById(idTransfer);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, transferFuExportResponse);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
    @Operation(summary = "lấy chi thiết phiếu điều chuyển Nhập (status hoan thanh) ")
    @GetMapping("transfer-full-receipt")
    public ResponseData getTransferFullReceiptById(@RequestParam(name = "idReceipt")int idTransfer){
        try{
            TransferFullReceiptResponse transferFuReceiptResponse = transferService.getTransferFullReceiptById(idTransfer);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, transferFuReceiptResponse);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

    @Operation(summary = "lấy chi thiết phiếu điều chuyển xuất (status san sang) ")
    @GetMapping("transfer-short-receipt")
    public ResponseData getTransferShReceiptById(@RequestParam(name = "idReceipt")int idTransfer){
        try{
            TransferShReceiptResponse transferShReceiptById = transferService.getTransferShReceiptById(idTransfer);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, transferShReceiptById);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }


    @Operation(summary = "Thực hiện điều chuyển nhập sản phẩm")
    @PostMapping("action-transfer-export")
    public ResponseData getTransferById(@RequestBody TransferExportProductRequest exportProductRequest){
        try{
            transferService.actionExportTransfer(exportProductRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
}
