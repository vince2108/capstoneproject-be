package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.equipment.EquipmentTranfer;
import fpt.edu.capstone.dto.equipment.EquipmentTypeResponse;
import fpt.edu.capstone.dto.medicine.MedicineTranfer;
import fpt.edu.capstone.dto.product.*;
import fpt.edu.capstone.dto.medicine.MedicineTypeResponse;
import fpt.edu.capstone.dto.productGroup.IProductGroupResponse;
import fpt.edu.capstone.dto.productGroup.PrGroupUpRequest;
import fpt.edu.capstone.dto.productGroup.ProductGroupRequest;
import fpt.edu.capstone.dto.vnpay.PaymentDTO;
import fpt.edu.capstone.dto.vnpay.PaymentResponseDTO;
import fpt.edu.capstone.entity.*;
import fpt.edu.capstone.service.ProductService;
import fpt.edu.capstone.service.ProductTypeService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class ProductController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    private final ProductService productService;

    @PostMapping("/create-product-group")
    public ResponseData createProductGroup(@RequestBody ProductGroupRequest productGroupRequest) {
        try {
            productService.createProductGroup(productGroupRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @GetMapping("/get-all-product-group")
    public ResponseData GetAllProductGroup() {
        try {
            List<IProductGroupResponse> productGroups = productService.getAllProductGroup();
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS, productGroups);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PutMapping("/update-product-group")
    public ResponseData updateProductGroup(@RequestBody PrGroupUpRequest prGroupUpRequest) {
        try {
            productService.updateProductGroup(prGroupUpRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @DeleteMapping("/delete-product-group")
    public ResponseData deleteProductGroup(@RequestParam (name = "idProductGroup", defaultValue = "0") int idProductGroup) {
        try {
            productService.deleteProduct(idProductGroup);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PostMapping("/create-product")
    public ResponseData createProduct(@RequestBody ProductRequest productRequest) {
        try {
            productService.createProduct(productRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }


    @GetMapping("/get-all-product")
    public ResponseData GetAllProduct() {
        try {
            List<ProductResponse> productResponses = productService.getAllProduct();
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS, productResponses);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
