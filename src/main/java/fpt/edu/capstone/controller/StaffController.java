package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.account.ChangePwRequest;
import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.staff.*;
import fpt.edu.capstone.entity.Account;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.service.ContactService;
import fpt.edu.capstone.service.StaffService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class StaffController {
    private static final Logger logger = LoggerFactory.getLogger(StaffController.class);
    private final ContactService contactService;
    private final StaffService staffService;
    //lấy ra danh sách nhan vien
    @GetMapping("/staff")
    public ResponseData getListStaff(){
        try {
            List<Contact> contactList = contactService.findStaffByRoleId();
            if(contactList.isEmpty() || contactList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contactList);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @GetMapping("/staff-for-change-position")
    public ResponseData getListStaffForChangePosition(){
        try {
            List<StaffForChangePosition> listStaff = staffService.getAllStaffForChange();
            if(listStaff.isEmpty() || listStaff == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, listStaff);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    //lấy ra thông tin 1 nhân viên
    @GetMapping("/staff/{id}")
    public ResponseData getProfileStaff(@PathVariable(value = "id") int accId){
        try {
            StaffResponse StaffResponse = contactService.findStaffByRoleIdAndAccountId(accId);
            if(StaffResponse == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, StaffResponse);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @GetMapping("/staffNameInConsultingRoom/{id}")
    public ResponseData getAllStaffName(@PathVariable(value = "id") int consultingRoomId){
        try {
            List<StaffName_ConsultingRoom> staffName_consultingRoomList = staffService.getAllStaffName(consultingRoomId);
            if(staffName_consultingRoomList.isEmpty() || staffName_consultingRoomList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, staffName_consultingRoomList);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PutMapping("/delete-staff/{id}")
    public ResponseData deleteStaff(@PathVariable(value = "id") int staffId){
        try {
            contactService.deleteStaff(staffId);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    //dang loi
    //lay ra 1 list nhan vien theo name
    @GetMapping("/staffByName")
    public ResponseData searchStaff(@RequestParam(name="fullName", defaultValue=StringUtils.EMPTY) String fullName){
        try {
            List<Contact> contactList = contactService.findStaffByName(fullName);
            if(contactList.isEmpty() || contactList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contactList);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
//    //Xoa 1 nhan vien
//    //False do khoa chinh khoa phu voi bang Staff
//    @DeleteMapping("/staff/{id}")
//    public ResponseData deleteStaff(@PathVariable(value = "id") int accId){
//        try {
//            Contact contact = contactService.findStaffByRoleIdAndAccountId(accId);
//            if (contact != null){
//                contactService.deleteStaff(accId);
//            }
//            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,contact);
//        } catch (Exception e){
//            String msg = LogUtils.printLogStackTrace(e);
//            logger.error(msg);
//            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
//        }
//    }
    //tao moi nhan vien
    @PostMapping("/staff")
    public ResponseData saveProfileStaff(@RequestBody @Valid StaffRequest staffRequest){

        try {

            contactService.ContactRequestToStaff(staffRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PostMapping("/inventory-staff")
    public ResponseData saveInventoryStaff(@RequestBody @Valid InventoryStaff inventoryStaff){
        try {
            contactService.ContactRequestToInventoryStaff(inventoryStaff);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PutMapping("/update-staff-position")
    public ResponseData updateStaffPosition(@RequestBody @Valid Staff_ConsultingRoomRequestTo staff_consultingRoomRequestTo){

        try {

            staffService.updateStaffPosition(staff_consultingRoomRequestTo);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PostMapping("/changePassword")
    public ResponseData changePW(@RequestBody ChangePwRequest changePwRequest) {
        try {
            contactService.changePW(changePwRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
    //update profile staff
    @PutMapping("/staff/{id}")
    public ResponseData updateProfileStaff(@RequestBody @Valid ContactRequest contactRequest,@PathVariable(value = "id") int accountId){
        try {
            Contact contact = contactService.updateStaff(accountId,contactRequest);
            if(contact == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contact);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PutMapping("/saveAccount")
    public ResponseData saveAccount(@RequestBody @Valid LoginRequest loginRequest){

        try {
            contactService.saveStaffAccount(loginRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PostMapping("/insert-image/{staffId}")
    @Operation(summary = "cập nhật ảnh đại diện cho nhân viên")
    public ResponseData insertImage(@RequestBody AvatarRequest request, @PathVariable int staffId){
        try {
            Staff staff = staffService.findStaffByStaffId(staffId);
            staff.setAvatar(request.getAvatar());
            staffService.save(staff);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, staff);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

}
