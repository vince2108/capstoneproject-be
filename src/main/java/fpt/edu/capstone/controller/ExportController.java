package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.export.*;
import fpt.edu.capstone.dto.product.IProduct;
import fpt.edu.capstone.dto.receipt.IShortReceipt;
import fpt.edu.capstone.dto.receipt.ReceiptRequest;
import fpt.edu.capstone.service.ExportService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.pdfbox.tools.ExportXFDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class ExportController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    private ExportService exportService;

    @Operation(summary = "1. create export")
    @PostMapping("/create-export")
    public ResponseData createExport(@RequestBody ExportRequest exportRequest) {
        try {
            exportService.createExport(exportRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "2.get export by id branch")
    @GetMapping("/get-export-by-branch")
    public ResponseData getAllExportByIdBranch(@RequestParam("idBranch") int idBranch) {
        try {
            List<IShortExport> iShortExports = exportService.getAllExport(idBranch);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                   iShortExports);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @Operation(summary = "2.1get export by id branch and status")
    @GetMapping("/get-export-by-status")
    public ResponseData getExportByBranchAndStatus(@RequestParam("idBranch") int idBranch, @RequestParam("idStatus")int idStatus) {
        try {
            List<IShortExport> iShortExports = exportService.getExportByStatus(idBranch,idStatus);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    iShortExports);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "lấy ra tất cả sản phẩm, sô lượng còn lại trong kho")
    @GetMapping("/products-quantity-by-id")
    public ResponseData getQuantityProducts(@RequestParam(name = "idStoreHouse") int idStoreHouse) {
        try {
            List<IProductQuantity> iProductQuantities =exportService.getProductsQuantityById(idStoreHouse);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, iProductQuantities);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @Operation(summary = "lấy ra sản phẩm, sô lượng còn lại trong kho")
    @GetMapping("/product-quantity")
    public ResponseData getQuantityProduct(@RequestParam(name = "idStoreHouse") int idStoreHouse,
                                           @RequestParam(name = "idProduct")int idProduct) {
        try {
            IProductQuantity iProductQuantities =exportService.getProductQuantityVyId(idStoreHouse, idProduct);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, iProductQuantities);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @Operation(summary = "lấy ra tất cả sản phẩm trong kho")
    @GetMapping("/products-in-store")
    public ResponseData getProductInStoreHouse(@RequestParam(name = "idStoreHouse") int idStoreHouse) {
        try {
            List<IProduct> iProducts =exportService.getProductsByIdStore(idStoreHouse);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, iProducts);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @Operation(summary = "3.2.3 Xuất kho sản phẩm")
    @PostMapping("/export-products")
    public ResponseData createExportProducts(@RequestBody ExportProductsRequest exportProductsRequest) {
        try {
            exportService.exportProducts(exportProductsRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }


    @Operation(summary = "Get export detail where status hoàn thành")
    @GetMapping("/full-export-detail")
    public ResponseData getFullExportDetail(@RequestParam(name = "idExport") int idExport) {
        try {
            ExportDetailResponse exportDetailResponse = exportService.getExportDetailById(idExport);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    exportDetailResponse);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @Operation(summary = "Get export detail where status sẵn sàng")
    @GetMapping("/short-export-detail")
    public ResponseData getShortExportDetail(@RequestParam(name = "idExport") int idExport) {
        try {
            ExportShortResponse exportDetailResponse = exportService.getExportShortById(idExport);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    exportDetailResponse);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
