package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.report.RevenueReport;
import fpt.edu.capstone.service.ReportService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class ReportController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    private final ReportService reportService;

    @GetMapping("/revenue-report")
    public ResponseData getRevenueReportInBranch(@RequestParam(name = "branchId") int branchId,
                                                 @RequestParam(name = "month") int month,
                                                 @RequestParam(name = "year") int year) {
        try {
            List<RevenueReport> revenueReportList = reportService.getAllRevenueInBranch(branchId,month , year);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS, revenueReportList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/report-patient-examined")
    public ResponseData getReportPatientExaminedInBranch(@RequestParam(name = "branchId") int branchId,
                                                         @RequestParam(name = "month") int month,
                                                         @RequestParam(name = "year") int year) {
        try {

            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getPatientExamined(month, year, branchId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/report-medical-visits")
    public ResponseData getReportMedicalVisitsInBranch(@RequestParam(name = "branchId") int branchId,
                                                       @RequestParam(name = "month") int month,
                                                       @RequestParam(name = "year") int year) {
        try {

            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getMedicalVisits(month, year, branchId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/revenue-report-day")
    public ResponseData getRevenueReportInBranch(@RequestParam(name = "branchId") int branchId,
                                                 @RequestParam(name = "month") int month,
                                                 @RequestParam(name = "year") int year,
                                                 @RequestParam(name = "day") int day) {
        try {
            List<RevenueReport> revenueReportList = reportService.getAllRevenueInBranch(day, branchId, month , year);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS, revenueReportList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/report-patient-examined-day")
    public ResponseData getReportPatientExaminedInBranch(@RequestParam(name = "branchId") int branchId,
                                                         @RequestParam(name = "month") int month,
                                                         @RequestParam(name = "year") int year,
                                                         @RequestParam(name = "day") int day) {
        try {

            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getPatientExamined(day, month, year, branchId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/report-medical-visits-day")
    public ResponseData getReportMedicalVisitsInBranch(@RequestParam(name = "branchId") int branchId,
                                                       @RequestParam(name = "month") int month,
                                                       @RequestParam(name = "year") int year,
                                                       @RequestParam(name = "day") int day) {
        try {

            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getMedicalVisits(day, month, year, branchId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
