package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.patient.MedicalRecordResponse;
import fpt.edu.capstone.dto.patient.PatientResponse;
import fpt.edu.capstone.dto.service.RServiceDTO;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.service.ContactService;
import fpt.edu.capstone.service.ExaminationService;
import fpt.edu.capstone.service.MedicalServiceService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class DoctorController {
    private static final Logger logger = LoggerFactory.getLogger(DoctorController.class);
    private final MedicalServiceService medicalServiceService;
    private final ContactService contactService;
    private final ExaminationService examinationService;

    //false
    //get all patient dang o phong nao
    @Operation(summary = "get all patient by consultingRoom")
    @GetMapping("patient-consulting-room")
    public ResponseData getContactsByConsultingId(@RequestParam(name = "consultingId") int consultingId,
                                                  @RequestParam(name = "idBranch")int idBranch) {
        try {
            List<PatientResponse> patientResponseList = examinationService.findPatientByConsultingRoom(consultingId, idBranch);
            if (patientResponseList.isEmpty() || patientResponseList == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    patientResponseList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "get all patient by consultingRoom")
    @GetMapping("patient-consulting-room-status")
    public ResponseData getContactsByConsultingIdAndStatus(@RequestParam(name = "consultingId") int consultingId,
                                                  @RequestParam(name = "idBranch")int idBranch) {
        try {
            List<PatientResponse> patientResponseList = examinationService.findPatientByConsultingRoomByStatus(consultingId, idBranch);
            if (patientResponseList.isEmpty() || patientResponseList == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    patientResponseList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    //get all patient
//    @Operation(summary = "get all patient in all consulting return(contact+medicalReport)")
//    @GetMapping("all-patient")
//    public ResponseData getAllPatient() {
//        try {
//            List<PatientResponse> patientResponseList = contactService.getAllPatient();
//            if (patientResponseList.isEmpty() || patientResponseList == null) {
//                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
//            }
//            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
//                    patientResponseList);
//        } catch (Exception e) {
//            String msg = LogUtils.printLogStackTrace(e);
//            logger.error(msg);
//            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
//        }
//    }

    //get patient by idAccount return(contact+medicalRecord)
    @Operation(summary = "get patient by idAccount return(contact+medicalRecord)")
    @GetMapping("find-patient")
    public ResponseData getPatientByPatientId(@RequestParam(name = "patientId") int patientId) {
        try {
            MedicalRecordResponse response = contactService.findPatientById(patientId);
            if (response == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, response);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "Update medicalRecord, medicalReport,...")
    @PutMapping("add-service")
    public ResponseData addMedicalReportService(@RequestParam(name = "idPatient") int idPatient,
                                                @RequestParam(name = "idThisConsulting") int idThisConsulting,
                                                @RequestParam(name = "note") String note,
                                                @RequestParam(name = "sttThis") int sttThis,
                                                @RequestParam(name = "idThatConsulting") int idThatConsulting,
                                                @RequestParam(name = "idThatService") int idThatService) {
        try {
            examinationService.addPatientService(idPatient, note, idThisConsulting, sttThis, idThatConsulting, idThatService);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, null);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "Create contact, create medicalRecord, medicalReport,...")
    @PostMapping("start-examination-create")
    public ResponseData startExaminationCreate(@RequestBody ContactRequest contactRequest,
                                               @RequestParam(name = "allergies") String allergies,
                                               @RequestParam(name = "pastMedicalHistory") String pastMedicalHistory,
                                               @RequestParam(name = "idService") int idService,
                                               @RequestParam(name = "idConsultingRoom") int idConsultingRoom,
                                               @RequestParam(name = "idStaff") int idStaff,
                                               @RequestParam(name = "idBranch") int idBranch) {
        try {
            Contact contact = examinationService.startExaminationCreate(contactRequest, allergies, pastMedicalHistory
                    , idService, idConsultingRoom, idStaff, idBranch);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,  contact);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "Update contact, create medicalRecord, medicalReport,...")
    @PostMapping("start-examination-update")
    public ResponseData startExaminationUpdate(@RequestParam(name = "idPatient") int idPatient,
                                               @RequestBody ContactRequest contactRequest,
                                               @RequestParam(name = "allergies") String allergies,
                                               @RequestParam(name = "pastMedicalHistory") String pastMedicalHistory,
                                               @RequestParam(name = "idService") int idService,
                                               @RequestParam(name = "idConsultingRoom") int idConsultingRoom,
                                               @RequestParam(name = "idStaff") int idStaff,
                                               @RequestParam(name = "idBranch") int idBranch) {
        try {
            Contact contact = examinationService.startExaminationUpdate(idPatient, contactRequest, allergies, pastMedicalHistory
                    , idService, idConsultingRoom, idStaff, idBranch);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contact);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "Update isFinish + medicalRecord ")
    @PutMapping("finish-examination")
    public ResponseData finishedExamination(@RequestParam(name = "idPatient") int idPatient,
                                            @RequestParam(name = "idConsulting") int idConsulting,
                                            @RequestParam(name = "note") String note) {
        try {
            examinationService.finishExamination(idPatient, idConsulting, note);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, null);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "update trạng thái đang kha cho bệnh nhân khi nhấn khám bệnh")
    @PutMapping("update-status-examination")
    public ResponseData updateStatusExamination(@RequestParam(name = "idPatient") int idPatient,
                                                @RequestParam(name = "idConsulting") int idConsulting) {
        try {
            examinationService.changeStatusExamination(idConsulting, idPatient);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "Khi bệnh nhân không có mặt STT cuối cùng")
    @PutMapping("update-index-patient")
    public ResponseData updateIndexPatient(@RequestParam(name = "idPatient") int idPatient,
                                           @RequestParam(name = "idConsulting") int idConsulting,
                                           @RequestParam(name = "stt") int stt) {
        try {
            examinationService.updateIndexPatient(idPatient, idConsulting, stt);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "Khi chọn dịch vụ, chuyển bệnh nhân về trạng thái chờ lấy kết quả và chuyển nó đén phòng dịch vụ")
    @PutMapping("change-room-service")
    public ResponseData addPatientIntoRoomService(@RequestBody List<RServiceDTO> rServiceDTOS) {
        try {
            examinationService.addSTT2(rServiceDTOS);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "Kết thúc khám ở phòng dịch vụ")
    @PutMapping("finish-examination-service")
    public ResponseData finishPatientServiceExamination(@RequestParam(name = "idPatient") int idPatient,
                                                        @RequestParam(name = "idConsulting") int idConsulting) {
        try {
            examinationService.finishServiceExamination(idPatient, idConsulting);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

}
