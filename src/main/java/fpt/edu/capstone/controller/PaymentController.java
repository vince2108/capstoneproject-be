package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.medicalreportservice.IReportServiceResponse;
import fpt.edu.capstone.dto.medicalreportservice.ServicesRequest;
import fpt.edu.capstone.dto.service.ReportServiceRequest;
import fpt.edu.capstone.dto.vnpay.PaymentDTO;
import fpt.edu.capstone.dto.vnpay.PaymentResponseDTO;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.service.ContactService;
import fpt.edu.capstone.service.MedicalRecordService;
import fpt.edu.capstone.service.PaymentService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
//Liên quan đến màn thanh toán
public class PaymentController {
    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    private final MedicalRecordService medicalRecordService;
    private final ContactService contactService;
    private final PaymentService paymentService;

    @PostMapping("/create-url-payment")
    public ResponseData createPayment(@RequestBody PaymentDTO paymentDTO) {
        try {
            PaymentResponseDTO dto = paymentService.getPaymentVNPay(paymentDTO);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.PAYMENT_SUCCESS, dto);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PostMapping("/save-payment-success")
    public ResponseData savePaymentSuccess(@RequestParam("vnp_ResponseCode") String vnpResponseCode,
                                           @RequestParam("vnp_OrderInfo") String vnpOrderInfo) {
        try {
            paymentService.savePayment(vnpResponseCode, vnpOrderInfo);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.PAYMENT_SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "lấy dịch vụ khám bệnh theo id bệnh nhân  và phiếu khám mới nhất")
    @GetMapping("patient-used-service")
    public ResponseData getReportServicePatient(@RequestParam(name = "idPatient") int idPatient){
        try{
            List<IReportServiceResponse> responses = medicalRecordService.getReportServiceByIdPatient(idPatient);
            if(responses.isEmpty() || responses == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, responses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
    @Operation(summary = "lấy dịch vụ khám bệnh theo id bệnh nhân  và phiếu khám mới nhất")
    @GetMapping("patient-used-service-consulting")
    public ResponseData getReportServicePatientByConsultingRoom(@RequestParam(name = "idPatient") int idPatient,
                                                @RequestParam(name = "idConsulting") int idConsulting){
        try{
            List<IReportServiceResponse> responses = medicalRecordService.getServiceByIdPatientInConsulting(idPatient, idConsulting);
            if(responses.isEmpty() || responses == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, responses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

    @Operation(summary = "lấy contact, invoice chưa pay, theo idBranch ")
    @GetMapping("patient-paid")
    public ResponseData getPatientPaid(@RequestParam(name = "idBranch")int idBranch){
        try{
            List<Contact> responses = contactService.getContactsPaidByIdBranch(idBranch);
            if(responses.isEmpty() || responses == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, responses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

    @Operation(summary = "lấy contact theo invoice chưa pay theo idBranch, name contact")
    @GetMapping("patient-paid-by-name")
    public ResponseData getPatientPaidByName(@RequestParam(name = "name") String name,
                                             @RequestParam(name = "idBranch")int idBranch){
        try{
            List<Contact> responses = contactService.getContactsPaidByIdBranchAndName(idBranch, name);
            if(responses.isEmpty() || responses == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, responses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
//    @Operation(summary = "Tạo bill thanh toán service")
//    @PutMapping("create-bill-service")
//    public ResponseData createBillServiceForPatient(@RequestBody ServicesRequest servicesRequest){
//        try{
//            medicalRecordService.createBillServices(servicesRequest);
//            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(),ResponseMessageConstants.SUCCESS);
//        }catch (Exception e){
//            String msg = LogUtils.printLogStackTrace(e);
//            logger.error(msg);
//            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
//        }
//    }
    @Operation(summary = "Thanh toan bill")
    @PutMapping("patient-paying-service")
    public ResponseData payingServiceByIdBill(@RequestBody ServicesRequest servicesRequest){
        try{
            medicalRecordService.paymentService(servicesRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(),ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "Kết thức thanh toán")
    @PutMapping("patient-finish-pay")
    public ResponseData finishPatientPay(@RequestParam(name = "idPatient")int idPatient){
        try{
            medicalRecordService.finishPayment(idPatient);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(),ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "Get Report Service by list id")
    @PostMapping("report-service-by-id")
    public ResponseData finishPatientPay(@RequestBody ReportServiceRequest reportServiceRequest){
        try{
            List<IReportServiceResponse> iReportServiceResponses = medicalRecordService.getServicePaying(reportServiceRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(),ResponseMessageConstants.SUCCESS,iReportServiceResponses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
