package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.service.impl.DinaryServiceImpl;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.ResponseData;
import lombok.AllArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class FileController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    private final DinaryServiceImpl dinaryService;

    private boolean isValidFile(MultipartFile file, String type){
        String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        if(type.equals("image")) {
            return  Arrays.asList(new String[] {"jpeg","jpg","png"}).contains(fileExtension.toLowerCase()) ;
        }
        return  Arrays.asList(new String[] {"pdf","jpeg","jpg","png"}).contains(fileExtension.toLowerCase()) ;
    }
    @PostMapping("upload-file")
    public ResponseData uploadToDinary(@RequestParam MultipartFile file, @RequestParam String typeUpload){
        try {
            if(!isValidFile(file, typeUpload)) {
                return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ResponseMessageConstants.FILE_UPLOAD_INVALID);
            }
            if(file.getSize() > 30000000) {
                return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ResponseMessageConstants.FILE_OVER_30MB);
            }
            String url = dinaryService.uploadFile(file);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(),
                    ResponseMessageConstants.UPLOAD_FILE_SUCCESS, url);
        }catch (Exception ex) {
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ex.getMessage());
        }
    }
}
