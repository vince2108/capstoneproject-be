package fpt.edu.capstone.dto.branch;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
@Getter
@Setter
public class BranchRequest {
    private String address;
    private String phoneNumber;
    private String branchName;
}