package fpt.edu.capstone.dto.productGroup;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrGroupUpRequest {
    private int productGroupId;
    private String productGroupName;
    private int productTypeId;
}
