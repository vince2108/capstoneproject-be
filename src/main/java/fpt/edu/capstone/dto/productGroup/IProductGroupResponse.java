package fpt.edu.capstone.dto.productGroup;

public interface IProductGroupResponse {
    int getProductGroupId();
    String getProductGroupName();
    int getProductTypeID();
    String getProductTypeName();
}
