package fpt.edu.capstone.dto.medicine;

public interface ProductMedicine {
    int getProductTypeId();
    String getName();
    int getMedicineTypeId();
    String getMedicineTypeName();
}
