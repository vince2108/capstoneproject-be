package fpt.edu.capstone.dto.medicine;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ReProductRequest {
    private int productId;
    private int amount;
    private String note;
}
