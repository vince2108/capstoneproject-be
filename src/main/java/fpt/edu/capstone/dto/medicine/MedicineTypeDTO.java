package fpt.edu.capstone.dto.medicine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicineTypeDTO {
    private int medicineTypeId;
    private String medicineTypeName;
    private int productTypeId;
    private String productTypeName;
}
