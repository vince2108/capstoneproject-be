package fpt.edu.capstone.dto.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePwRequest {
    private String username;
    private String password;
    private String newPassword;
    private String renewPassword;
}
