package fpt.edu.capstone.dto.medicalrecord;

public interface IMedicalReportService {
    int getMedicalReportId();
    int getInvoiceDetailId();
    double getTaxCode();
}
