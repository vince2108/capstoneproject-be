package fpt.edu.capstone.dto.consulting;

import fpt.edu.capstone.dto.service.ServiceResponse;
import fpt.edu.capstone.entity.ConsultingRoom;
import fpt.edu.capstone.entity.MedicalService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConsultingServicesResponse {
    ConsultingRoom consultingRoom;
    List<ServiceResponse> medicalServices;
}
