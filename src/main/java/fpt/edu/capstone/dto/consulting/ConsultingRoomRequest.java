package fpt.edu.capstone.dto.consulting;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ConsultingRoomRequest {
    @NotBlank
    private String roomName;
    @NotBlank
    private String abbreviationName;
    @NotNull
    private int branchId;
    @NotNull
    private boolean isAction;
}
