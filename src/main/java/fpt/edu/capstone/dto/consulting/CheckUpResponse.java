package fpt.edu.capstone.dto.consulting;

import lombok.Getter;
import lombok.Setter;

public interface CheckUpResponse {
     int getMedicalServiceId();
     int getConsultingRoomId();
     String getServiceName();
     Double getPrice();
     String getRoomName();
     String getAbbreviationName();
     boolean getIsAction();
}
