package fpt.edu.capstone.dto.service;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
public interface ServiceResponse {
    int getMedicalServiceId();
    int getConsultingRoomId();
    String getServiceName();
    double getPrice();
    String getRoomName();
}
