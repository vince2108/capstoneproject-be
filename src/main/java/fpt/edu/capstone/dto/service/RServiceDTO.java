package fpt.edu.capstone.dto.service;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class RServiceDTO {
    int idReportService;
    int idService;
}
