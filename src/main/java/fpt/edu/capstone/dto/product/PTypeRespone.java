package fpt.edu.capstone.dto.product;

public interface PTypeRespone {
    int getProductTypeId();
    String getName();
}
