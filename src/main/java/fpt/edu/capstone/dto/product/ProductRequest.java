package fpt.edu.capstone.dto.product;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProductRequest {
    String productName;
    int unitId;
    Double price;
    String note;
    String userObject;
    String using;
    List<Integer> productGroupIds;
}
