package fpt.edu.capstone.dto.product;

import fpt.edu.capstone.dto.productGroup.IProductGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.List;

@Getter
@Setter
public class ProductResponse {

    private int productId;

    private String productName;

    private Double price;

    private int unitId;
    private String unitName;

    private String note;

    private String userObject;

    private String using;

    private List<IProductGroup> iUnitGroupList;
}
