package fpt.edu.capstone.dto.product;

public interface IProductResponse {
    int getProductId();

    String getProductName();

    Double getPrice();

    int getUnitId();

    String getUnitName();

    String getNote();

    String getUserObject();

    String getUsing();

}
