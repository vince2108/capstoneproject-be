package fpt.edu.capstone.dto.storeHouse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MedicineStoreResponse {
    private int medicineId;
    private String medicineName;
    private int amount;
}
