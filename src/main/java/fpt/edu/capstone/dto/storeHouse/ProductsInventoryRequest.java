package fpt.edu.capstone.dto.storeHouse;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
@Getter
@Setter
public class ProductsInventoryRequest {
    @NotNull
    private int idProduct;
    @NotNull
    private int quantityStock;
    @NotNull
    private int actualQuantity;
    @NotNull
    private int deviant;
    @NotBlank
    private String note;
}
