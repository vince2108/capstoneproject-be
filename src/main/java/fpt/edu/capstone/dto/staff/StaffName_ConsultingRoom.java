package fpt.edu.capstone.dto.staff;

public interface StaffName_ConsultingRoom {
    int getConsultingRoomId();
    int getStaffId();
    String getFullName();
    String getPosition();
}
