package fpt.edu.capstone.dto.staff;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Staff_ConsultingRoomRequestForEach {
    private int accountId;
    private String fullName;
    private String possition;
    private String roomName;
    private int consultingRoomId;
}
