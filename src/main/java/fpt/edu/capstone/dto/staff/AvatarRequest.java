package fpt.edu.capstone.dto.staff;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AvatarRequest {
    private String avatar;
}
