package fpt.edu.capstone.dto.staff;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Staff_ConsultingRoomRequest{
    public int staffId;
    public String position;
    public int consultingRoomId;

}
