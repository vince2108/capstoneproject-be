package fpt.edu.capstone.dto.staff;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;


import java.util.Date;

@Getter
@Setter
public class StaffResponse {

    private int accountId;
    private String fullName;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dob;
    private String address;

    private String village;

    private String district;

    private String province;

    private String sex;

    private String identityCard;

    private String phoneNumber;

    private String ethnicity;

    private String job;
    private int roleId;

    private String email;

    private String avatar;
    private String education;
    private String certificate;
    private int branchId;

    private int consultingRoomId;
    private String possition;
}
