package fpt.edu.capstone.dto.staff;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StaffRequest {
    private String fullName;
    private Date dob;
    private String address;
    private String village;
    private String district;
    private String province;
    private String sex;
    private String identityCard;
    private String phoneNumber;
    private String ethnicity;
    private String job;
    private String email;
    private String userName;
    private String passWord;
    private String confirmPassWord;
    private int branchId;
    private int roleId;
    private int idConsultingRoom;
    private String position;
}
