package fpt.edu.capstone.dto.export;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@Getter
@Setter
public class ExportRequest {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date expectedDate;
    private int petitionerId;
    List<ProductExRequest> productExRequests;
}
