package fpt.edu.capstone.dto.export;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface IShortExport {
    int getReceiptIssueId();
    String getPetitionerName();
    @JsonFormat(pattern = "yyyy-MM-dd")
    Date getExpectedDate();
    int getStatusID();
    String getStatusName();
}
