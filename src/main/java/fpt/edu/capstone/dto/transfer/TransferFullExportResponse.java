package fpt.edu.capstone.dto.transfer;

import fpt.edu.capstone.dto.export.IProductExportResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TransferFullExportResponse {
    private int exportId;
    private String sourceStoreName;
    private String destinationStoreName;
    private String petitionerName;
    private String personInChargeName;
    private Date expectedDate;
    private Date exportDate;
    private List<IProductExportResponse> productResponses;


}
