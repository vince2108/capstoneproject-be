package fpt.edu.capstone.dto.transfer;

import java.util.Date;

public interface ITransferExportResponse {
    int getExportId();
    String getSourceStoreName();

    String getDestinationStoreName();

    Date getExpectedDate();

    int getStatusID();

    String getStatusName();
}
