package fpt.edu.capstone.dto.transfer;

import fpt.edu.capstone.dto.export.IProductExportResponse;
import fpt.edu.capstone.dto.medicine.IProductReResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@Getter
@Setter
public class TransferShReceiptResponse {
    private int receiptId;
    private String sourceStoreName;
    private String destinationStoreName;
    private Date expectedDate;
    private int statusID;
    private String statusName;
    private List<IProductReResponse> productResponses;
}
