package fpt.edu.capstone.dto.transfer;

import java.util.Date;

public interface ITransferReceiptResponse {
    int getReceiptId();

    String getSourceStoreName();

    String getDestinationStoreName();

    Date getExpectedDate();

    int getStatusID();

    String getStatusName();
}
