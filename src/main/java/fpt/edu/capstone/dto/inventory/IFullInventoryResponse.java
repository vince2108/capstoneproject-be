package fpt.edu.capstone.dto.inventory;

import java.util.Date;

public interface IFullInventoryResponse {
    int getInventoryId();
    Date getInventoryDate();
    String getBranchName();
    String getStoreHouseName();
    String getPersonInChargeName();
    int getStoreHouseId();
    int getStatusId();
    String getStatusName();
}
