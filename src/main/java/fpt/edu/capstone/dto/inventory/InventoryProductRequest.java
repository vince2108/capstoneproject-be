package fpt.edu.capstone.dto.inventory;

import com.fasterxml.jackson.annotation.JsonFormat;
import fpt.edu.capstone.dto.storeHouse.ProductsInventoryRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class InventoryProductRequest {
    private int idInventory;
    private int idPersonInCharge;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date inventoryDate;
    List<ProductsInventoryRequest> productsInventoryRequestList;
}
