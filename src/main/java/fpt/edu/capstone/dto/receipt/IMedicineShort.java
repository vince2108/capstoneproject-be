package fpt.edu.capstone.dto.receipt;

import java.util.Date;

public interface IMedicineShort {
    int getReceiptId();
    int getMedicineId();
    String getMedicineName();
    int getAmount();
    String getNote();
    int getUnitId();
    String getUnitName();
    Date getDateOfManufacture();
    Date getExpirationDate();
    Integer getSupplierId();
    Double getEntryPrice();
}
