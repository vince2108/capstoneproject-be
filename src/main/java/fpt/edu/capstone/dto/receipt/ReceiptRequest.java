package fpt.edu.capstone.dto.receipt;

import com.fasterxml.jackson.annotation.JsonFormat;
import fpt.edu.capstone.dto.product.ProductReRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ReceiptRequest {
    private int receivingWareHouseId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date expectedDate;
    private int petitionerId;
    private int supplierId;
//    private double totalCost;
    List<ProductReRequest> productReRequests;
}
