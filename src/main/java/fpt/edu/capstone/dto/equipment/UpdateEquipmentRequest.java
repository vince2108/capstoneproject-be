package fpt.edu.capstone.dto.equipment;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UpdateEquipmentRequest {
    private int equipmentId;
    private double price;
    private String name;
    private String note;
    private int unitId;
    private List<Integer> equipmentTypeIds;
}
