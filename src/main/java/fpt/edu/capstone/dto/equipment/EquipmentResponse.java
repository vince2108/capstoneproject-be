package fpt.edu.capstone.dto.equipment;

import fpt.edu.capstone.dto.medicine.IMedicine_Type;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class EquipmentResponse {
    private int equipmentId;
    private String name;
    private double price;
    private int unitId;
    private String unitName;
    private String note;
    private List<IEquipment_Type> equipmentTypes;
}
