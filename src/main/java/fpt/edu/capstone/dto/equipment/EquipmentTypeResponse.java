package fpt.edu.capstone.dto.equipment;

public interface EquipmentTypeResponse {
    int getEquipmentTypeId();
    String getEquipmentTypeName();
    int getProductTypeId();
    String getProductTypeName();
  //  boolean getIsUse();
}
