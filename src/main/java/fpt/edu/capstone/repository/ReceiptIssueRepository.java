package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.export.IFullExport;
import fpt.edu.capstone.dto.export.IProductExportResponse;
import fpt.edu.capstone.dto.export.IProductQuantity;
import fpt.edu.capstone.dto.export.IShortExport;
import fpt.edu.capstone.dto.medicine.IProductReResponse;
import fpt.edu.capstone.dto.product.IProduct;
import fpt.edu.capstone.dto.receipt.IReceiptHistoryResponse;
import fpt.edu.capstone.dto.receipt.IShortReceipt;
import fpt.edu.capstone.dto.transfer.IFullTransferReceipt;
import fpt.edu.capstone.dto.transfer.ITransferExportResponse;
import fpt.edu.capstone.dto.transfer.ITransferFull;
import fpt.edu.capstone.dto.transfer.ITransferReceiptResponse;
import fpt.edu.capstone.entity.ReceiptAndIssue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public interface ReceiptIssueRepository extends JpaRepository<ReceiptAndIssue, Integer> {
    @Transactional
    @Modifying
    @Query(value = "insert Receipt_Product (ReceiptId,ProductId,NumberOfRequests,EntryPrice) " +
            "values (?1,?2,?3,?4)", nativeQuery = true)
    void insertReceipt_Product(int idReceipt, int idProduct, int numberRequest, Double entryPrice);

    @Query(value = "select ri.ReceiptIssueId, ri.ReceivingWarehouseID, sh.StoreHouseName, sp.SupplierName, ri.ExpectedDate, " +
            "s.StatusID, s.StatusName from ReceiptAndIssue ri " +
            "join StoreHouse sh on sh.StoreHouseId = ri.ReceivingWarehouseID " +
            "join Status s on s.StatusID = ri.Statusid " +
            "join Supplier sp on sp.SupplierId = ri.SupplierId " +
            "where sh.Branchid = ?1 and ri.NOX = ?2 and ri.Istranfer = 0", nativeQuery = true)
    List<IShortReceipt> getReceiptsByIdBranch(int idBranch, boolean nox);

    @Query(value = "select ri.ReceiptIssueId, ri.ReceivingWarehouseID, sh.StoreHouseName, sp.SupplierName, ri.ExpectedDate, " +
            "s.StatusID, s.StatusName from ReceiptAndIssue ri " +
            "join StoreHouse sh on sh.StoreHouseId = ri.ReceivingWarehouseID " +
            "join Status s on s.StatusID = ri.Statusid " +
            "join Supplier sp on sp.SupplierId = ri.SupplierId " +
            "where sh.Branchid = ?1 and ri.NOX = ?2 and ri.Statusid =?3 and ri.Istranfer = 0", nativeQuery = true)
    List<IShortReceipt> getReceiptsByIdBranchAndStatus(int idBranch, boolean nox, int idStatus);

    @Query(value = "select ri.ReceiptIssueId, ri.ReceivingWarehouseID, sh.StoreHouseName, sp.SupplierName, ri.ExpectedDate, " +
            "s.StatusID, s.StatusName from ReceiptAndIssue ri " +
            "join StoreHouse sh on sh.StoreHouseId = ri.ReceivingWarehouseID " +
            "join Status s on s.StatusID = ri.Statusid " +
            "join Supplier sp on sp.SupplierId = ri.SupplierId " +
            "where ri.ReceiptIssueId = ?1", nativeQuery = true)
    IShortReceipt getReceiptsByIdReceipt(int idReceipt);
    @Transactional
    @Modifying
    @Query(value = "insert Receipt_product_2 (id,ProductId,Amount,Note) " +
            "values (?1,?2,?3,?4)", nativeQuery = true)
    void insertReceiptProduct2(int id, int productId, int amount, String note);

    @Query(value = "select rp.ReceiptId, rp.ProductId, p.ProductName, rp.NumberOfRequests, u.UnitName, crp.NumberReceipted " +
            "from Receipt_Product rp " +
            "join Product p on rp.ProductId = p.ProductID " +
            "join Unit u on p.UnitId = u.UnitId " +
            "left join (select rp2.ProductId, crp.ReceiptId, sum(rp2.Amount) as NumberReceipted " +
            "from Create_Receipt_Product crp " +
            "join Receipt_product_2 rp2 on rp2.id = crp.id " +
            "where crp.ReceiptId = ?1 " +
            "Group by rp2.ProductId, crp.ReceiptId) crp on crp.ProductId = rp.ProductId " +
            "where rp.ReceiptId = ?1 ", nativeQuery = true)
    List<IProductReResponse> getProductsReceipt(int idReceipt);
    @Query(value = "select crp.id as receiptedId, crp.dateReceipt, s.Avatar, c.FullName " +
            "from Create_Receipt_Product crp " +
            "join Staff s on crp.personInChargeId = s.StaffId " +
            "join Contact c on c.AccountId = crp.personInChargeId " +
            "where crp.ReceiptId = ?1", nativeQuery = true)
    List<IReceiptHistoryResponse> getReceiptsHistory(int idReceipt);
    @Transactional
    @Modifying
    @Query(value = "insert Issue_Product (IssueId,ProductId,NumberOfRequests) " +
            "values (?1,?2,?3)", nativeQuery = true)
    void insertIssue_Product(int idExport, int idProduct, int numberOfRequest);

    @Query(value = "select ri.ReceiptIssueId, c.FullName as petitionerName, " +
            "ri.ExpectedDate, s.StatusID, s.StatusName from ReceiptAndIssue ri " +
            "join Status s on s.StatusID = ri.Statusid " +
            "join Contact c on c.AccountId = ri.petitioner  " +
            "join Staff st on st.StaffId = ri.petitioner " +
            "where st.BranchId = ?1 and ri.NOX = ?2 and ri.Istranfer = 0", nativeQuery = true)
    List<IShortExport> getExportsByIdBranch(int idBranch, boolean nox);

    @Query(value = "select ri.ReceiptIssueId, c.FullName as petitionerName, " +
            "ri.ExpectedDate, s.StatusID, s.StatusName from ReceiptAndIssue ri " +
            "join Status s on s.StatusID = ri.Statusid " +
            "join Contact c on c.AccountId = ri.petitioner  " +
            "join Staff st on st.StaffId = ri.petitioner " +
            "where st.BranchId = ?1 and ri.NOX = ?2 and ri.Statusid =?3 and ri.Istranfer = 0", nativeQuery = true)
    List<IShortExport> getExportsByIdBranchAndStatus(int idBranch, boolean nox, int idStatus);
    @Transactional
    @Modifying
    @Query(value = "Update Issue_Product set Amount = ?3, Note = ?4 " +
            "where IssueId = ?1 and ProductId = ?2", nativeQuery = true)
    void updateIssue_Product(int idExport, int idProduct, int amount, String note);
    @Query(value = "select sp.ProductID, p.ProductName from StoreHouse_Product sp " +
            "join Product p on sp.ProductID = p.ProductID " +
            "where sp.StoreHouseId = ?1", nativeQuery = true)
    List<IProduct> getProductsByIdStoreHouse(int idStore);
    @Query(value = "with receipt as(select  rp2.ProductId, p.ProductName, sum(rp2.Amount) as Quantity, u.UnitName " +
            "from ReceiptAndIssue ri " +
            "join Create_Receipt_Product crp on ri.ReceiptIssueId = crp.ReceiptId " +
            "join Receipt_product_2 rp2 on rp2.id = crp.id " +
            "join Product p on p.ProductID = rp2.ProductId " +
            "join Unit u on u.UnitId = p.ProductID " +
            "where ri.ReceivingWarehouseID = ?1 and ri.NOX = 1 " +
            "Group By rp2.ProductId, ri.ReceivingWarehouseID, p.ProductName, u.UnitName), " +

            "export as(select isp.ProductId, IsNull(sum(isp.Amount),0) as Quantity " +
            "from ReceiptAndIssue ri " +
            "join Issue_Product isp on ri.ReceiptIssueId = isp.IssueId " +
            "where ri.ReceivingWarehouseID = ?1 and ri.NOX = 0 " +
            " Group By ri.ReceivingWarehouseID, isp.ProductId), " +

            "inven as (select inp.ProductId, sum((inp.QuantityStock-inp.ActualQuantity) ) as deviant " +
            "from Inventory i " +
            "join Inventory_Product inp on i.InventoryId = inp.InventoryId " +
            "where i.StoreHouseId = ?1 " +
            "Group By i.StoreHouseId, inp.ProductId) " +
            "select r.ProductId, r.ProductName, (r.Quantity-IsNull(e.Quantity,0)-IsNull(i.deviant,0)) as Quantity, r.UnitName from " +
            "receipt r " +
            "left join export e on r.ProductId = e.ProductId " +
            "left join inven i on i.ProductId = r.ProductId ", nativeQuery = true)
    List<IProductQuantity> getProductsQuantity(int storeHouseId);
    @Query(value = "with receipt as(select  rp2.ProductId, p.ProductName, sum(rp2.Amount) as Quantity, u.UnitName " +
            "from ReceiptAndIssue ri " +
            "join Create_Receipt_Product crp on ri.ReceiptIssueId = crp.ReceiptId " +
            "join Receipt_product_2 rp2 on rp2.id = crp.id " +
            "join Product p on p.ProductID = rp2.ProductId " +
            "join Unit u on u.UnitId = p.ProductID " +
            "where ri.ReceivingWarehouseID = ?1 and ri.NOX = 1 and p.ProductID = ?2 " +
            "Group By rp2.ProductId, ri.ReceivingWarehouseID, p.ProductName, u.UnitName), " +

            "export as(select isp.ProductId, IsNull(sum(isp.Amount),0) as Quantity " +
            "from ReceiptAndIssue ri " +
            "join Issue_Product isp on ri.ReceiptIssueId = isp.IssueId " +
            "where ri.ReceivingWarehouseID = ?1 and ri.NOX = 0 and isp.ProductID = ?2 " +
            " Group By ri.ReceivingWarehouseID, isp.ProductId), " +

            "inven as (select inp.ProductId, sum(inp.AdjustmentAmount) as deviant " +
            "from Inventory i " +
            "join Inventory_Product inp on i.InventoryId = inp.InventoryId " +
            "where i.StoreHouseId = ?1 and inp.ProductID = ?2 " +
            "Group By i.StoreHouseId, inp.ProductId) " +
            "select r.ProductId, r.ProductName, (r.Quantity-IsNull(e.Quantity,0)-IsNull(i.deviant,0)) as Quantity, r.UnitName from " +
            "receipt r " +
            "left join export e on r.ProductId = e.ProductId " +
            "left join inven i on i.ProductId = r.ProductId ", nativeQuery = true)
    IProductQuantity getProductQuantity(int storeHouseId, int productId);
    @Query(value = "select ri.ReceiptIssueId, ri.ReceivingWarehouseID as storeHouseId, sh.StoreHouseName, c.FullName as petitionerName, " +
            "c2.FullName as personInCharge, ri.ExportDate from ReceiptAndIssue ri " +
            "join StoreHouse sh on sh.StoreHouseId = ri.ReceivingWarehouseID " +
            "join Status s on s.StatusID = ri.Statusid  " +
            "join Contact c on c.AccountId = ri.petitioner " +
            "join Contact c2 on c2.AccountId = ri.PersonInCharge " +
            "where ri.ReceiptIssueId = ?1", nativeQuery = true)
    IFullExport getFullExport(int idExport);
    @Query(value = "select ri.ReceiptIssueId, c.FullName as petitionerName, " +
            "ri.ExpectedDate, s.StatusID, s.StatusName from ReceiptAndIssue ri " +
            "join Status s on s.StatusID = ri.Statusid " +
            "join Contact c on c.AccountId = ri.petitioner " +
            "join Staff st on st.StaffId = ri.petitioner " +
            "where ri.ReceiptIssueId = ?1", nativeQuery = true)
    IShortExport getShortExport(int idExport);

    @Query(value = "select i.ProductId, p.ProductName, i.NumberOfRequests, i.Amount, u.UnitName, i.Note " +
            "from Issue_Product i " +
            "join Product p on p.ProductID = i.ProductId " +
            "join Unit u on p.UnitId = u.UnitId " +
            "where i.IssueId = ?1", nativeQuery = true)
    List<IProductExportResponse> getProductsExportById(int idExport);

    @Transactional
    @Modifying
    @Query(value = "insert Transfer(ReceiptId,ExportId) " +
            "values (?1,?2)",nativeQuery = true)
    void insert_Transfer(int receiptId, int exportId);

    @Query(value = "select rec.ReceiptIssueId as ReceiptId, sh.StoreHouseName as SourceStoreName, " +
            "sh2.StoreHouseName as DestinationStoreName, rec.ExpectedDate, s.StatusID, s.StatusName from Transfer t " +
            "join ReceiptAndIssue rec on rec.ReceiptIssueId = t.ReceiptId " +
            "join StoreHouse sh on sh.StoreHouseId = rec.ReceivingWarehouseID " +
            "join StoreHouse sh2 on sh2.StoreHouseId = rec.WarehouseTransferID " +
            "join Status s on s.StatusID = rec.Statusid", nativeQuery = true)
    List<ITransferReceiptResponse> getTransfersReceipt();

    @Query(value = "select rec.ReceiptIssueId as ReceiptId, sh.StoreHouseName as SourceStoreName, " +
            "sh2.StoreHouseName as DestinationStoreName, rec.ExpectedDate, s.StatusID, s.StatusName from Transfer t " +
            "join ReceiptAndIssue rec on rec.ReceiptIssueId = t.ReceiptId " +
            "join StoreHouse sh on sh.StoreHouseId = rec.ReceivingWarehouseID " +
            "join StoreHouse sh2 on sh2.StoreHouseId = rec.WarehouseTransferID " +
            "join Status s on s.StatusID = rec.Statusid " +
            "where rec.Statusid = IIF(?1 Is Null, rec.Statusid, ?1) ", nativeQuery = true)
    List<ITransferReceiptResponse> getTransfersReceiptByStatus(Integer idStatus);

    @Query(value = "select rec.ReceiptIssueId as ExportId, sh.StoreHouseName as SourceStoreName, " +
            "sh2.StoreHouseName as DestinationStoreName, rec.ExpectedDate, s.StatusID, s.StatusName from Transfer t " +
            "join ReceiptAndIssue rec on rec.ReceiptIssueId = t.ExportId " +
            "join StoreHouse sh on sh.StoreHouseId = rec.ReceivingWarehouseID " +
            "join StoreHouse sh2 on sh2.StoreHouseId = rec.WarehouseTransferID " +
            "join Status s on s.StatusID = rec.Statusid", nativeQuery = true)
    List<ITransferExportResponse> getTransfersExport();

    @Query(value = "select rec.ReceiptIssueId as ExportId, sh.StoreHouseName as SourceStoreName, " +
            "sh2.StoreHouseName as DestinationStoreName, rec.ExpectedDate, s.StatusID, s.StatusName from Transfer t " +
            "join ReceiptAndIssue rec on rec.ReceiptIssueId = t.ExportId " +
            "join StoreHouse sh on sh.StoreHouseId = rec.ReceivingWarehouseID " +
            "join StoreHouse sh2 on sh2.StoreHouseId = rec.WarehouseTransferID " +
            "join Status s on s.StatusID = rec.Statusid " +
            "where rec.Statusid = IIF(?1 Is Null, rec.Statusid, ?1)", nativeQuery = true)
    List<ITransferExportResponse> getTransfersExportByStatus(Integer idStatus);
    @Query(value = "select rec.ReceiptIssueId as ExportId, sh.StoreHouseName as SourceStoreName, " +
            "sh2.StoreHouseName as DestinationStoreName, rec.ExpectedDate, s.StatusID, s.StatusName from Transfer t " +
            "join ReceiptAndIssue rec on rec.ReceiptIssueId = t.ExportId " +
            "join StoreHouse sh on sh.StoreHouseId = rec.ReceivingWarehouseID " +
            "join StoreHouse sh2 on sh2.StoreHouseId = rec.WarehouseTransferID " +
            "join Status s on s.StatusID = rec.Statusid " +
            "where rec.ReceiptIssueId = ?1", nativeQuery = true)
    ITransferExportResponse getTransferExportById(int idExport);
    @Query(value = "select rec.ReceiptIssueId as ReceiptId, sh.StoreHouseName as SourceStoreName, " +
            "sh2.StoreHouseName as DestinationStoreName, rec.ExpectedDate, s.StatusID, s.StatusName from Transfer t " +
            "join ReceiptAndIssue rec on rec.ReceiptIssueId = t.ReceiptId " +
            "join StoreHouse sh on sh.StoreHouseId = rec.ReceivingWarehouseID " +
            "join StoreHouse sh2 on sh2.StoreHouseId = rec.WarehouseTransferID " +
            "join Status s on s.StatusID = rec.Statusid " +
            "where rec.ReceiptIssueId = ?1", nativeQuery = true)
    ITransferReceiptResponse getTransferReceiptById(int idReceipt);

    @Query(value = "select rec.ReceiptIssueId as ExportId, sh.StoreHouseName as SourceStoreName, " +
            "sh2.StoreHouseName as DestinationStoreName, rec.ExpectedDate, rec.ExportDate, " +
            "c1.FullName as PetitionerName, c2.FullName as PersonInChargeName from Transfer t " +
            "join ReceiptAndIssue rec on rec.ReceiptIssueId = t.ExportId " +
            "join StoreHouse sh on sh.StoreHouseId = rec.ReceivingWarehouseID " +
            "join StoreHouse sh2 on sh2.StoreHouseId = rec.WarehouseTransferID " +
            "join Contact c1 on c1.AccountId = rec.petitioner " +
            "join Contact c2 on c2.AccountId = rec.PersonInCharge " +
            "where rec.ReceiptIssueId = ?1",nativeQuery = true)
    ITransferFull getTransferFullExportById(int idExport);
    @Query(value = "select rec.ReceiptIssueId as receiptId, sh.StoreHouseName as SourceStoreName, " +
            "sh2.StoreHouseName as DestinationStoreName, rec.ExpectedDate, " +
            "c1.FullName as PetitionerName from Transfer t " +
            "join ReceiptAndIssue rec on rec.ReceiptIssueId = t.ReceiptId " +
            "join StoreHouse sh on sh.StoreHouseId = rec.ReceivingWarehouseID " +
            "join StoreHouse sh2 on sh2.StoreHouseId = rec.WarehouseTransferID " +
            "join Contact c1 on c1.AccountId = rec.petitioner " +
            "where rec.ReceiptIssueId = ?1", nativeQuery = true)
    IFullTransferReceipt getTransferFullReceiptById(int idReceipt);

}
