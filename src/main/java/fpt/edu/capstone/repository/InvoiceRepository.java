package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.medicalrecord.IMedicalReport;
import fpt.edu.capstone.entity.Invoice;
import fpt.edu.capstone.entity.MedicalRecord;
import fpt.edu.capstone.entity.MedicalReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {
    @Modifying
    @Transactional
    @Query(value = "Update Invoice set TotalCost =?1" +
            " where MedicalReportId = ?2", nativeQuery = true)
    void updateTotalCostInvoice(double totalCost, int medicalReportId);

    @Modifying
    @Transactional
    @Query(value = "update Invoice set status = ?1" +
            " where medicalReportId = ?2")
    void updateStatusInvoice(boolean status, int medicalReportId);

    @Query(value = "select top 1 mr.MedicalReportId, mr.AccountId, mr.IsFinishedExamination " +
            "from MedicalReport mr " +
            "where AccountId = ?1 order by mr.MedicalReportId desc", nativeQuery = true)
    IMedicalReport getMedicalReportNewest(int idPatient);

    @Query(value = "select i from Invoice i where i.medicalReportId = ?1 ")
    Optional<Invoice> getByMedicalReportId(int medicalReportId);

}
