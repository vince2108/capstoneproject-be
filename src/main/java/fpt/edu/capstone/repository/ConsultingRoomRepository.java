package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.consulting.AllConsultingResponse;
import fpt.edu.capstone.dto.consulting.CheckUpResponse;
import fpt.edu.capstone.entity.ConsultingRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
@Repository
public interface ConsultingRoomRepository extends JpaRepository<ConsultingRoom,Integer> {

    @Query(value = "select cr from ConsultingRoom cr " +
            "where cr.isAction = :isAction and lower(cr.roomName) like lower(concat('%', :name ,'%')) or :name is null or :name like '' ")
    List<ConsultingRoom> getConsultingRoomByName(@Param("name") String name, @Param("isAction") boolean isAction);

    @Query(value = "select cr from ConsultingRoom cr where cr.consultingRoomId = ?1 and cr.isAction = ?2 ")
    Optional<ConsultingRoom> getByConsultingRoomIdAndAction(int idRoom, boolean isAction);

    @Query(value = "select cr from ConsultingRoom cr " +
            "join MedicalService ms on cr.consultingRoomId = ms.consultingRoomId " +
            "where ms.medicalServiceId = ?1 and cr.isAction = true ")
    List<ConsultingRoom> getConsultingByIdService(int idService);

    @Query(value = "select c from ConsultingRoom c where c.isAction = true and c.branchId = ?1")
    List<ConsultingRoom> getAllConsultingByIdBranch(int idBranch);
    @Query(value = "select cr.consultingRoomId as consultingRoomId, " +
            "cr.roomName as roomName,cr.abbreviationName as abbreviationName, " +
            "cr.isAction as isAction,ms.price as price,ms.serviceName as serviceName, " +
            "ms.medicalServiceId as medicalServiceId " +
            "from ConsultingRoom cr join MedicalService ms on cr.consultingRoomId = ms.consultingRoomId " +
            "where cr.isAction = true ")
    List<CheckUpResponse> getAllService();
    @Query(value = "select cr.consultingRoomId as consultingRoomId,cr.roomName as roomName , cr.abbreviationName as abbreviationName, cr.branchId as branchId , br.branchName as branchName , cr.isAction as isAction " +
            "from ConsultingRoom as cr join  Branch as br on cr.branchId = br.branchId " +
            "where cr.branchId = ?1 and cr.isAction = true")
    List<AllConsultingResponse> getAllConsultingRoomFix(int idBranch);

    @Query("select cr from ConsultingRoom cr where cr.consultingRoomId = ?1 and cr. isAction = true")
    ConsultingRoom getByConsultingRoomId(int id);

    @Transactional
    @Modifying
    @Query(value = "update ConsultingRoom set isAction = false where consultingRoomId = ?1")
    void deleteConsultingById(int idConsulting);
}
