package fpt.edu.capstone.repository;

import fpt.edu.capstone.entity.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Integer> {
    @Query(value = "select s from Supplier s where s.isUse = true")
    List<Supplier> getAllSuppliers();

    @Query(value = "select s from Supplier s where s.isUse = true and s.supplierId = ?1 ")
    Optional<Supplier> findBySupplierId(int idSupplier);
}
