package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.equipment.ProductEquipment;
import fpt.edu.capstone.dto.medicine.ProductMedicine;
import fpt.edu.capstone.dto.product.PTypeRespone;
import fpt.edu.capstone.dto.product.ProductTypeResponse;
import fpt.edu.capstone.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductTypeRepository extends JpaRepository<ProductType,Integer> {
//    @Query(value = "select mt.medicineTypeId as medicineTypeId,mt.medicineTypeName as medicineTypeName , mt.productTypeId as productTypeId,pt.name as name from ProductType as pt join MedicineType mt on pt.productTypeId = mt.productTypeId ")
//    List<ProductMedicine> getAllMedicineProduct();
//    @Query(value = "select pt.productTypeId as productTypeId, pt.name as name , et.equimentTypeId as equipmentTypeId, et.equipmentTypeName as equipmentTypeName from  ProductType pt join EquipmentType  et on pt.productTypeId=et.productTypeId")
//    List<ProductEquipment> getAllEquipmentProduct();
//    @Query(value = "select sum (quantity) as quantity, pt.ProductTypeId as productTypeId, pt.Name as productTypeName " +
//            "from " +
//            "(select count (*) as quantity, pt.ProductTypeId, pt.Name  " +
//            "from EquimentType et " +
//            "join ProductType pt on pt.ProductTypeId = et.ProductTypeId " +
//            "where et.isUse = 'True' " +
//            "group by pt.ProductTypeId, pt.Name " +
//            "union " +
//            "select count (*) as quantity, pt.ProductTypeId, pt.Name  " +
//            "from MedicineType mt join ProductType pt on mt.ProductTypeID = pt.ProductTypeId " +
//            "where mt.isUse = 'True' " +
//            "group by pt.ProductTypeId, pt.Name) pt " +
//            "group by pt.ProductTypeId, pt.Name" , nativeQuery = true)
//    List<ProductTypeResponse> getAllProduct();
//    @Query(value = "select ProductTypeId, Name from ProductType " , nativeQuery = true)
//    List<PTypeRespone> getAllProduct1();


}
