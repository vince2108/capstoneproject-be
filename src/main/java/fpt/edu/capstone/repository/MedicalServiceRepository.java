package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.service.ServiceResponse;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.entity.MedicalService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalServiceRepository extends JpaRepository<MedicalService, Integer> {
    @Query(value = "Select ms from MedicalService ms " +
            "join ConsultingRoom cr on ms.consultingRoomId = cr.consultingRoomId " +
            "where  cr.isAction = true and cr.branchId = ?1")
    List<MedicalService> getAllServiceByIdBranch(int idBranch);

    @Query(value = "Select * from MedicalService where medicalServiceId=?1", nativeQuery = true)
    MedicalService getConsultingRoomByMedicalServiceId(int medicalServiceId);
    @Query(value = "select m.medicalServiceId as medicalServiceId,m.consultingRoomId as consultingRoomId, " +
            "m.serviceName as serviceName,m.price as price,c.roomName as roomName from MedicalService m " +
            "join ConsultingRoom c on m.consultingRoomId = c.consultingRoomId " +
            "where m.consultingRoomId=:consultingRoomId and c.isAction = true")
    List<ServiceResponse> getDataService(@Param("consultingRoomId") int consultingRoomId);
    @Query(value = "select m.medicalServiceId as medicalServiceId,m.consultingRoomId as consultingRoomId, " +
            "m.serviceName as serviceName,m.price as price,c.roomName as roomName from MedicalService m " +
            "join ConsultingRoom c on m.consultingRoomId = c.consultingRoomId ")
    List<ServiceResponse> getAllServiceName();
    @Query(value = "select m.medicalServiceId as medicalServiceId,m.consultingRoomId as consultingRoomId , m.serviceName as serviceName , m.price as price , c.roomname as roomName from MedicalService m join ConsultingRoom c on m.consultingRoomid = c.consultingRoomid where (lower(serviceName) like lower(concat('%', :serviceName ,'%')) or :serviceName is null or :serviceName like '')",nativeQuery = true)
    List<ServiceResponse> findServiceByServiceName(@Param("serviceName") String serviceName);

    @Query(value = "select m.MedicalServiceId , m.ServiceName , m.Price, m.ConsultingRoomId  " +
            "from MedicalService m " +
            "where m.MedicalServiceId not in   (select  mrs.MedicalServiceId from MedicalReport_Service mrs " +
            "join (select  top 1 *  from MedicalReport where AccountId = ?2 " +
            "order by MedicalReportId desc) mr on mr.MedicalReportId = mrs.MedicalReportId  " +
            "join MedicalService mds on mds.MedicalServiceId = mrs.MedicalServiceId " +
            "where mds.ConsultingRoomId = ?1 and mrs.MedicalServiceId = m.MedicalServiceId)  " +
            "and m.ConsultingRoomId = ?1", nativeQuery = true)
    List<ServiceResponse> getServicesByIdConsulting(int idConsulting, int idPatient);

}
