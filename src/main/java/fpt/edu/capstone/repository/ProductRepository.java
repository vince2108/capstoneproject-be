package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.product.IProductResponse;
import fpt.edu.capstone.dto.productGroup.IProductGroup;
import fpt.edu.capstone.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Transactional
    @Modifying
    @Query(value = "insert ProductGroup_Product (ProductId,ProductGroupId) " +
            "values (?1,?2)", nativeQuery = true)
    void insertProduct_ProductGroup(int idProduct, int idProductGroup);

    @Query(value = "select p.unitId as unitId, p.productId as productId, p.price as price, p.productName as productName, " +
            "p.userObject as userObject, p.using as using, u.unitName as unitName, p.note as note from Product p " +
            "join Unit u on p.unitId = u.unitId")
    List<IProductResponse> getAllProduct();

    @Query(value = "select pg.ProductGroupId, pg.ProductGroupName " +
            "from ProductGroup_Product pgp " +
            "join ProductGroup pg on pgp.ProductGroupId = pg.ProductGroupId " +
            "where pgp.ProductId = ?1", nativeQuery = true)
    List<IProductGroup> getProGroupByIdProduct(int idProduct);
}
