package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.productGroup.IProductGroupResponse;
import fpt.edu.capstone.entity.ProductGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductGroupRepository extends JpaRepository<ProductGroup, Integer> {
    @Query(value = "select pg.ProductGroupId, pg.ProductGroupName, pg.ProductTypeID, py.Name as productTypeName from ProductGroup pg " +
            "join ProductType py on pg.ProductTypeID = py.ProductTypeId " +
            "where pg.IsUse = 1", nativeQuery = true)
    List<IProductGroupResponse> getAllProductGroup();
}
