package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.staff.IStaffM;
import fpt.edu.capstone.dto.staff.StaffForChangePosition;
import fpt.edu.capstone.dto.staff.StaffName_ConsultingRoom;
import fpt.edu.capstone.dto.staff.Staff_ConsultingRoom;
import fpt.edu.capstone.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface StaffRepository extends JpaRepository<Staff, Integer> {
    @Query(value = "select * from Staff_ConsultingRoom where StaffId = ?1", nativeQuery = true)
    Optional<Staff_ConsultingRoom> findStaffInConsultingById(int idStaff);
    @Modifying
    @Transactional
    @Query(value = "insert Staff_ConsultingRoom (StaffId, ConsultingRoomId, Possition)  " +
            "values(?1 ,?2, ?3)",nativeQuery = true)
    void insertStaff_Consulting(int idStaff, int idConsulting, String position);
    @Transactional
    @Modifying
    @Query(value = "update Staff_ConsultingRoom " +
            "set Possition = ?1 , ConsultingRoomId = ?2 " +
            "where StaffId = ?3", nativeQuery = true)
    void updateStaff_Consulting(String position, int idConsulting, int idStaff);

    @Query(value = "select s.Avatar, s.Education, s.Certificate, s.BranchId, sc.ConsultingRoomId, sc.Possition from Staff s " +
            "join Staff_ConsultingRoom sc on sc.StaffId = s.StaffId " +
            "where s.StaffId = ?1 ", nativeQuery = true)
    IStaffM getStaffInfo(int idStaff);
    @Query(value = "select sc.StaffId as staffId , sc.ConsultingRoomId as consultingRoomId , sc.Possition as position , c.FullName as fullName from Staff_ConsultingRoom sc join Contact c on sc.StaffId = c.AccountId " +
            " " +
            "where ConsultingRoomId = ?1",nativeQuery = true)
    List<StaffName_ConsultingRoom> getListStaffName(@Param("ConsultingRoomId") int consultingRoomId);
    @Modifying
    @Transactional
    @Query(value = "Update Staff_ConsultingRoom set ConsultingRoomId = ?1 , Possition = ?2 " +
            "where StaffId = ?3",nativeQuery = true)
    void updatePositionStaff(int consultingRoomId,String position , int staffId);
    @Query(value = "select c.FullName as fullName, c.AccountId as accountId , sc.Possition as possition , cr.RoomName as roomName , cr.ConsultingRoomId as consultingRoomId  from Contact c join Staff_ConsultingRoom sc on c.AccountId = sc.StaffId join ConsultingRoom cr on sc.ConsultingRoomId = cr.ConsultingRoomId",nativeQuery = true)
    List<StaffForChangePosition> getAllStaffToChangePosition();
}
