package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.inventory.IFullInventoryResponse;
import fpt.edu.capstone.dto.inventory.IInventoryProductResponse;
import fpt.edu.capstone.dto.inventory.IInventoryResponse;
import fpt.edu.capstone.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Integer> {
    @Query(value = "select i.InventoryId, i.Date, sh.StoreHouseId, sh.StoreHouseName, b.BranchName, " +
            "s.StatusID, s.StatusName  from Inventory i " +
            "join Contact c on c.AccountId = i.petitioner " +
            "join Status s on s.StatusID = i.StatusId " +
            "join StoreHouse sh on sh.StoreHouseId = i.StoreHouseId " +
            "join Branch b on sh.Branchid = b.BranchId " +
            "where b.BranchId = ?1", nativeQuery = true)
    List<IInventoryResponse> getByIdBranch(int idBranch);
    @Query(value = "select i.InventoryId, i.Date, sh.StoreHouseId, sh.StoreHouseName, b.BranchName, " +
            "s.StatusID, s.StatusName  from Inventory i " +
            "join Contact c on c.AccountId = i.petitioner " +
            "join Status s on s.StatusID = i.StatusId " +
            "join StoreHouse sh on sh.StoreHouseId = i.StoreHouseId " +
            "join Branch b on sh.Branchid = b.BranchId " +
            "where b.BranchId =  ?1 and s.StatusID = ?2", nativeQuery = true)
    List<IInventoryResponse> getByIdBranchByStatus(int idBranch, int idStatus);

    @Query(value = "select i.InventoryId, i.InventoryDate, b.BranchName, " +
            "c.FullName as PersonInChargeName, sh.StoreHouseName , sh.StoreHouseId, i.StatusId, s.StatusName " +
            "from Inventory i " +
            "join StoreHouse sh on i.StoreHouseId = sh.StoreHouseId " +
            "join Branch b on sh.Branchid = b.BranchId " +
            "join Status s on s.StatusID = i.StatusId " +
            "left join Contact c on c.AccountId = i.PersonInCharge " +
            "where i.InventoryId = ?1", nativeQuery = true)
    IFullInventoryResponse getFullInventory(int idInventory);
    @Transactional
    @Modifying
    @Query(value = "insert Inventory_Product (InventoryId,ProductId,QuantityStock,ActualQuantity,Deviant,AdjustmentAmount,Note) " +
            "values (?1,?2,?3,?4,?5,?6,?7)",nativeQuery = true)
    void insertInventoryProduct(int idInventory, int idProduct, int quantityStock,
                                int actualQuantity, int deviant, int adjustmentAmount, String note);

    @Transactional
    @Modifying
    @Query(value = "update Inventory set statusId = ?1 where inventoryId = ?2 ")
    void updateInventoryStatus(int idStatus, int  idInventory);
@Query(value = "select inp.InventoryId, p.ProductID, p.ProductName, inp.QuantityStock, inp.ActualQuantity, " +
        "inp.Deviant, u.UnitName, inp.Note from Inventory_Product inp " +
        "join Product p on inp.ProductId = p.ProductID " +
        "join Unit u on u.UnitId = p.UnitId " +
        "where inp.InventoryId = ?1", nativeQuery = true)
    List<IInventoryProductResponse> getInventoriesProduct(int idInventory);

}
