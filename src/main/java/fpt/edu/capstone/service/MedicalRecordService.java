package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.medicalreportservice.IReportServiceResponse;
import fpt.edu.capstone.dto.medicalreportservice.ServicesRequest;
import fpt.edu.capstone.dto.service.ReportServiceRequest;

import java.util.List;

public interface MedicalRecordService {
    void updateNotePatient(int idPatient, String note);

    List<IReportServiceResponse> getReportServiceByIdPatient(int idPatient);
    List<IReportServiceResponse> getServiceByIdPatientInConsulting(int idPatient, int idConsulting);
//    void createBillServices(ServicesRequest servicesRequest);
//    void paymentService(int idBill);
    void paymentService(ServicesRequest servicesRequest);
    void finishPayment(int idPatient);

    List<IReportServiceResponse> getServicePaying(ReportServiceRequest reportServiceRequest);
}
