package fpt.edu.capstone.service;


import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.patient.PatientResponse;
import fpt.edu.capstone.dto.service.RServiceDTO;
import fpt.edu.capstone.entity.Contact;

import java.util.List;

public interface ExaminationService {
    Contact startExaminationCreate(ContactRequest contactRequest, String allergies, String pastMedicalHistory, int idService, int idConsultingRoom, int idStaff, int idBranch);

    Contact startExaminationUpdate(int idPatient, ContactRequest contactRequest, String allergies, String pastMedicalHistory, int idService, int idConsultingRoom, int idStaff, int idBranch);

    void finishExamination(int idAccount,int idConsulting, String note);
    void addPatientService(int idAccount, String note, int idThisConsulting, int sttThis, int idThatConsulting, int idThatService);
    List<PatientResponse> findPatientByConsultingRoom(int idConsultingRoom, int idBranch);

    void changeStatusExamination(int idConsulting, int idMedicalReport);

    void updateIndexPatient(int idPatient, int idConsultingRoom, int stt);

    void changeRoomService(int idPatient, int idThisConsulting, int sttThis, int idThatConsulting, int idThatService);

    void finishServiceExamination(int idPatient, int idConsulting);


    List<PatientResponse> findPatientByConsultingRoomByStatus(int idConsultingRoom, int idBranch);

    void addSTT(int idReportService, int idThatService);
    void addSTT2(List<RServiceDTO> rServiceDTOS);
}
