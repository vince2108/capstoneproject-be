package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.report.RevenueReport;

import java.util.List;

public interface ReportService {
    List<RevenueReport> getAllRevenueInBranch(int branchId, int month, int year);
    int getPatientExamined(int month, int year, int branchId);
    int getMedicalVisits(int month, int year, int branchId);

    List<RevenueReport> getAllRevenueInBranch(int day, int branchId, int month, int year);
    int getPatientExamined(int day, int month, int year, int branchId);
    int getMedicalVisits(int day, int month, int year, int branchId);
}
