package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.product.IProductResponse;
import fpt.edu.capstone.dto.product.ProductRequest;
import fpt.edu.capstone.dto.product.ProductResponse;
import fpt.edu.capstone.dto.productGroup.IProductGroupResponse;
import fpt.edu.capstone.dto.productGroup.PrGroupUpRequest;
import fpt.edu.capstone.dto.productGroup.ProductGroupRequest;
import fpt.edu.capstone.entity.Product;
import fpt.edu.capstone.entity.ProductGroup;
import fpt.edu.capstone.repository.ProductGroupRepository;
import fpt.edu.capstone.repository.ProductRepository;
import fpt.edu.capstone.service.ProductService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ProductGroupRepository productGroupRepository;
    private final ModelMapper modelMapper;

    @Override
    public void createProductGroup(ProductGroupRequest productGroupRequest) {
        ProductGroup productGroup = modelMapper.map(productGroupRequest, ProductGroup.class);
        productGroup.setProductTypeId(2);
        productGroup.setUse(true);
        productGroupRepository.save(productGroup);
    }

    @Override
    public List<IProductGroupResponse> getAllProductGroup() {
        return productGroupRepository.getAllProductGroup();
    }

    @Override
    public void updateProductGroup(PrGroupUpRequest prGroupUpRequest) {
        if(!productGroupRepository.findById(prGroupUpRequest.getProductGroupId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.PRODUCT_GROUP_DOES_NOT_EXIST);
        ProductGroup productGroup = modelMapper.map(prGroupUpRequest, ProductGroup.class);
        productGroup.setUse(true);
        productGroupRepository.save(productGroup);
    }

    @Override
    public void deleteProduct(int idProductGroup) {
        Optional<ProductGroup> productGroup = productGroupRepository.findById(idProductGroup);
        if(!productGroup.isPresent())
            throw new RuntimeException(ResponseMessageConstants.PRODUCT_GROUP_DOES_NOT_EXIST);
        productGroup.get().setUse(false);
        productGroupRepository.save(productGroup.get());
    }

    @Override
    public void createProduct(ProductRequest productRequest) {
        Product product = modelMapper.map(productRequest, Product.class);
        Product afProduct = productRepository.save(product);
        for (Integer pGroup : productRequest.getProductGroupIds()){
            productRepository.insertProduct_ProductGroup(afProduct.getProductId(),pGroup);
        }
    }

    @Override
    public List<ProductResponse> getAllProduct() {
        List<IProductResponse> iProductResponses = productRepository.getAllProduct();
        List<ProductResponse> productResponses = new ArrayList<>();
        for (IProductResponse iProductResponse : iProductResponses){
            ProductResponse productResponse = modelMapper.map(iProductResponse, ProductResponse.class);
            productResponse.setIUnitGroupList(productRepository.getProGroupByIdProduct(iProductResponse.getProductId()));
            productResponses.add(productResponse);
        }
        return productResponses;
    }
}
