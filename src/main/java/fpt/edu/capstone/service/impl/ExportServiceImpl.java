package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.common.StatusApp;
import fpt.edu.capstone.dto.export.*;
import fpt.edu.capstone.dto.product.IProduct;
import fpt.edu.capstone.dto.receipt.IShortReceipt;
import fpt.edu.capstone.entity.ReceiptAndIssue;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.service.ExportService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class ExportServiceImpl implements ExportService {
    private final ReceiptIssueRepository receiptIssueRepository;
    private final StaffRepository staffRepository;
    private final StatusRepository statusRepository;
    private final StoreHouseRepository storeHouseRepository;
    private final ModelMapper modelMapper;


    @Override
    public void createExport(ExportRequest exportRequest) {
        //check
        if(!staffRepository.findById(exportRequest.getPetitionerId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);
        ReceiptAndIssue receiptAndIssue = modelMapper.map(exportRequest, ReceiptAndIssue.class);
        receiptAndIssue.setStatusId(StatusApp.READY);
        receiptAndIssue.setNox(false);
        receiptAndIssue.setTransfer(false);
        ReceiptAndIssue afExport = receiptIssueRepository.save(receiptAndIssue);
        for(ProductExRequest productExRequest:exportRequest.getProductExRequests()){
            receiptIssueRepository.insertIssue_Product(afExport.getReceiptIssueId(),productExRequest.getProductId(),
                    productExRequest.getNumberOfRequest());
        }
    }

    @Override
    public List<IShortExport> getAllExport(int idBranch) {
        //check
//        List<IShortReceipt> iShortExports = receiptIssueRepository.getReceiptsByIdBranch(idBranch, false, StatusApp.WAITING_FOR_PROGRESSING);
//        iShortExports.addAll(receiptIssueRepository.getReceiptsByIdBranch(idBranch, false, StatusApp.AGREE));
//        iShortExports.addAll(receiptIssueRepository.getReceiptsByIdBranch(idBranch, false, StatusApp.CANCEL));
//        iShortExports.addAll(receiptIssueRepository.getReceiptsByIdBranch(idBranch, false, StatusApp.COMPLETE));
//        return iShortExports;
        return receiptIssueRepository.getExportsByIdBranch(idBranch, false);
    }

    @Override
    public List<IShortExport> getExportByStatus(int idBranch, int idStatus) {
       return receiptIssueRepository.getExportsByIdBranchAndStatus(idBranch, false, idStatus);
    }

    @Override
    public List<IProductQuantity> getProductsQuantityById(int storeHouseId) {
        return receiptIssueRepository.getProductsQuantity(storeHouseId);
    }

    @Override
    public IProductQuantity getProductQuantityVyId(int storeHouseId, int productId) {
        return receiptIssueRepository.getProductQuantity(storeHouseId,productId);
    }

    @Override
    public List<IProduct> getProductsByIdStore(int idStore) {
        return receiptIssueRepository.getProductsByIdStoreHouse(idStore);
    }
//
//    //export detail
//    //choose medicine
//    @Override
//    public List<IMedicineEResponse> getMedicinesByIdStore(int idStore) {
//        //check
//        return storeHouseRepository.getMedicineByIdStore(idStore);
//    }
//
//    @Override
//    public List<ISerialNumberMResponse> getSerialNumberByIdMedicine(int idMedicine, int idStore) {
//        //check
//        return storeHouseRepository.getSerialsByIdMedicine(idStore, idMedicine);
//    }
//
//    @Override
//    public IMedicineInfoResponse getMedicineInfoBySerialNumber(int idReceipt, int idStore, int idMedicine) {
//        //check
//        if(!receiptIssueRepository.findById(idReceipt).isPresent())
//            throw new RuntimeException(ResponseMessageConstants.RECEIPT_DOES_NOT_EXIST);
//        if(!storeHouseRepository.findById(idStore).isPresent())
//            throw new RuntimeException(ResponseMessageConstants.STORE_HOUSE_DOSE_NOT_EXIST);
//        if(!medicineRepository.findById(idMedicine).isPresent())
//            throw new RuntimeException(ResponseMessageConstants.MEDICINE_DOES_NOT_EXIST);
//        return storeHouseRepository.getMedicineInfoBy(idStore, idReceipt, idMedicine);
//    }
//


    @Override
    public void exportProducts(ExportProductsRequest exportProductsRequest) {
        //check
        Optional<ReceiptAndIssue> export = receiptIssueRepository.findById(exportProductsRequest.getIdExport());
        if (!export.isPresent() || export.get().isNox())
            throw new RuntimeException(ResponseMessageConstants.EXPORT_DOES_NOT_EXIST);
        if (!storeHouseRepository.findById(exportProductsRequest.getStoreHouseId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STORE_HOUSE_DOSE_NOT_EXIST);
        if (!staffRepository.findById(exportProductsRequest.getPersonInChargeId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STATUS_DOES_NOT_EXIST);

        //update export
        export.get().setReceivingWareHouseId(exportProductsRequest.getStoreHouseId());
        export.get().setExportDate(exportProductsRequest.getExportDate());
        export.get().setPersonInChargeId(exportProductsRequest.getPersonInChargeId());
        export.get().setStatusId(StatusApp.COMPLETE);
        receiptIssueRepository.save(export.get());

        for(ProductExportRequest productExportRequest: exportProductsRequest.getProductExportRequests()){
            receiptIssueRepository.updateIssue_Product(exportProductsRequest.getIdExport(),productExportRequest.getIdProduct(),
                    productExportRequest.getAmount(), productExportRequest.getNote());
        }
    }

    @Override
    public ExportShortResponse getExportShortById(int idExport) {
        //phieu xuat
        IShortExport export = receiptIssueRepository.getShortExport(idExport);
        if (export == null) throw new RuntimeException(ResponseMessageConstants.EXPORT_DOES_NOT_EXIST);
        ExportShortResponse exportDetailResponse = modelMapper.map(export, ExportShortResponse.class);

        exportDetailResponse.setIProductExportResponses(receiptIssueRepository.getProductsExportById(idExport));

        return exportDetailResponse;
    }

    @Override
    public ExportDetailResponse getExportDetailById(int idExport) {
        //phieu xuat
        IFullExport export = receiptIssueRepository.getFullExport(idExport);
        if (export == null) throw new RuntimeException(ResponseMessageConstants.EXPORT_DOES_NOT_EXIST);
        ExportDetailResponse exportDetailResponse = modelMapper.map(export, ExportDetailResponse.class);

        exportDetailResponse.setIProductExportResponses(receiptIssueRepository.getProductsExportById(idExport));

        return exportDetailResponse;
    }

}
