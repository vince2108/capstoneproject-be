package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.storeHouse.StoreHouseResponse;
import fpt.edu.capstone.entity.Branch;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.entity.StoreHouse;
import fpt.edu.capstone.repository.BranchRepository;
import fpt.edu.capstone.repository.StaffRepository;
import fpt.edu.capstone.repository.StoreHouseRepository;
import fpt.edu.capstone.service.StoreHouseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StoreHouseServiceImpl implements StoreHouseService {
    private final StoreHouseRepository storeHouseRepository;
    private final StaffRepository staffRepository;
    private final BranchRepository branchRepository;

    @Override
    public void createStoreHouse(StoreHouse storeHouse) {
        //check name, ten viet tat
        List<StoreHouse> storeHouses = storeHouseRepository.getByStoreName(storeHouse.getStoreHouseName()
                , storeHouse.getAbbreviationName());
        if (storeHouses.size() > 0) throw new RuntimeException(ResponseMessageConstants.STORE_NAME_EXIST);

        Optional<Branch> branch = branchRepository.findById(storeHouse.getBranchId());
        if (!branch.isPresent()) throw new RuntimeException(ResponseMessageConstants.BRANCH_DOES_NOT_EXIST);
        storeHouseRepository.save(storeHouse);
    }

    @Override
    public void updateStoreHouse(StoreHouse storeHouse) {
//        check kho có tồn tại không với id + isAction
        Optional<StoreHouse> afterStore = storeHouseRepository.findByStoreHouseId(storeHouse.getStoreHouseId());
        if (!afterStore.isPresent()) throw new RuntimeException(ResponseMessageConstants.STORE_HOUSE_DOSE_NOT_EXIST);

        List<StoreHouse> storeHouses = storeHouseRepository.getByStoreNameN(storeHouse.getStoreHouseName()
                , storeHouse.getAbbreviationName(), storeHouse.getStoreHouseId());
        if (storeHouses.size() > 0) throw new RuntimeException(ResponseMessageConstants.STORE_NAME_EXIST);

        Optional<Branch> branch = branchRepository.findById(storeHouse.getBranchId());
        if (!branch.isPresent()) throw new RuntimeException(ResponseMessageConstants.BRANCH_DOES_NOT_EXIST);
        storeHouseRepository.save(storeHouse);
    }

    @Override
    public List<StoreHouse> getAllStoreHouse() {
        return storeHouseRepository.getAllStoreHouse();
    }

    @Override
    public StoreHouse getStoreHouseById(int idStoreHouse) {
        Optional<StoreHouse> storeHouse = storeHouseRepository.getByIdStoreHouse(idStoreHouse);
        if (!storeHouse.isPresent()) throw new RuntimeException(ResponseMessageConstants.STORE_HOUSE_DOSE_NOT_EXIST);
        return storeHouse.get();
    }

    @Override
    public void deleteStore(int idStoreHouse) {
        Optional<StoreHouse> storeHouse = storeHouseRepository.getByIdStoreHouse(idStoreHouse);
        if (!storeHouse.isPresent()) throw new RuntimeException(ResponseMessageConstants.STORE_HOUSE_DOSE_NOT_EXIST);
        storeHouseRepository.deleteByIdStoreHouse(idStoreHouse);
    }

    @Override
    public StoreHouseResponse storeHouseOverview() {
        StoreHouseResponse storeHouseResponse = new StoreHouseResponse();

        return null;
    }

    @Override
    public List<StoreHouse> getStoreHouseByBranchId(int idBranch) {
        return storeHouseRepository.getStoreHouseByIdBranch(idBranch);
    }

}
