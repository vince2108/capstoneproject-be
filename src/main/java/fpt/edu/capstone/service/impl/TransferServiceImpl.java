package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.common.StatusApp;
import fpt.edu.capstone.dto.export.ProductExRequest;
import fpt.edu.capstone.dto.export.ProductExportRequest;
import fpt.edu.capstone.dto.transfer.*;
import fpt.edu.capstone.entity.ReceiptAndIssue;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.service.TransferService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TransferServiceImpl implements TransferService {
    private final StoreHouseRepository storeHouseRepository;
    private final ReceiptIssueRepository receiptIssueRepository;
    private final StaffRepository staffRepository;
    private final ModelMapper modelMapper;

    @Override
    public void createTransfer(TransferRequest transferRequest) {
        //check
        if (!storeHouseRepository.findById(transferRequest.getReceivingWareHouseId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STORE_HOUSE_DOSE_NOT_EXIST);
        if (!staffRepository.findById(transferRequest.getPetitionerId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);
        // create transfer
        ReceiptAndIssue receipt = modelMapper.map(transferRequest, ReceiptAndIssue.class);
        receipt.setStatusId(StatusApp.READY);
        receipt.setTransfer(true);
        receipt.setNox(true);
        ReceiptAndIssue afReceipt = receiptIssueRepository.save(receipt);
        ReceiptAndIssue export = modelMapper.map(transferRequest, ReceiptAndIssue.class);
        export.setStatusId(StatusApp.READY);
        export.setTransfer(true);
        export.setNox(false);
        ReceiptAndIssue afExport = receiptIssueRepository.save(export);
        receiptIssueRepository.insert_Transfer(afReceipt.getReceiptIssueId(),afExport.getReceiptIssueId());
        //

        for(ProductExRequest productExRequest:transferRequest.getProductExRequests()){
            receiptIssueRepository.insertReceipt_Product(afReceipt.getReceiptIssueId(),
                    productExRequest.getProductId(), productExRequest.getNumberOfRequest(),
                    null);
            receiptIssueRepository.insertIssue_Product(afExport.getReceiptIssueId(),productExRequest.getProductId(),
                    productExRequest.getNumberOfRequest());
        }
    }

    @Override
    public void actionExportTransfer(TransferExportProductRequest exportProductsRequest) {
        //check
        Optional<ReceiptAndIssue> export = receiptIssueRepository.findById(exportProductsRequest.getIdExport());
        if (!export.isPresent() || export.get().isNox())
            throw new RuntimeException(ResponseMessageConstants.EXPORT_DOES_NOT_EXIST);
        if (!staffRepository.findById(exportProductsRequest.getPersonInChargeId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STATUS_DOES_NOT_EXIST);

        //update export
        export.get().setExportDate(exportProductsRequest.getExportDate());
        export.get().setPersonInChargeId(exportProductsRequest.getPersonInChargeId());
        export.get().setStatusId(StatusApp.COMPLETE);
        receiptIssueRepository.save(export.get());

        for(ProductExportRequest productExportRequest: exportProductsRequest.getProductExportRequests()){
            receiptIssueRepository.updateIssue_Product(exportProductsRequest.getIdExport(),productExportRequest.getIdProduct(),
                    productExportRequest.getAmount(), productExportRequest.getNote());
        }
    }

    @Override
    public List<ITransferReceiptResponse> getTransfersReceipt() {
        return receiptIssueRepository.getTransfersReceipt();
    }

    @Override
    public List<ITransferReceiptResponse> getTransfersReceipt(Integer idStatus) {
        return receiptIssueRepository.getTransfersReceiptByStatus(idStatus);
    }

    @Override
    public List<ITransferExportResponse> getTransfersExport() {
        return receiptIssueRepository.getTransfersExport();
    }

    @Override
    public List<ITransferExportResponse> getTransfersExport(Integer idStatus) {
        return receiptIssueRepository.getTransfersExportByStatus(idStatus);
    }

    @Override
    public TransferShExportResponse getTransferShExportById(int idExport) {
        ITransferExportResponse iTransfer = receiptIssueRepository.getTransferExportById(idExport);
        if (iTransfer == null) throw new RuntimeException("Không tồn tại phiếu điều chuyển xuất");

        TransferShExportResponse transferResponse = modelMapper.map(iTransfer, TransferShExportResponse.class);

        transferResponse.setProductResponses(receiptIssueRepository.getProductsExportById(idExport));
        return transferResponse;
    }

    @Override
    public TransferShReceiptResponse getTransferShReceiptById(int idTransfer) {
        ITransferReceiptResponse iTransfer = receiptIssueRepository.getTransferReceiptById(idTransfer);
        if (iTransfer == null) throw new RuntimeException("Không tồn tại phiếu điều chuyển nhập");

        TransferShReceiptResponse transferResponse = modelMapper.map(iTransfer, TransferShReceiptResponse.class);

        transferResponse.setProductResponses(receiptIssueRepository.getProductsReceipt(idTransfer));
        return transferResponse;
    }

    @Override
    public TransferFullExportResponse getTransferFullExportById(int idTransfer) {
        ITransferFull iTransfer = receiptIssueRepository.getTransferFullExportById(idTransfer);
        if (iTransfer == null) throw new RuntimeException("Không tồn tại phiếu điều chuyển xuất");

        TransferFullExportResponse transferResponse = modelMapper.map(iTransfer, TransferFullExportResponse.class);

        transferResponse.setProductResponses(receiptIssueRepository.getProductsExportById(idTransfer));
        return transferResponse;
    }

    @Override
    public TransferFullReceiptResponse getTransferFullReceiptById(int idTransfer) {
        IFullTransferReceipt iFullTransferReceipt = receiptIssueRepository.getTransferFullReceiptById(idTransfer);
        TransferFullReceiptResponse transferResponse = modelMapper.map(iFullTransferReceipt, TransferFullReceiptResponse.class);

        transferResponse.setProductsResponse(receiptIssueRepository.getProductsReceipt(idTransfer));
        return transferResponse;
    }


}
