package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.common.StatusApp;
import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.consulting.IConsultingSTT;
import fpt.edu.capstone.dto.medicalrecord.IMedicalReportService;
import fpt.edu.capstone.dto.patient.PatientResponse;
import fpt.edu.capstone.dto.patientInConsulitng.MedicalReport_Consulting;
import fpt.edu.capstone.dto.service.RServiceDTO;
import fpt.edu.capstone.entity.*;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.service.ContactService;
import fpt.edu.capstone.service.ExaminationService;
import lombok.AllArgsConstructor;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ExaminationServiceImpl implements ExaminationService {
    private final ContactRepository contactRepository;
    private final MedicalReportRepository medicalReportRepository;
    private final ConsultingRoomRepository consultingRoomRepository;
    private final MedicalRecordRepository medicalRecordRepository;
    private final MedicalReportServiceRepository mRServiceRepository;
    private final MedicalServiceRepository medicalServiceRepository;
    private final InvoiceRepository invoiceRepository;
    private final InvoiceDetailRepository invoiceDetailRepository;
    private final StaffRepository staffRepository;
    private final BranchRepository branchRepository;
    private final ContactService contactService;
    private final MedicalReportServiceRepository mrServiceRepository;

    private final ModelMapper modelMapper;


    @Override
    public List<PatientResponse> findPatientByConsultingRoom(int idConsultingRoom, int idBranch) {
        List<IConsultingSTT> iConsultingSTTS = medicalReportRepository
                .findByFinishedExaminationAndConsultingRoomId(idConsultingRoom, StatusApp.WAITING_FOR_RESULT, idBranch);
        List<PatientResponse> patients = new ArrayList<>();
        for (IConsultingSTT iConsultingSTT : iConsultingSTTS) {

            contactRepository.findByAccountIdAndStatus(iConsultingSTT.getAccountId(), true)
                    .ifPresent(contact -> {
                        PatientResponse patientResponse = modelMapper.map(contact, PatientResponse.class);
                        patientResponse.setIdConsultingRoom(iConsultingSTT.getConsultingRoomId());
                        patientResponse.setConsultingRoomName(iConsultingSTT.getRoomName());
                        patientResponse.setIdMedicalReport(iConsultingSTT.getMedicalReportId());
                        patientResponse.setStt(iConsultingSTT.getSTT());
                        patientResponse.setIdStatus(iConsultingSTT.getStatusId());
                        patientResponse.setStatusName(iConsultingSTT.getStatusName());
                        patients.add(patientResponse);
                    });
        }
        return patients;
    }

    //khi nhấn khám bệnh cho bệnh nhân //
    @Override
    public void changeStatusExamination(int idConsulting, int idPatient) {
        MedicalReport medicalReport = medicalReportRepository.findMedicalReportNewest(idPatient);
        if (medicalReport == null) throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_DOES_NOT_EXIST);
        medicalReportRepository.changeStatusExamination(idConsulting, medicalReport.getMedicalReportId(),
                StatusApp.BEING_EXAMINATION);
    }

    //bệnh nhân không có mặt nhan next //
    @Override
    public void updateIndexPatient(int idPatient, int idConsultingRoom, int stt) {
        //check
        MedicalReport medicalReport = medicalReportRepository.findMedicalReportNewest(idPatient);
        if (medicalReport == null) throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_DOES_NOT_EXIST);
        List<MedicalReport_Consulting> sttConsultings = medicalReportRepository
                .getByConsultingAndWait(idConsultingRoom, StatusApp.WAITING_FOR_RESULT);
        //set lai Stt
        for (int i = stt; i < sttConsultings.size(); i++) {
            medicalReportRepository.updatePatientInConsulting2(sttConsultings.get(i).getMedicalReportId(),
                    sttConsultings.get(i).getConsultingRoomId(), i);
        }
        //this patient cho ve final index
        medicalReportRepository.updatePatientInConsulting2(medicalReport.getMedicalReportId(), idConsultingRoom, sttConsultings.size());
    }

    //
    @Override
    public Contact startExaminationCreate(ContactRequest contactRequest, String allergies
            , String pastMedicalHistory, int idService, int idConsultingRoom, int idStaff, int idBranch) {
        if (!consultingRoomRepository.findById(idConsultingRoom).isPresent())
            throw new RuntimeException(ResponseMessageConstants.CONSULTING_ROOM_DOES_NOT_EXIST);
        Optional<Staff> staff = staffRepository.findById(idStaff);
        if (!staff.isPresent())
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);
        if (!branchRepository.findById(idBranch).isPresent())
            throw new RuntimeException(ResponseMessageConstants.BRANCH_DOES_NOT_EXIST);
        if (staff.get().getBranchId() != idBranch)
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST_IN_BRANCH);
        //   if(!ValidateEmail.validEmail(contactRequest.getEmail())) throw new RuntimeException(ResponseMessageConstants.EMAIL_FORMAT_INCORRECT);
        if (contactRequest.getPhoneNumber() == null)
            throw new RuntimeException(ResponseMessageConstants.PhoneNumber_NULL);

        //create new contacts
        Contact contact1 = contactService.ContactRequestToContact(contactRequest);
        System.out.println(contact1);
        //tạo hồ sơ bệnh án
        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setAccountId(contact1.getAccountId());
        medicalRecord.setDate(LocalDate.now().toDate());
        medicalRecord.setAllergies(allergies);
        medicalRecord.setPastMedicalHistory(pastMedicalHistory);

        MedicalRecord after = medicalRecordRepository.save(medicalRecord);

        System.out.println(after);
        //tạo phiếu khám mới
        MedicalReport medicalReport = new MedicalReport();
        medicalReport.setMedicalRecordId(after.getMedicalRecordId());
        medicalReport.setAccountId(contact1.getAccountId());
        medicalReport.setFinishedExamination(false);
        medicalReport.setDate(LocalDate.now().toDate());
//        medicalReport.setConsultingRoomId(idConsultingRoom);

        MedicalReport afterMedicalReport = medicalReportRepository.save(medicalReport);
        System.out.println(afterMedicalReport);

        //tạo invoice mới cần (staffId, BranchID)

        Invoice invoice = new Invoice();
        invoice.setStaffId(idStaff);
        invoice.setDate(DateTime.now().toDate());
        invoice.setBranchId(idBranch);
        invoice.setMedicalReportId(afterMedicalReport.getMedicalReportId());
        invoice.setTotalCost(0);

        Invoice afterInvoice = invoiceRepository.save(invoice);
        System.out.println(afterInvoice);

        //tạo invoice detail mới với  invoiceType(tax)

        InvoiceDetail invoiceDetail = new InvoiceDetail();
        invoiceDetail.setInvoiceId(afterInvoice.getInvoiceId());
        invoiceDetail.setInvoiceTypeId(ResponseMessageConstants.INVOICE_TAX_SERVICE);

        InvoiceDetail afterInvoiceDetail = invoiceDetailRepository.save(invoiceDetail);
        System.out.println(afterInvoiceDetail);

        //add medical reportService
        addMedicalReportService(after, idService);
        //add stt in consulting
//        addSTTFinal(afterMedicalReport.getMedicalReportId(), idService, idConsultingRoom, StatusApp.WAITING);
        return contact1;
    }

    //
    @Override
    public Contact startExaminationUpdate(int idPatient, ContactRequest contactRequest, String allergies
            , String pastMedicalHistory, int idService, int idConsultingRoom, int idStaff, int idBranch) {
        if (!contactRepository.findById(idPatient).isPresent())
            throw new RuntimeException(ResponseMessageConstants.CONTACT_DOES_NOT_EXIST);
        if (!consultingRoomRepository.findById(idConsultingRoom).isPresent())
            throw new RuntimeException(ResponseMessageConstants.CONSULTING_ROOM_DOES_NOT_EXIST);
        Optional<Staff> staff = staffRepository.findById(idStaff);
        if (!staff.isPresent())
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);
        if (!branchRepository.findById(idBranch).isPresent())
            throw new RuntimeException(ResponseMessageConstants.BRANCH_DOES_NOT_EXIST);
        if (staff.get().getBranchId() != idBranch)
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST_IN_BRANCH);
        //   if(!ValidateEmail.validEmail(contactRequest.getEmail())) throw new RuntimeException(ResponseMessageConstants.EMAIL_FORMAT_INCORRECT);
        if (contactRequest.getPhoneNumber() == null)
            throw new RuntimeException(ResponseMessageConstants.PhoneNumber_NULL);
        //Update contacts
        Contact contact1 = contactService.updateContact(idPatient, contactRequest);
        //tạo hồ sơ bệnh án
        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setAccountId(contact1.getAccountId());
        medicalRecord.setDate(LocalDate.now().toDate());
        medicalRecord.setAllergies(allergies);
        medicalRecord.setPastMedicalHistory(pastMedicalHistory);

        MedicalRecord after = medicalRecordRepository.save(medicalRecord);

        //tạo phiếu khám mới

        MedicalReport medicalReport = new MedicalReport();
        medicalReport.setMedicalRecordId(after.getMedicalRecordId());
        medicalReport.setAccountId(contact1.getAccountId());
        medicalReport.setFinishedExamination(false);
        medicalReport.setDate(LocalDate.now().toDate());


        MedicalReport afterMedicalReport = medicalReportRepository.save(medicalReport);


        //tạo invoice mới cần (staffId, BranchID)

        Invoice invoice = new Invoice();
        invoice.setStaffId(idStaff);
        invoice.setDate(DateTime.now().toDate());
        invoice.setBranchId(idBranch);
        invoice.setMedicalReportId(afterMedicalReport.getMedicalReportId());
        invoice.setTotalCost(0);

        Invoice afterInvoice = invoiceRepository.save(invoice);

        //tạo invoice detail mới với  invoiceType(tax)

        InvoiceDetail invoiceDetail = new InvoiceDetail();
        invoiceDetail.setInvoiceId(afterInvoice.getInvoiceId());
        invoiceDetail.setInvoiceTypeId(ResponseMessageConstants.INVOICE_TAX_SERVICE);

        InvoiceDetail afterInvoiceDetail = invoiceDetailRepository.save(invoiceDetail);

        //add medical reportService
        addMedicalReportService(after, idService);
        //add stt in consulting
//        addSTTFinal(afterMedicalReport.getMedicalReportId(), idService, idConsultingRoom, StatusApp.WAITING);
        return contact1;
    }

    @Override
    public void finishExamination(int idPatient, int idConsulting, String note) {
        MedicalRecord medicalRecordNewest = medicalRecordRepository.findMedicalRecordNewestByAccountId(idPatient);
        MedicalReport medicalReport = medicalReportRepository.findMedicalReportNewest(idPatient);
        if (medicalReport == null) throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_DOES_NOT_EXIST);
        if (medicalRecordNewest == null)
            throw new ClinicException(ResponseMessageConstants.MEDICAL_RECORD_DOES_NOT_EXIST);
        //update PastMedicalHistory
        medicalRecordRepository.updatePastMedicalHistory(note, medicalRecordNewest.getMedicalRecordId());

        //add service
        // lay thong tin can thiet de add medicalReportService
        IMedicalReportService mrService = medicalRecordRepository
                .findMedicalReportService(medicalRecordNewest.getMedicalRecordId());
        if (mrService == null)
            throw new RuntimeException(ResponseMessageConstants.INVOICE_AND_INVOICE_DETAIL_DOES_NOT_EXIST);

        //set  finishExamination
        medicalReportRepository.updateFinishExamination(medicalRecordNewest.getMedicalRecordId(), idPatient);

        // tinh tong tien (service,..)set invoice
        double totalCostService = mRServiceRepository.getSumCostService(mrService.getInvoiceDetailId());
        invoiceRepository.updateTotalCostInvoice(totalCostService, mrService.getMedicalReportId());

        // xoa STT
        MedicalReport_Consulting medicalReport_consulting = medicalReportRepository.existPatientInConsulting(idConsulting,
                medicalReport.getMedicalReportId());
        medicalReportRepository.deletePatientIndex(medicalReport.getMedicalReportId(), idConsulting);
        //set lai stt
        setAgainSTT(idConsulting, medicalReport_consulting.getSTT());
    }

    @Override
    public void addPatientService(int idPatient, String note, int idThisConsulting, int sttThis
            , int idThatConsulting, int idThatService) {
        MedicalRecord medicalRecordNewest = medicalRecordRepository.findMedicalRecordNewestByAccountId(idPatient);
        if (medicalRecordNewest == null)
            throw new ClinicException(ResponseMessageConstants.MEDICAL_RECORD_DOES_NOT_EXIST);

        //update PastMedicalHistory
        medicalRecordRepository.updatePastMedicalHistory(note, medicalRecordNewest.getMedicalRecordId());
        //add medical record service
        addMedicalReportService(medicalRecordNewest, idThatService);

        //dieu chuyen phong benh
//        changeRoomService(idPatient, idThisConsulting, sttThis, idThatConsulting, idThatService);

        MedicalReport medicalReport = medicalReportRepository.findMedicalReportNewest(idPatient);
        if (medicalReport == null) throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_DOES_NOT_EXIST);

        setAgainSTT(idThisConsulting, sttThis);
        // patient sang phong dịch vụ
        // phong kham chinh update status
        medicalReportRepository.updatePatientInConsulting3(medicalReport.getMedicalReportId(), idThisConsulting, StatusApp.WAITING_FOR_RESULT);


    }

    @Override
    //khi chon dich vu cho benh nhan //
    public void changeRoomService(int idPatient, int idThisConsulting, int sttThis, int idThatConsulting, int idThatService) {
        //check
        MedicalReport medicalReport = medicalReportRepository.findMedicalReportNewest(idPatient);
        if (medicalReport == null) throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_DOES_NOT_EXIST);

//        setAgainSTT(idThisConsulting, sttThis);
        // patient sang phong dịch vụ
        // phong kham chinh update status
//        medicalReportRepository.updatePatientInConsulting3(medicalReport.getMedicalReportId(), idThisConsulting, StatusApp.WAITING_FOR_RESULT);
        //sang phong dịch vụ
        List<MedicalReport_Consulting> sttConsultingService = medicalReportRepository.getByConsultingAndWait(idThatConsulting, StatusApp.WAITING_FOR_RESULT);

        addConsultingRoom(medicalReport.getMedicalReportId(), idThatService, idThatConsulting, StatusApp.WAITING, sttConsultingService.size() + 1);
    }


    //ket thuc kham o phong dich vu
    @Override
    public void finishServiceExamination(int idPatient, int idConsulting) {
        MedicalReport medicalReport = medicalReportRepository.findMedicalReportNewest(idPatient);
        if (medicalReport == null) throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_DOES_NOT_EXIST);
        //xoa stt patient
        medicalReportRepository.deletePatientIndex(medicalReport.getMedicalReportId(), idConsulting);
        setAgainSTT(idConsulting, 1);
        MedicalReport_Consulting medicalReport_consulting =
                medicalReportRepository.findReport_Consulting(medicalReport.getMedicalReportId());
        //add stt final phong kham chinh
        List<MedicalReport_Consulting> sttConsultings = medicalReportRepository
                .getByConsultingAndWait(medicalReport_consulting.getConsultingRoomId(), StatusApp.WAITING_FOR_RESULT);
        //stt final
        medicalReportRepository.updatePatientInConsulting2(medicalReport_consulting.getMedicalReportId()
                , idConsulting, (sttConsultings.size() + 1));
        //status
        medicalReportRepository.updatePatientInConsulting3(medicalReport_consulting.getMedicalReportId(),
                medicalReport_consulting.getConsultingRoomId(),
                StatusApp.WAITING);
    }

    @Override
    public void addSTT(int idReportService, int idThatService) {
        Optional<MedicalService> medicalService = medicalServiceRepository.findById(idThatService);
        if (!medicalService.isPresent()) throw new RuntimeException(ResponseMessageConstants.SERVICE_DOES_NOT_EXIST);
        List<MedicalReport_Consulting> medicalReport_consultings = medicalReportRepository.getByConsultingAndIdConsulting(medicalService.get().getConsultingRoomId(), idReportService);
        if (medicalReport_consultings.size() > 0)
            throw new RuntimeException("Bệnh nhân đang trong phòng khám dịch vụ này");
        List<MedicalReport_Consulting> sttConsultingService = medicalReportRepository.getByConsultingAndWait(medicalService.get().getConsultingRoomId(), StatusApp.WAITING_FOR_RESULT);

        addConsultingRoom(idReportService, idThatService, medicalService.get().getConsultingRoomId(), StatusApp.WAITING, sttConsultingService.size() + 1);


    }

    @Override
    public void addSTT2(List<RServiceDTO> rServiceDTOS) {
        for (RServiceDTO reportServiceId : rServiceDTOS) {
            //find MedicalReport_Service
            Optional<MedicalReportService> medicalReportService = mrServiceRepository.findById(reportServiceId.getIdReportService());
            if (!medicalReportService.isPresent())
                throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_SERVICE_DOES_NOT_EXIST);
            medicalReportService.get().setPay(true);
            mrServiceRepository.save(medicalReportService.get());
            //set stt
            Optional<MedicalService> medicalService = medicalServiceRepository.findById(reportServiceId.getIdService());
            if (!medicalService.isPresent())
                throw new RuntimeException(ResponseMessageConstants.SERVICE_DOES_NOT_EXIST);
            List<MedicalReport_Consulting> medicalReport_consultings = medicalReportRepository.getByConsultingAndIdConsulting(medicalService.get().getConsultingRoomId(), reportServiceId.getIdReportService());
            if (medicalReport_consultings.size() > 0)
                throw new RuntimeException("Bệnh nhân đang trong phòng khám dịch vụ này");
            List<MedicalReport_Consulting> sttConsultingService = medicalReportRepository.getByConsultingAndWait(medicalService.get().getConsultingRoomId(), StatusApp.WAITING_FOR_RESULT);

            addConsultingRoom(reportServiceId.getIdReportService(), reportServiceId.getIdService(), medicalService.get().getConsultingRoomId(), StatusApp.WAITING, sttConsultingService.size() + 1);
        }

    }

    @Override
    public List<PatientResponse> findPatientByConsultingRoomByStatus(int idConsultingRoom, int idBranch) {
        List<IConsultingSTT> iConsultingSTTS = medicalReportRepository
                .findByFinishedExaminationAndConsultingRoomIdAndStatus(idConsultingRoom, StatusApp.WAITING_FOR_RESULT, idBranch);
        List<PatientResponse> patients = new ArrayList<>();
        for (IConsultingSTT iConsultingSTT : iConsultingSTTS) {

            contactRepository.findByAccountIdAndStatus(iConsultingSTT.getAccountId(), true)
                    .ifPresent(contact -> {
                        PatientResponse patientResponse = modelMapper.map(contact, PatientResponse.class);
                        patientResponse.setIdConsultingRoom(iConsultingSTT.getConsultingRoomId());
                        patientResponse.setConsultingRoomName(iConsultingSTT.getRoomName());
                        patientResponse.setIdMedicalReport(iConsultingSTT.getMedicalReportId());
                        patientResponse.setStt(iConsultingSTT.getSTT());
                        patientResponse.setIdStatus(iConsultingSTT.getStatusId());
                        patientResponse.setStatusName(iConsultingSTT.getStatusName());
                        patients.add(patientResponse);
                    });
        }
        return patients;
    }

    //func extend
    private void addMedicalReportService(MedicalRecord medicalRecord, int medicalServiceId) {
        // lay thong tin can thiet de add medicalReportService
        IMedicalReportService mrService = medicalRecordRepository
                .findMedicalReportService(medicalRecord.getMedicalRecordId());
        if (mrService == null)
            throw new RuntimeException(ResponseMessageConstants.INVOICE_AND_INVOICE_DETAIL_DOES_NOT_EXIST);

        // add them service vao medicalReportService
        Optional<MedicalService> medicalService = medicalServiceRepository.findById(medicalServiceId);
        if (!medicalService.isPresent()) throw new ClinicException(ResponseMessageConstants.SERVICE_DOES_NOT_EXIST);
        MedicalReportService medicalReportService = new MedicalReportService();
        medicalReportService.setMedicalReportId(mrService.getMedicalReportId());
        medicalReportService.setServiceId(medicalService.get().getMedicalServiceId());
        medicalReportService.setPay(false);
        medicalReportService.setUse(false);
        //cost+tax
        medicalReportService.setTotalCost(
                medicalService.get().getPrice() + medicalService.get().getPrice() * mrService.getTaxCode() / 100);
        medicalReportService.setInvoiceDetailId(mrService.getInvoiceDetailId());

        MedicalReportService medicalReportService1 = mRServiceRepository.save(medicalReportService);
        System.out.println(medicalReportService1);
    }

    //create or update STT patient
    private void addConsultingRoom(int idMedicalReport, int idService, int idConsultingRoom, int idStatus, int sTT) {
        //check
        if (medicalReportRepository.existPatientInConsulting(idConsultingRoom, idMedicalReport) != null)
            //update
            medicalReportRepository.updatePatientInConsulting(idMedicalReport, idConsultingRoom, idService, idStatus, sTT);
        else
            //create
            medicalReportRepository.insertPatientInConsulting(idMedicalReport, idConsultingRoom, idService, idStatus, sTT);
    }

    private void addSTTFinal(int idMedicalReport, int idService, int idConsultingRoom, int idStatus) {
        List<MedicalReport_Consulting> sttConsultings;
        //6 la trang thai cho lay ket qua
        if (idStatus != StatusApp.WAITING_FOR_RESULT)
            //chờ khám, đang khám
            sttConsultings = medicalReportRepository.getByConsultingAndWait(idConsultingRoom, StatusApp.WAITING_FOR_RESULT);
        else
            // chờ lấy kết quả
            sttConsultings = medicalReportRepository.getByConsultingAndInWait(idConsultingRoom, StatusApp.WAITING_FOR_RESULT);
        addConsultingRoom(idMedicalReport, idService, idConsultingRoom, idStatus, sttConsultings.size() + 1);
    }

    private void setAgainSTT(int idConsulting, int stt) {
        List<MedicalReport_Consulting> sttConsultings = medicalReportRepository
                .getByConsultingAndWait(idConsulting, StatusApp.WAITING_FOR_RESULT);
        //set lai Stt
        for (int i = stt; i < sttConsultings.size(); i++) {
            medicalReportRepository.updatePatientInConsulting2(sttConsultings.get(i).getMedicalReportId(),
                    sttConsultings.get(i).getConsultingRoomId(), i);
        }
    }

}
