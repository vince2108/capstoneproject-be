package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.common.RoleApp;
import fpt.edu.capstone.common.StatusApp;
import fpt.edu.capstone.dto.product.ProductReRequest;
import fpt.edu.capstone.dto.receipt.*;
import fpt.edu.capstone.dto.storeHouse.IProductStore;
import fpt.edu.capstone.dto.medicine.ReProductRequest;
import fpt.edu.capstone.entity.CreateReceiptProduct;
import fpt.edu.capstone.entity.ReceiptAndIssue;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.service.ReceiptService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor

public class ReceiptServiceImpl implements ReceiptService {
    private final StoreHouseRepository storeHouseRepository;
    private final StaffRepository staffRepository;
    private final ModelMapper modelMapper;
    private final ReceiptIssueRepository receiptIssueRepository;
    private final ProductRepository productRepository;
    private final CReceiptProductRepository cReceiptProductRepo;

    @Override
    public void createReceipt(ReceiptRequest receiptRequest) {
        //check
        if (!storeHouseRepository.findById(receiptRequest.getReceivingWareHouseId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STORE_HOUSE_DOSE_NOT_EXIST);
        if (!staffRepository.findById(receiptRequest.getPetitionerId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);
        ReceiptAndIssue receiptAndIssue = modelMapper.map(receiptRequest, ReceiptAndIssue.class);
        receiptAndIssue.setStatusId(StatusApp.READY);
        receiptAndIssue.setNox(true);
        receiptAndIssue.setTransfer(false);
        double totalCost = 0;
        for (ProductReRequest productReRequest : receiptRequest.getProductReRequests()) {
            totalCost += (productReRequest.getEntryPrice() * productReRequest.getNumberOfRequest());
        }
        receiptAndIssue.setTotalCost(totalCost);
        ReceiptAndIssue afReceipt = receiptIssueRepository.save(receiptAndIssue);

        for (ProductReRequest productReRequest : receiptRequest.getProductReRequests()) {
            receiptIssueRepository.insertReceipt_Product(afReceipt.getReceiptIssueId(),
                    productReRequest.getProductId(), productReRequest.getNumberOfRequest(),
                    productReRequest.getEntryPrice());
        }
    }


    @Override
    public void cancelReceipt(int idReceipt) {
        Optional<ReceiptAndIssue> receipt = receiptIssueRepository.findById(idReceipt);
        if (!receipt.isPresent()) throw new RuntimeException(ResponseMessageConstants.RECEIPT_DOES_NOT_EXIST);
        receipt.get().setStatusId(StatusApp.CANCEL);
        receiptIssueRepository.save(receipt.get());
    }

    @Override
    public void completeReceipt(int idReceipt) {
        Optional<ReceiptAndIssue> receipt = receiptIssueRepository.findById(idReceipt);
        if (!receipt.isPresent()) throw new RuntimeException(ResponseMessageConstants.RECEIPT_DOES_NOT_EXIST);
        receipt.get().setStatusId(StatusApp.COMPLETE);
        receiptIssueRepository.save(receipt.get());
    }

    @Override
    public void receiptProduct(ReceiptProductRequest receiptProductRequest) {
        //check
        Optional<ReceiptAndIssue> receipt = receiptIssueRepository.findById(receiptProductRequest.getReceiptId());
        if (!receipt.isPresent())
            throw new RuntimeException(ResponseMessageConstants.RECEIPT_DOES_NOT_EXIST);
        if (!staffRepository.findById(receiptProductRequest.getPersonInChargeId()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);

        CreateReceiptProduct afCReceiptProduct = cReceiptProductRepo.save(new CreateReceiptProduct(receiptProductRequest.getReceiptId(),
                receiptProductRequest.getPersonInChargeId(), receiptProductRequest.getDate()));
        //insert
        for (ReProductRequest productRequest : receiptProductRequest.getProductRequests()) {
            //check
            if (!productRepository.findById(productRequest.getProductId()).isPresent())
                throw new RuntimeException(ResponseMessageConstants.PRODUCT_DOES_NOT_EXIST);
            //insert receipt_product_2
            receiptIssueRepository.insertReceiptProduct2(afCReceiptProduct.getId(), productRequest.getProductId(),
                    productRequest.getAmount(), productRequest.getNote());

            //check trong kho co product nay khong
            IProductStore iProductStore = storeHouseRepository.getProductInStore(productRequest.getProductId(), receipt.get().getReceivingWareHouseId());
            if (iProductStore == null) storeHouseRepository.insertStore_Product(productRequest.getProductId(),
                    receipt.get().getReceivingWareHouseId());
        }
    }

    @Override
    public List<IShortReceipt> getAllReceipt(int idBranch) {
//        List<IShortReceipt> iShortReceipts = receiptIssueRepository.getReceiptsByIdBranch(idBranch, true, StatusApp.WAITING_FOR_PROGRESSING);
//        iShortReceipts.addAll(receiptIssueRepository.getReceiptsByIdBranch(idBranch, true, StatusApp.AGREE));
//        iShortReceipts.addAll(receiptIssueRepository.getReceiptsByIdBranch(idBranch, true, StatusApp.CANCEL));
//        iShortReceipts.addAll(receiptIssueRepository.getReceiptsByIdBranch(idBranch, true, StatusApp.COMPLETE));
//        return iShortReceipts;
        return receiptIssueRepository.getReceiptsByIdBranch(idBranch, true);
    }

    @Override
    public ReceiptProductsResponse getReceiptProductsById(int idReceipt) {
        IShortReceipt receiptResponse = receiptIssueRepository.getReceiptsByIdReceipt(idReceipt);
        if (receiptResponse == null) throw new RuntimeException(ResponseMessageConstants.RECEIPT_DOES_NOT_EXIST);
        ReceiptProductsResponse receiptProductsResponse = modelMapper.map(receiptResponse, ReceiptProductsResponse.class);
        receiptProductsResponse.setProductsResponse(receiptIssueRepository.getProductsReceipt(idReceipt));

        return receiptProductsResponse;
    }

    @Override
    public List<IReceiptHistoryResponse> getHistoryReceiptByIdReceipt(int idReceipt) {
        return receiptIssueRepository.getReceiptsHistory(idReceipt);
    }

    @Override
    public List<IShortReceipt> getReceiptByStatus(int idBranch, int idStatus) {
        return receiptIssueRepository.getReceiptsByIdBranchAndStatus(idBranch, true, idStatus);
    }
}
