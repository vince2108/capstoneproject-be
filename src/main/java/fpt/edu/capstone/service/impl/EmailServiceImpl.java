package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service
public class EmailServiceImpl implements EmailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;
    @Override
    public void sendForgotPasswordEmail(String recipientEmail, String newPassword) throws MessagingException, UnsupportedEncodingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper;
        try {
            mimeMessageHelper = new MimeMessageHelper(mimeMessage);
            mimeMessageHelper.setFrom(sender, "Clinic Management Support");
            mimeMessageHelper.setTo(recipientEmail);

            String subject = "Here's the Code to reset your password";

            String content = "Hello,"
                    + "\nYou have requested to reset your password. "
                    + "\nNew password: "
                    + newPassword
                    + "\nIgnore this email if you do remember your password, "
                    + "or you have not made the request.";

            mimeMessageHelper.setSubject(subject);
            mimeMessageHelper.setText(content, true);
            javaMailSender.send(mimeMessage);
        } catch (Exception e) {
            throw e;
        }
    }
}
