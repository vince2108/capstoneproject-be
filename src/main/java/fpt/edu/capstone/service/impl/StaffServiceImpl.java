package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.staff.*;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.StaffRepository;
import fpt.edu.capstone.service.StaffService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StaffServiceImpl implements StaffService {
    private final StaffRepository staffRepository;
    @Override
    public Staff findStaffByStaffId(int id){
        return staffRepository.findById(id).get();
    }

    @Override
    public List<StaffName_ConsultingRoom> getAllStaffName(int consultingRoomId) {
        return staffRepository.getListStaffName(consultingRoomId);
    }

    @Override
    public void updateStaffPosition(Staff_ConsultingRoomRequestTo staff_consultingRoomRequestTo) {
       // Staff_ConsultingRoomRequest rq = new Staff_ConsultingRoomRequest();
        for (Staff_ConsultingRoomRequestForEach staff_consultingRoomRequestForEach : staff_consultingRoomRequestTo.getListStaffUpdate()) {
            staffRepository.updatePositionStaff(staff_consultingRoomRequestForEach.getConsultingRoomId(),staff_consultingRoomRequestForEach.getPossition(),staff_consultingRoomRequestForEach.getAccountId());
        }
    }

    @Override
    public void save(Staff staff) {
        staffRepository.save(staff);
    }

    @Override
    public List<StaffForChangePosition> getAllStaffForChange()
    {
        return staffRepository.getAllStaffToChangePosition();
    }
}
