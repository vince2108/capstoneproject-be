package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.account.ChangePwRequest;
import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.patient.ContactAndRecordRequest;
import fpt.edu.capstone.dto.patient.MedicalRecordResponse;
import fpt.edu.capstone.dto.patient.PatientResponse;
import fpt.edu.capstone.dto.staff.IStaffM;
import fpt.edu.capstone.dto.staff.InventoryStaff;
import fpt.edu.capstone.dto.staff.StaffRequest;
import fpt.edu.capstone.dto.staff.StaffResponse;
import fpt.edu.capstone.entity.*;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.repository.ContactRepository;
import fpt.edu.capstone.service.ContactService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.Pagination;
import fpt.edu.capstone.utils.ResponseDataPagination;
import fpt.edu.capstone.validate.ValidateEmail;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class ContactServiceImpl implements ContactService {
    private final ContactRepository contactRepository;
    private final MedicalReportRepository medicalReportRepository;
    private final ConsultingRoomRepository consultingRoomRepository;
    private final MedicalRecordRepository medicalRecordRepository;
    private final MedicalReportServiceRepository mRServiceRepository;
    private final MedicalServiceRepository medicalServiceRepository;
    private final InvoiceRepository invoiceRepository;
    private final InvoiceDetailRepository invoiceDetailRepository;
    private final StaffRepository staffRepository;
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;
    private final AuthenticationRepository loginRepository;
//    private final AuthenticationServiceImpl authenticationService;


    @Override
    public List<PatientResponse> getAllPatient() {
        List<MedicalReport> medicalReports = medicalReportRepository
                .findReportByFinishedExamination(false);
//
//        List<PatientResponse> patients = new ArrayList<>();
//
//        for (MedicalReport medicalReport : medicalReports) {
//            contactRepository.findByAccountIdAndStatus(medicalReport.getAccountId(), true)
//                    .ifPresent(contact -> {
//                        Optional<ConsultingRoom> consultingRoom = consultingRoomRepository
//                                .findById(medicalReport.getConsultingRoomId());
//                        PatientResponse patientResponse = new PatientResponse();
//                        patientResponse.setAccountId(contact.getAccountId());
//                        patientResponse.setFullName(contact.getFullName());
//                        patientResponse.setDob(contact.getDob());
//                        patientResponse.setIdConsultingRoom(medicalReport.getConsultingRoomId());
//                        patientResponse.setConsultingRoomName(consultingRoom.get().getRoomName());
//                        patients.add(patientResponse);
//                    });
//        }
//        return patients;
        return null;
    }


    @Override
    public Contact findContactByAccountId(int accId) {
        return contactRepository.findById(accId).get();
    }
    public List<Contact> findContactByRoleIdAndStatusT(int roleId, int idBranch) {
        List<Integer> idAccounts = contactRepository.getIdPatientExaminationByIdBranch(idBranch);
        List<Contact> contacts = new ArrayList<>();
        for(Integer idAccount : idAccounts){
            contacts.add(contactRepository.findContactByRoleIdAndStatusT(roleId,idAccount));
        }
        return contacts;
    }

    public List<Contact> findContactByRoleIdAndStatusF(int roleId) {
        return contactRepository.findContactByRoleIdAndStatusF(roleId);
    }

    //find all patient ( patient as user with role 1 )
    public List<Contact> findContactByRoleId(int roleId) {
        return contactRepository.findContactByRoleId(roleId);
    }

    public Contact ContactRequestToContact(ContactRequest contactRequest) {
        if (!ValidateEmail.validEmail(contactRequest.getEmail()))
            throw new RuntimeException(ResponseMessageConstants.EMAIL_FORMAT_INCORRECT);
        Contact contact = new Contact();
        //    contact.setAccountId(contactRequest.getAccountId());
        contact.setFullName(contactRequest.getFullName().trim());
        contact.setDob(contactRequest.getDob());
        contact.setAddress(contactRequest.getAddress().trim());
        contact.setVillage(contactRequest.getVillage().trim());
        contact.setDistrict(contactRequest.getDistrict().trim());
        contact.setProvince(contactRequest.getProvince().trim());
        contact.setSex(contactRequest.getSex().trim());
        contact.setIdentityCard(contactRequest.getIdentityCard().trim());
        contact.setPhoneNumber(contactRequest.getPhoneNumber().trim());
        contact.setEthnicity(contactRequest.getEthnicity().trim());
        contact.setJob(contactRequest.getJob().trim());
        contact.setEmail(contactRequest.getEmail().trim());
        //False
        contact.setStatus(true);
        //
        contact.setRoleId(1);
        return contactRepository.save(contact);
    }

    //tao moi nhan vien
    public void ContactRequestToStaff(StaffRequest staffRequest) {
        if (!ValidateEmail.validEmail(staffRequest.getEmail()))
            throw new RuntimeException(ResponseMessageConstants.EMAIL_FORMAT_INCORRECT);
        if (!StringUtils.equals(staffRequest.getPassWord(), staffRequest.getConfirmPassWord())) {
            throw new ClinicException(ResponseMessageConstants.CONFIRM_PASSWORD_WRONG);
        }
        if (!consultingRoomRepository.findById(staffRequest.getIdConsultingRoom()).isPresent())
            throw new RuntimeException(ResponseMessageConstants.CONSULTING_ROOM_DOES_NOT_EXIST);
        Optional<Account> account = loginRepository.findAccountsByUsername(staffRequest.getUserName());
        if (account.isPresent()) {
            throw new ClinicException(ResponseMessageConstants.ACCOUNT_EXIST);
        }

        //create contact
        Contact contact = new Contact();

        contact.setFullName(staffRequest.getFullName().trim());
        contact.setDob(staffRequest.getDob());
        contact.setAddress(staffRequest.getAddress().trim());
        contact.setVillage(staffRequest.getVillage().trim());
        contact.setDistrict(staffRequest.getDistrict().trim());
        contact.setProvince(staffRequest.getProvince().trim());
        contact.setSex(staffRequest.getSex().trim());
        contact.setIdentityCard(staffRequest.getIdentityCard().trim());
        contact.setPhoneNumber(staffRequest.getPhoneNumber().trim());
        contact.setEthnicity(staffRequest.getEthnicity().trim());
        contact.setJob(staffRequest.getJob().trim());
        contact.setEmail(staffRequest.getEmail().trim());
        contact.setStatus(true);
        contact.setRoleId(staffRequest.getRoleId());

        Contact newContact = contactRepository.save(contact);

        //create account
        Account acc = new Account();
        acc.setAccountId(newContact.getAccountId());
        acc.setUsername(staffRequest.getUserName().trim());
        acc.setPassword(staffRequest.getPassWord().trim());
        loginRepository.save(acc);

        //create staff
        Staff staff = new Staff();
        staff.setStaffId(newContact.getAccountId());
        staff.setBranchId(staffRequest.getBranchId());
        staff.setAvatar("Vinhcute");
        staff.setEducation("Vinhdz");
        staff.setCertificate("TueNgu");
        Staff afStaff = staffRepository.save(staff);

        //create staff_Consulting
        staffRepository.insertStaff_Consulting(
                afStaff.getStaffId(), staffRequest.getIdConsultingRoom(), staffRequest.getPosition());
    }
    public void ContactRequestToInventoryStaff(InventoryStaff inventoryStaff) {
        if (!ValidateEmail.validEmail(inventoryStaff.getEmail()))
            throw new RuntimeException(ResponseMessageConstants.EMAIL_FORMAT_INCORRECT);
        if (!StringUtils.equals(inventoryStaff.getPassWord(), inventoryStaff.getConfirmPassWord())) {
            throw new ClinicException(ResponseMessageConstants.CONFIRM_PASSWORD_WRONG);
        }
        Optional<Account> account = loginRepository.findAccountsByUsername(inventoryStaff.getUserName());
        if (account.isPresent()) {
            throw new ClinicException(ResponseMessageConstants.ACCOUNT_EXIST);
        }

        //create contact
        Contact contact = new Contact();

        contact.setFullName(inventoryStaff.getFullName().trim());
        contact.setDob(inventoryStaff.getDob());
        contact.setAddress(inventoryStaff.getAddress().trim());
        contact.setVillage(inventoryStaff.getVillage().trim());
        contact.setDistrict(inventoryStaff.getDistrict().trim());
        contact.setProvince(inventoryStaff.getProvince().trim());
        contact.setSex(inventoryStaff.getSex().trim());
        contact.setIdentityCard(inventoryStaff.getIdentityCard().trim());
        contact.setPhoneNumber(inventoryStaff.getPhoneNumber().trim());
        contact.setEthnicity(inventoryStaff.getEthnicity().trim());
        contact.setJob(inventoryStaff.getJob().trim());
        contact.setEmail(inventoryStaff.getEmail().trim());
        contact.setStatus(true);
        contact.setRoleId(5);

        Contact newContact = contactRepository.save(contact);

        //create account
        Account acc = new Account();
        acc.setAccountId(newContact.getAccountId());
        acc.setUsername(inventoryStaff.getUserName());
        acc.setPassword(inventoryStaff.getPassWord());
        loginRepository.save(acc);

        //create staff
        Staff staff = new Staff();
        staff.setStaffId(newContact.getAccountId());
        staff.setBranchId(inventoryStaff.getBranchId());
        staff.setAvatar("Vinhcute");
        staff.setEducation("Vinhdz");
        staff.setCertificate("TueNgu");
        Staff afStaff = staffRepository.save(staff);
    }


    public Contact updateContact(int accountId, ContactRequest contactRequest) {
        Contact contact = contactRepository.getOne(accountId);
        if (!ValidateEmail.validEmail(contactRequest.getEmail()))
            throw new RuntimeException(ResponseMessageConstants.EMAIL_FORMAT_INCORRECT);
        // ContactRequest contactRequest = new ContactRequest();
        //    contact.setAccountId(contactRequest.getAccountId());
        contact.setFullName(contactRequest.getFullName().trim());
        contact.setDob(contactRequest.getDob());
        contact.setAddress(contactRequest.getAddress().trim());
        contact.setVillage(contactRequest.getVillage().trim());
        contact.setDistrict(contactRequest.getDistrict().trim());
        contact.setProvince(contactRequest.getProvince().trim());
        contact.setSex(contactRequest.getSex().trim());
        contact.setIdentityCard(contactRequest.getIdentityCard().trim());
        contact.setPhoneNumber(contactRequest.getPhoneNumber().trim());
        contact.setEthnicity(contactRequest.getEthnicity().trim());
        contact.setJob(contactRequest.getJob().trim());
        contact.setEmail(contactRequest.getEmail().trim());
        //False
        contact.setStatus(true);
        //
        contact.setRoleId(1);
        return contactRepository.save(contact);
    }

    public Contact updateStaff(int accountId, ContactRequest contactRequest) {
        Contact contact = contactRepository.getOne(accountId);
        Optional<Staff> staff = staffRepository.findById(accountId);
        if (!staff.isPresent()) throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);

        contact.setFullName(contactRequest.getFullName().trim());
        contact.setDob(contactRequest.getDob());
        contact.setAddress(contactRequest.getAddress().trim());
        contact.setVillage(contactRequest.getVillage().trim());
        contact.setDistrict(contactRequest.getDistrict().trim());
        contact.setProvince(contactRequest.getProvince().trim());
        contact.setSex(contactRequest.getSex().trim());
        contact.setIdentityCard(contactRequest.getIdentityCard().trim());
        contact.setPhoneNumber(contactRequest.getPhoneNumber().trim());
        contact.setEthnicity(contactRequest.getEthnicity().trim());
        contact.setJob(contactRequest.getJob().trim());
        contact.setEmail(contactRequest.getEmail().trim());
        contact.setStatus(true);//
        return contactRepository.save(contact);
    }

    @Override
    public void deleteStaff(int accountId) {
        Contact contact = contactRepository.getById(accountId);
        contact.setStatus(false);
        contactRepository.save(contact);
    }

    //get patient by idAccount return(contact+medicalRecord)
    @Override
    public MedicalRecordResponse findPatientById(int idPatient) {
        Optional<Contact> contact = contactRepository.findById(idPatient);
        if (!contact.isPresent()) {
            throw new ClinicException(ResponseMessageConstants.CONTACT_DOES_NOT_EXIST);
        }
        Optional<MedicalRecord> medicalRecord = medicalRecordRepository.findTopByAccountIdOrderByMedicalRecordIdDesc(idPatient);
        if (!medicalRecord.isPresent()) {
            throw new ClinicException(ResponseMessageConstants.MEDICAL_RECORD_DOES_NOT_EXIST);
        }
        MedicalRecordResponse patientResponse = modelMapper.map(medicalRecord.get(), MedicalRecordResponse.class);
        patientResponse.setFullName(contact.get().getFullName());
        patientResponse.setSex(contact.get().getSex());
        patientResponse.setDob(contact.get().getDob());
        patientResponse.setAddress(contact.get().getAddress());
        patientResponse.setVillage(contact.get().getVillage());
        patientResponse.setDistrict(contact.get().getDistrict());
        patientResponse.setProvince(contact.get().getProvince());
        patientResponse.setIdentityCard(contact.get().getIdentityCard());
        patientResponse.setPhoneNumber(contact.get().getPhoneNumber());
        patientResponse.setEthnicity(contact.get().getEthnicity());
        patientResponse.setJob(contact.get().getJob());
        patientResponse.setEmail(contact.get().getEmail());
        return patientResponse;
    }

    public List<Contact> findStaffByRoleId() {
        return contactRepository.findStaffByRoleId();

    }

    public StaffResponse findStaffByRoleIdAndAccountId(int accountId) {
        StaffResponse staffResponse = modelMapper.map(contactRepository.findStaffByRoleIdAndAccountId(accountId), StaffResponse.class);
        IStaffM iStaffM = staffRepository.getStaffInfo(accountId);
        if(iStaffM == null) throw  new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);
        staffResponse.setAvatar(iStaffM.getAvatar());
        staffResponse.setCertificate(iStaffM.getCertificate());
        staffResponse.setEducation(iStaffM.getEducation());
        staffResponse.setBranchId(iStaffM.getBranchId());
        staffResponse.setPossition(iStaffM.getPossition());
        staffResponse.setConsultingRoomId(iStaffM.getConsultingRoomId());
        return staffResponse;
    }

    public List<Contact> findStaffByName(String fullName) {
        return contactRepository.findStaffByName(fullName);
    }
//    public void deleteStaff(int accountId){
//        contactRepository.deleteById(accountId);
//    }


    @Override
    public List<Contact> getContactByNameAndStatus(String name, boolean status) {
        return contactRepository.findContactByNameAndStatus(name, status);
    }

    @Override
    public List<Contact> getContactByName(String name) {
        return contactRepository.findContactByName(name);
    }

    @Override
    public Account getContactByExactName(String name) {
        return contactRepository.findContactByExactName(name);
    }

    @Override
    public List<Contact> getContactsPaidByIdBranch(int idBranch) {
        return contactRepository.findContactPaymentByIdBranch(idBranch);
    }

    @Override
    public List<Contact> getContactsPaidByIdBranchAndName(int idBranch, String name) {
        return contactRepository.findContactPaymentByIdBranchAndName(idBranch, name);

    }

    @Override
    public ResponseDataPagination getListContact(Integer pageNo, Integer pageSize) {
        List<Contact> contactList = new ArrayList<>();
        int pageReq = pageNo >= 1 ? pageNo - 1 : pageNo;
        Pageable pageable = PageRequest.of(pageReq, pageSize);
        Page<Contact> contactPage = contactRepository.getAllContact(pageable);
        if (contactPage.hasContent()) {
            for (Contact c : contactPage) {
                contactList.add(c);
            }
        }
        ResponseDataPagination responseDataPagination = new ResponseDataPagination();
        Pagination pagination = new Pagination();
        responseDataPagination.setData(contactList);
        pagination.setCurrentPage(pageNo);
        pagination.setPageSize(pageSize);
        pagination.setTotalPage(contactPage.getTotalPages());
        pagination.setTotalRecords(contactList.size());
        responseDataPagination.setStatus(Enums.ResponseStatus.SUCCESS.getStatus());
        responseDataPagination.setPagination(pagination);
        return responseDataPagination;
    }

    @Override
    public void saveStaffAccount(LoginRequest loginRequest) {
        Account account = loginRepository.findAccountByUsername(loginRequest.getUsername().trim());
        if (account == null) {
            throw new ClinicException(ResponseMessageConstants.ACCOUNT_DOES_NOT_EXIST);
        }
        account.setPassword(loginRequest.getPassword().trim());
        loginRepository.save(account);
    }

    @Override
    public void changePW(ChangePwRequest changePwRequest) {
        Optional<Account> acc = loginRepository.findAccountsByUsernameAndPassword(changePwRequest.getUsername(), changePwRequest.getPassword());
        if (acc != null) {
            if (changePwRequest.getNewPassword().equals(changePwRequest.getRenewPassword())) {
                Account accS = new Account();
                accS.setAccountId(acc.get().getAccountId());
                accS.setUsername(acc.get().getUsername());
                accS.setPassword(changePwRequest.getNewPassword());
                loginRepository.save(accS);
            }
        }
    }

    @Override
    public void updateExaminationInfo(ContactAndRecordRequest contactAndRecordRequest) {
        Optional<Contact> contact = contactRepository.findById(contactAndRecordRequest.getAccountId());
        Optional<MedicalRecord> medicalRecord = medicalRecordRepository.findById(contactAndRecordRequest.getMedicalRecordId());
        if(!contact.isPresent()) throw  new RuntimeException(ResponseMessageConstants.CONTACT_DOES_NOT_EXIST);
        if(!medicalRecord.isPresent()) throw new RuntimeException(ResponseMessageConstants.MEDICAL_RECORD_DOES_NOT_EXIST);
        System.out.println("Before save"+contact.get().toString()+" "+medicalRecord.get().toString());
        Contact contact1 = modelMapper.map(contactAndRecordRequest, Contact.class);
        contact1.setStatus(contact.get().isStatus());
        contact1.setRoleId(contact.get().getRoleId());
        MedicalRecord medicalRecord1 = modelMapper.map(contactAndRecordRequest, MedicalRecord.class);

        Contact afCon =  contactRepository.save(contact1);

        MedicalRecord fapMe = medicalRecordRepository.save(medicalRecord1);

        System.out.println("After save"+afCon+" "+ fapMe);
    }

    @Override
    public Optional<Contact> findByEmail(String email) {
        return contactRepository.findByEmail(email);
    }

}
