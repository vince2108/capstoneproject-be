package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.report.RevenueReport;
import fpt.edu.capstone.repository.MedicalReportRepository;
import fpt.edu.capstone.service.ReportService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ReportServiceImpl implements ReportService {

    private final MedicalReportRepository medicalReportRepository;

    @Override
    public List<RevenueReport> getAllRevenueInBranch(int branchId, int month, int year) {
        return medicalReportRepository.getRevenueByBranchId1(branchId, month, year);
    }

    @Override
    public int getPatientExamined(int month, int year, int branchId) {
        return medicalReportRepository.getPatientExaminedInBranch(branchId, month, year);
    }

    @Override
    public int getMedicalVisits(int month, int year, int branchId) {
        return medicalReportRepository.getMedicalVisitsInBranch(branchId, month, year);
    }

    @Override
    public List<RevenueReport> getAllRevenueInBranch(int day, int branchId, int month, int year) {
        return medicalReportRepository.getRevenueByBranchId1(branchId, month, year, day);
    }

    @Override
    public int getPatientExamined(int day, int month, int year, int branchId) {
        return medicalReportRepository.getPatientExaminedInBranch1(branchId, month, year, day);
    }

    @Override
    public int getMedicalVisits(int day, int month, int year, int branchId) {
        return medicalReportRepository.getMedicalVisitsInBranch1(branchId, month, year, day);
    }
}
