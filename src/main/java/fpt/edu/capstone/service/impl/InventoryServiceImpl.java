package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.common.StatusApp;
import fpt.edu.capstone.dto.inventory.*;
import fpt.edu.capstone.dto.storeHouse.*;
import fpt.edu.capstone.entity.Inventory;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.service.ExportService;
import fpt.edu.capstone.service.InventoryService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class InventoryServiceImpl implements InventoryService {
    private final InventoryRepository inventoryRepository;
    private final StaffRepository staffRepository;
    private final StoreHouseRepository storeHouseRepository;

    private final ExportService exportService;
    private final ModelMapper modelMapper;

    @Override
    public Inventory createInventory(InventoryRequest inventoryRequest) {
        //check
        if (!staffRepository.existsById(inventoryRequest.getPetitionerId()))
            throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);
        if (!storeHouseRepository.existsById(inventoryRequest.getStoreHouseId()))
            throw new RuntimeException(ResponseMessageConstants.STORE_HOUSE_DOSE_NOT_EXIST);
        Inventory inventory = modelMapper.map(inventoryRequest, Inventory.class);
        // set trạng thái chờ
        inventory.setStatusId(StatusApp.READY);
        Inventory afInventory = inventoryRepository.save(inventory);
        return afInventory;
    }

    @Override
    public List<IInventoryResponse> getAllInventory(int idBranch) {
        //admin

//            return inventoryRepository.getAllByAdmin();
        return inventoryRepository.getByIdBranch(idBranch);

    }

    @Override
    public List<IInventoryResponse> getInventoriesByStatus(int idBranch, int idStatus) {
        return inventoryRepository.getByIdBranchByStatus(idBranch, idStatus);
    }

    @Override
    public IFullInventoryResponse getInventoryByIdInventory(int idInventory) {
        if (!inventoryRepository.existsById(idInventory))
            throw new RuntimeException(ResponseMessageConstants.INVENTORY_DOES_NOT_EXIST);
        return inventoryRepository.getFullInventory(idInventory);
    }

    @Override
    public List<IInventoryProductResponse> getInventoryProduct(int idInventory) {
        return inventoryRepository.getInventoriesProduct(idInventory);
    }

    @Override
    public void cancelInventory(int idInventory) {
        inventoryRepository.updateInventoryStatus(StatusApp.CANCEL, idInventory);
    }

    @Override
    public void finishInventory(InventoryProductRequest inventoryProductRequest) {
        //check
        Optional<Inventory> inventory = inventoryRepository.findById(inventoryProductRequest.getIdInventory());
        if (!inventory.isPresent())
            throw new RuntimeException(ResponseMessageConstants.INVENTORY_DOES_NOT_EXIST);

        for (ProductsInventoryRequest product : inventoryProductRequest.getProductsInventoryRequestList()) {
            //check
//            if (!equipmentRepository.existsById(equipmentStore.getIdEquipment()))
//                throw new RuntimeException(ResponseMessageConstants.EQUIPMENT_DOES_NOT_EXIST);

            //create inventory_product
            inventoryRepository.insertInventoryProduct(inventoryProductRequest.getIdInventory(),
                    product.getIdProduct(), product.getQuantityStock(),
                    product.getActualQuantity(), product.getDeviant(),
                    product.getQuantityStock() - product.getActualQuantity(), product.getNote());

        }

        //update status
        inventory.get().setInventoryDate(inventoryProductRequest.getInventoryDate());
        inventory.get().setPersonInCharge(inventoryProductRequest.getIdPersonInCharge());
        inventoryRepository.updateInventoryStatus(StatusApp.COMPLETE, inventoryProductRequest.getIdInventory());
        inventoryRepository.save(inventory.get());

    }

}
