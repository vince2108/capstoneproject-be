package fpt.edu.capstone.service;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface EmailService {
    void sendForgotPasswordEmail(String recipientEmail, String newPassword) throws MessagingException, UnsupportedEncodingException;
}
