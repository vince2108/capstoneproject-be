package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.inventory.*;
import fpt.edu.capstone.entity.Inventory;

import java.util.List;

public interface InventoryService {
    Inventory createInventory(InventoryRequest inventoryRequest);
    List<IInventoryResponse> getAllInventory(int idBranch);
    List<IInventoryResponse> getInventoriesByStatus(int idBranch, int idStatus);
    IFullInventoryResponse getInventoryByIdInventory(int idInventory);
    List<IInventoryProductResponse> getInventoryProduct(int idInventory);
    void cancelInventory(int idInventory);
    void finishInventory(InventoryProductRequest inventoryProductRequest);
}
