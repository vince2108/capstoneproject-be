package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.receipt.*;

import java.util.List;

public interface ReceiptService {

    //    List<IShortReceipt> getAllShor
    void createReceipt(ReceiptRequest receiptRequest);
    void cancelReceipt(int idReceipt);
    void completeReceipt(int idReceipt);
    List<IShortReceipt> getAllReceipt(int idBranch);
    List<IShortReceipt> getReceiptByStatus(int idBranch, int idStatus);
    void receiptProduct(ReceiptProductRequest receiptProductRequest);

    ReceiptProductsResponse getReceiptProductsById(int idReceipt);

    List<IReceiptHistoryResponse> getHistoryReceiptByIdReceipt(int idReceipt);


}
