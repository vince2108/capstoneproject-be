package fpt.edu.capstone.service;

import fpt.edu.capstone.entity.Supplier;

import java.util.List;

public interface SupplierService {
    List<Supplier> getAllSupplier();
}
