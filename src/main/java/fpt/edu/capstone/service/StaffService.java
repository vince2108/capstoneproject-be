package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.staff.StaffForChangePosition;
import fpt.edu.capstone.dto.staff.StaffName_ConsultingRoom;
import fpt.edu.capstone.dto.staff.Staff_ConsultingRoomRequest;
import fpt.edu.capstone.dto.staff.Staff_ConsultingRoomRequestTo;
import fpt.edu.capstone.entity.Staff;

import java.util.List;

public interface StaffService {
    Staff findStaffByStaffId(int id);
    List<StaffName_ConsultingRoom> getAllStaffName(int consultingRoomId);
    void updateStaffPosition(Staff_ConsultingRoomRequestTo staff_consultingRoomRequestTo);
    void save(Staff staff);
    List<StaffForChangePosition> getAllStaffForChange();
}
