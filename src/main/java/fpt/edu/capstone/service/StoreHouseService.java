package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.storeHouse.IEquipmentStoreResponse;
import fpt.edu.capstone.dto.storeHouse.IMedicineStoreResponse;
import fpt.edu.capstone.dto.storeHouse.StoreHouseResponse;
import fpt.edu.capstone.entity.StoreHouse;

import java.util.List;

public interface StoreHouseService {
    void createStoreHouse(StoreHouse storeHouse);
    void updateStoreHouse(StoreHouse storeHouse);
    List<StoreHouse> getAllStoreHouse();
    StoreHouse getStoreHouseById(int idStoreHouse);
    void deleteStore(int idStoreHouse);
    StoreHouseResponse storeHouseOverview();
    List<StoreHouse> getStoreHouseByBranchId(int idBranch);



}
