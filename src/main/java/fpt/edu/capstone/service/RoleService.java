package fpt.edu.capstone.service;

import fpt.edu.capstone.entity.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAllRole();
}
