package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.unit.UnitsRequest;
import fpt.edu.capstone.dto.unitgroup.UnitRequest;
import fpt.edu.capstone.dto.unitgroup.UnitResponse;
import fpt.edu.capstone.entity.Unit;

import java.util.List;

public interface UnitService {
    List<UnitResponse> getListUnit();

    void deleteUnit(UnitsRequest unitsRequest);

    void createUnit(UnitRequest request);

    List<Unit> getUnitByUnitGroupId(int idUnitGroup);
}
