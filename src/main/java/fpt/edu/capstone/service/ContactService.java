package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.account.ChangePwRequest;
import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.patient.ContactAndRecordRequest;
import fpt.edu.capstone.dto.patient.MedicalRecordResponse;
import fpt.edu.capstone.dto.patient.PatientResponse;
import fpt.edu.capstone.dto.staff.InventoryStaff;
import fpt.edu.capstone.dto.staff.StaffRequest;
import fpt.edu.capstone.dto.staff.StaffResponse;
import fpt.edu.capstone.entity.Account;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.utils.ResponseDataPagination;

import java.util.List;
import java.util.Optional;

public interface ContactService {
    List<PatientResponse> getAllPatient();

    Contact findContactByAccountId(int accId);

    List<Contact> findContactByRoleId(int roleId);

    List<Contact> findContactByRoleIdAndStatusT(int roleId, int idBranch);

    List<Contact> findContactByRoleIdAndStatusF(int roleId);

    Contact ContactRequestToContact(ContactRequest contactRequest);

    Contact updateContact(int accountId, ContactRequest contactRequest);

    Contact updateStaff(int accountId, ContactRequest contactRequest);
    void deleteStaff(int accountId);

    MedicalRecordResponse findPatientById(int idPatient);
    void ContactRequestToInventoryStaff(InventoryStaff inventoryStaff);
    List<Contact> findStaffByRoleId();

    StaffResponse findStaffByRoleIdAndAccountId(int accountId);

    //z void deleteStaff(int accountId);
    void ContactRequestToStaff(StaffRequest staffRequest);

    List<Contact> findStaffByName(String fullName);

    List<Contact> getContactByNameAndStatus(String name, boolean status);
    List<Contact> getContactByName(String name);
    Account getContactByExactName(String name);
    List<Contact> getContactsPaidByIdBranch(int idBranch);

    List<Contact> getContactsPaidByIdBranchAndName(int idBranch, String name);

    ResponseDataPagination getListContact(Integer pageNo, Integer pageSize);
    void saveStaffAccount(LoginRequest loginRequest);
    void changePW(ChangePwRequest changePwRequest);
    void updateExaminationInfo(ContactAndRecordRequest contactAndRecordRequest);
    Optional<Contact> findByEmail(String email);
}
