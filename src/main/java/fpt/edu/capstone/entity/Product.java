package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "Product")
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ProductID")
    private int productId;
    @Column(name = "ProductName")
    private String productName;
    @Column(name = "Price")
    private double price;
    @Column(name = "UnitId")
    private int unitId;
    @Column(name = "Note")
    private String note;
    @Column(name = "UserObject")
    private String userObject;
    @Column(name = "Using")
    private String using;
}
