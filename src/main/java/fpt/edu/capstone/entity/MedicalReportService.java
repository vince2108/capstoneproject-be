package fpt.edu.capstone.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "MedicalReport_Service")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class MedicalReportService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ReportServiceId")
    private int reportServiceId;
    @Column(name = "MedicalReportId")
    private int medicalReportId;
    @Column(name = "MedicalServiceId")
    private int serviceId;
    @Column(name = "TotalCost")
    private double totalCost;
    @Column(name = "isPay")
    private boolean isPay;
    @Column(name = "InvoiceDetailId")
    private int InvoiceDetailId;
    @Column(name = "IsUse")
    private boolean isUse;

}
