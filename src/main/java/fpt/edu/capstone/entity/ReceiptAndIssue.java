package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "ReceiptAndIssue")
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptAndIssue {
    @Id
    @Column(name = "ReceiptIssueId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int receiptIssueId;
    @Column(name = "WarehouseTransferID")
    private Integer wareHouseTransferId;
    @Column(name = "ReceivingWarehouseID")
    private Integer receivingWareHouseId;
    @Column(name = "PersonInCharge")
    private Integer personInChargeId;
    @Column(name = "petitioner")
    private int petitionerId;
    @Column(name = "TotalCost")
    private Double totalCost;
    @Column(name = "ExpectedDate")
    private Date expectedDate;
    @Column(name = "ExportDate")
    private Date exportDate;
    @Column(name = "Statusid")
    private int statusId;
    @Column(name = "NOX")
    private boolean nox;
    @Column(name = "SupplierId")
    private Integer supplierId;
    @Column(name = "Istranfer")
    private boolean isTransfer;

}
