package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "WorkSchedule")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WorkSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int workScheduleId;
    @Column(name = "Date")
    private Date date;
    @Column(name = "TimeStart")
    private Date timeStart;
    @Column(name = "StaffId")
    private int staffId;
    @Column(name = "ConsultingRoomId")
    private int consultingRoomId;
    @Column(name ="TimeEnd")
    private Date timeEnd;
}
