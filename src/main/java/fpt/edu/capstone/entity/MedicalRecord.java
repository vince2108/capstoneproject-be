package fpt.edu.capstone.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "MedicalRecord")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MedicalRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int medicalRecordId;
    @Column(name = "AccountId")
    private Integer accountId;
    @Column(name = "Circuit")
    private Integer circuit;
    @Column(name = "BloodPressure")
    private Integer bloodPressure;
    @Column(name = "BMI")
    private Float BMI;
    @Column(name = "Height")
    private Float Height;
    @Column(name = "Temperature")
    private Float Temperature;
    @Column(name = "Weight")
    private Float Weight;
    @Column(name = "PastMedicalHistory")
    private String pastMedicalHistory;
    @Column(name = "Allergies")
    private String allergies;
    @Column(name = "Note")
    private String note;
    @Column(name = "ICD10")
    private String icd10;
    @Column(name = "ICD")
    private String icd;
    @Column(name = "Advice")
    private String advice;
    @Column(name = "Date")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date date;

}
