package fpt.edu.capstone.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.apache.xmlbeans.impl.regex.Match;

import javax.persistence.*;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Getter
@Setter
@Entity
@Table(name = "Contact")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int accountId;
    @Column(name = "FullName")
    private String fullName;
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "Dob")
    private Date dob;
    @Column(name = "Address")
    private String address;
    @Column(name = "Village")
    private String village;
    @Column(name = "District")
    private String district;
    @Column(name = "Province")
    private String province;
    @Column(name = "Sex")
    private String sex;
    @Column(name="IdentityCard")
    private String identityCard;
    @Column(name = "PhoneNumber")
    private String phoneNumber;
    @Column(name = "Ethnicity")
    private String ethnicity;
    @Column(name = "Job")
    private String job;
    @Column(name = "Status")
    private boolean status;
    @Column(name = "RoleId")
    private int roleId;
    @Column(name = "Email")
    private String email;
}
