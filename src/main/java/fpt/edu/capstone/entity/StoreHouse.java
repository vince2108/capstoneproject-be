package fpt.edu.capstone.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "StoreHouse")
@AllArgsConstructor
@NoArgsConstructor
public class StoreHouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "StoreHouseId")
    private int storeHouseId;
    @Column(name = "StoreHouseName")
    private String storeHouseName;
    @Column(name = "AbbreviationName")
    private String abbreviationName;
    @Column(name = "Address")
    private String address;
//    @Column(name = "StaffId")
//    private Integer staffId;
    @Column(name = "IsAction")
    @JsonProperty(value = "isAction", required = true)
    private boolean isAction;
    @Column(name = "BranchId")
    private int branchId;
    @Column(name = "Location")
    private String location;
}
