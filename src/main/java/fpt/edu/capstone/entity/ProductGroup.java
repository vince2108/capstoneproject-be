package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "ProductGroup")
@NoArgsConstructor
@AllArgsConstructor
public class ProductGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ProductGroupId")
    private int productGroupId;
    @Column(name = "ProductGroupName")
    private String productGroupName;
    @Column(name = "ProductTypeID")
    private int productTypeId;
    @Column(name = "IsUse")
    private boolean isUse;
}
