package fpt.edu.capstone.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "Create_Receipt_Product")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateReceiptProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "ReceiptId")
    private int receiptId;
    @Column(name = "personInChargeId")
    private int personInChargeId;
    @Column(name = "dateReceipt")
    private Date dateReceipt;

    public CreateReceiptProduct(int receiptId, int personInChargeId, Date dateReceipt) {
        this.receiptId = receiptId;
        this.personInChargeId = personInChargeId;
        this.dateReceipt = dateReceipt;
    }
}
